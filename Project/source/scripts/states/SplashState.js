export default class extends Phaser.State{

	preload(){
		this.game.load.image('splash-image', 'assets/images/splash/splash-image.png');
	}

	create(){
		var splashImage = this.game.add.sprite(this.game.world.centerX, 20, 'splash-image');
		splashImage.anchor.setTo(0.5, 0);
		splashImage.alpha = 0;

		var tweenAnimateIn = this.game.add.tween(splashImage).to( { alpha: 1 }, 2000, "Linear");
		var tweenAnimateOut = this.game.add.tween(splashImage).to( { alpha: 0 }, 2000, "Linear", false, 2000);

		tweenAnimateIn.chain(tweenAnimateOut);
		tweenAnimateOut.onComplete.add(this.handleTweenComplete, this);
		tweenAnimateIn.start();
	}

	update(){
		if (this.game.input.activePointer.isDown){
			this.goToMainMenuScreen();
		}
	}

	handleTweenComplete(){
		this.goToMainMenuScreen();
	}

	goToMainMenuScreen(){
		this.game.state.start("MainMenu");
	}
}