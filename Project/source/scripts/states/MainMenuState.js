export default class extends Phaser.State{

	preload(){
		this.game.load.image('drop7-logo-image', 'assets/images/mainMenu/drop7-logo.png');
		this.game.load.spritesheet('main-menu-button', 'assets/images/mainMenu/main-menu-button.png', 236, 80);
	}

	create(){
		this.game.stage.backgroundColor = "#ffffff";
		var logoImage = this.game.add.sprite(this.game.world.centerX, 100, 'drop7-logo-image');
		logoImage.anchor.setTo(0.5, 0);

		var style1 = { font: "40px Permanent Marker", fill: "#b05dff", align: "center" };
		this.highscoreTextfield = this.game.add.text(0, 0,"High score: " + this.game.model.player.highscore, style1);
		this.highscoreTextfield.anchor.setTo(0.5, 0);
		this.highscoreTextfield.x = this.game.world.centerX;
		this.highscoreTextfield.y = 425;

		this.button = this.game.add.button(this.game.world.centerX, 550, 'main-menu-button', this.handleClick, this, 1, 0, 0);
		this.button.anchor.setTo(0.5, 0.5);

		var style2 = { font: "50px Permanent Marker", fill: "#FFFFFF", align: "center" };
		var label = this.game.add.text(0, 0, "play", style2);
		this.button.addChild(label);
		label.anchor.set(0.5);

		this.game.commander.execute("AppCommand.PlaySound", "welcome");
	}

	handleClick(){
		/*this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;
		this.game.scale.startFullScreen(false);*/
		this.game.state.start("Gameplay");
	}

}
