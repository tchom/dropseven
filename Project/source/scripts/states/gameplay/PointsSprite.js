import GameSettings from '../../settings/GameSettings'

export default class extends Phaser.Sprite {

	constructor(game, pointsValue, x, y){
		var style1 = { font: "40px Permanent Marker", fill: "#b05dff", align: "center" };

		var text = game.make.text(0, 0,"+"+pointsValue, style1);
		text.stroke = '#ffffff';
		text.strokeThickness = 6;
		
		super(game, 0, 0, text.generateTexture());
		this.anchor.set(0.5);
		this.x = x * GameSettings.GRID_TILE_SIZE + (GameSettings.GRID_TILE_SIZE*0.5);
		this.y = y * GameSettings.GRID_TILE_SIZE + (GameSettings.GRID_TILE_SIZE*0.5);
	}

	animate(){
		var timeline = new TimelineMax({onComplete:this.destroy.bind(this)})
		.to(this, 1, {y:"-=150", ease:Quad.easeIn})
		.to(this, 0.5, {alpha:0}, 0.5)
	}

}