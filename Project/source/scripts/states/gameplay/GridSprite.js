import GameSettings from '../../settings/GameSettings'

export default class extends Phaser.Sprite {

	constructor(game){
		
		var graphics = game.add.graphics(0, 0);

		graphics.beginFill(0xFF0000, 1);

		for (var y = 0; y < GameSettings.TRUE_GRID_HEIGHT; y++) {
			for (var x = 0; x < GameSettings.GRID_WIDTH; x++) {
				//add extra row for the overflow
				if(y == 0){
					graphics.beginFill(0xFF71ce, 0.1);
				}else{
					graphics.beginFill(0xFF71ce, 1);
				}
				
				graphics.lineStyle(1, 0x05ffa1, 1);
				graphics.drawRect(x*GameSettings.GRID_TILE_SIZE, y*GameSettings.GRID_TILE_SIZE, GameSettings.GRID_TILE_SIZE, GameSettings.GRID_TILE_SIZE);
			}
		}

		super(game, 0, 0, graphics.generateTexture());
		graphics.destroy();

		this.inputEnabled = true;
	}

	
}

