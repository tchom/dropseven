import GameSettings from '../../settings/GameSettings'

export default class extends Phaser.Sprite {

	constructor(game, gridDiscData){

		var sprite;
		if(gridDiscData.armour >= 2){
			sprite =  game.make.sprite(0, 0, 'discsprites', "armour2");
		}else if(gridDiscData.armour == 1){
			sprite =  game.make.sprite(0, 0, 'discsprites', "armour1");
		}else{
			sprite =  game.make.sprite(0, 0, 'discsprites', gridDiscData.discValue.toString());
		}

		
		super(game, 0, 0, sprite.texture);


		this.armour2Sprite =  game.make.sprite(0, 0, 'discsprites', "armour2");
		this.armour1Sprite =  game.make.sprite(0, 0, 'discsprites', "armour1");
		this.valueSprite = game.make.sprite(0, 0, 'discsprites', gridDiscData.discValue.toString());

		this.anchor.set(0.5);
		this.data = gridDiscData;
		this.data.onChanged.add(this._handlePositionUpdated, this)
		this.data.onArmourChanged.add(this._handleArmourUpdated, this)
		this.updated = false;
		this.tween = null;
	}

	update(){
		this.z = -this.y;
	}

	changeSprite(){
		if(this.data.armour >= 2){
			this.loadTexture('discsprites', "armour2");
		}else if(this.data.armour == 1){
			this.loadTexture('discsprites', "armour1");
		}else{
			this.loadTexture('discsprites', this.data.discValue.toString());
		}
	}

	get destroyed(){
		return this.data.destroyed;
	}


	_handlePositionUpdated(){
		this.updated = true;
	}

	_handleArmourUpdated(){
		this.changeSprite();
	}

	gridPositionToWorldPosition(){
		this.x = this.data.x * GameSettings.GRID_TILE_SIZE + (GameSettings.GRID_TILE_SIZE*0.5);
		this.y = this.data.y * GameSettings.GRID_TILE_SIZE + (GameSettings.GRID_TILE_SIZE*0.5);
	}

	tweenToDestroy(onCompleteCallback, scope){
		this.tween = TweenMax.to(this.scale, 0.5, {x:0, y:0, ease:Back.easeIn.config(1.7), onComplete: onCompleteCallback.bind(scope),  onCompleteParams:[this, true],  paused:true});
	}

	tweenToWorldPosition(onCompleteCallback, scope){
		var worldX = this.data.x * GameSettings.GRID_TILE_SIZE + (GameSettings.GRID_TILE_SIZE*0.5);
		var worldY = this.data.y * GameSettings.GRID_TILE_SIZE + (GameSettings.GRID_TILE_SIZE*0.5);


		this.tween = TweenMax.to(this, 0.25, { x: worldX, y: worldY, ease: Sine.easeInOut, onComplete: onCompleteCallback.bind(scope),  onCompleteParams:[this], paused:true});
		this.updated = false;
	}

	destroy(){
		this.data.onChanged.remove(this._handlePositionUpdated, this)
		super.destroy()
	}

	startTween(){
		this.tween.play();
	}


}

