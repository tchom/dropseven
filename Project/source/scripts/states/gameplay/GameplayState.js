import GridSprite from './GridSprite'
import GridDiscSprite from './GridDiscSprite'
import PointsSprite from './PointsSprite'

import GameSettings from '../../settings/GameSettings'


export default class extends Phaser.State{

	preload(){
		this.game.load.atlasJSONHash('discsprites', 'assets/images/gameplay/disc_sprites.png', 'assets/images/gameplay/disc_sprites.json');
	}

	create(){
		this.discSprites = [];
		this.animationQueue = [];
		this.isAnimating = false;
		this.inGameOverSequence = false;
		this.container = this.game.add.group();
		this.container.x = 100;
		this.container.y = 100;

		

		this.discsGroup = this.game.add.group();

		this.previousDiscSprite = null;
		this.nextDiscSprite = null;

		this.gridSprite = new GridSprite(this.game);
		this.gridSprite.events.onInputUp.add(this.handleGridClicked, this);
		this.container.add(this.gridSprite);
		this.container.add(this.discsGroup);

		this.effects = this.game.add.group();
		this.effects.x = this.container.x;
		this.effects.y = this.container.y;

		var style1 = { font: "40px Permanent Marker", fill: "#b05dff", align: "left" };
		this.scoreTextfield = this.game.add.text(0, 0,"Score", style1);
		this.scoreTextfield.x = 20;
		this.scoreTextfield.y = 10;

		var style2 = { font: "40px Permanent Marker", fill: "#b05dff", align: "right" };
		this.countdownTextfield = this.game.add.text(0, 0,"Next round", style2);
		this.countdownTextfield.x = 730;
		this.countdownTextfield.y = 10;
		this.countdownTextfield.anchor.set(1, 0);

		this.game.commander.execute("GameplayCommand.InitialiseGameplay", this.game, this);
		this.game.commander.execute("AppCommand.PlaySound", "new_game");
	}

	update() {
		if(this.nextDiscSprite != null && !this.inGameOverSequence){
			this.handleGridMove();
		}

		this.discsGroup.sort('y', Phaser.Group.SORT_DESCENDING);
		this.scoreTextfield.text = "Score: " + this.game.model.gameplay.score;
		this.countdownTextfield.text = "Next round: " + this.game.model.gameplay.turnsUntilNextRound + " turns";
	}

	handleGridClicked(owner, pointer){
		if(!this.isAnimating && !this.inGameOverSequence){
			//Determine row clicked
			var localX = pointer.x - this.container.x;
			var columnX = Math.floor(localX / GameSettings.GRID_TILE_SIZE);

			//Bound to grid
			if(columnX < 0){
				columnX = 0;
			}
			if(columnX > GameSettings.GRID_WIDTH - 1){
				columnX = GameSettings.GRID_WIDTH - 1;
			}

			this.previousDiscSprite = this.nextDiscSprite;
			this.nextDiscSprite = null;

			this.game.commander.execute("GameplayCommand.DropNextGridDisc", this.game, this.previousDiscSprite.data, columnX, this);
		}
	}

	handleGridMove(){
		//Determine row clicked
		var localX = this.game.input.x - this.container.x;
		//var localY = pointer.y - this.container.y;
		var columnX = Math.floor(localX / GameSettings.GRID_TILE_SIZE);

		//Bound to grid
		if(columnX < 0){
			columnX = 0;
		}
		if(columnX > GameSettings.GRID_WIDTH - 1){
			columnX = GameSettings.GRID_WIDTH - 1;
		}

		var columnWorldX = columnX *  GameSettings.GRID_TILE_SIZE + (GameSettings.GRID_TILE_SIZE * 0.5);

		this.nextDiscSprite.x = columnWorldX;
		this.nextDiscSprite.y = (GameSettings.GRID_TILE_SIZE * 0.5);
	}

	createNextDiscSprite(gridDiscData){
		this.nextDiscSprite = this.createNewGridDiscSprite(gridDiscData);
	}

	createNewGridDiscSprite(gridDiscData){
		var newGridDiscSprite = new GridDiscSprite(this.game, gridDiscData);
		this.discsGroup.add(newGridDiscSprite);
		this.discSprites.push(newGridDiscSprite);
		return newGridDiscSprite
	}

	createPointsSprite(value, x, y){
		var newPointsSprite = new PointsSprite(this.game, value, x, y);
		this.effects.add(newPointsSprite);
		newPointsSprite.animate();
	}


	handleAnimations(){
		var discsToDestroy = [];
		var discsToUpdate = [];
		//determine discs to animate
		for (var i = 0; i < this.discSprites.length; i++) {
			var discSprite = this.discSprites[i];

			if(discSprite.destroyed){
				discsToDestroy.push(discSprite);
			}else if(discSprite.updated){
				discsToUpdate.push(discSprite);
			}
		}

		//remove destroyed sprites from main array
		this.discSprites = this.discSprites.filter( function( el ) {
			return !discsToDestroy.includes( el );
		} );

		if(discsToDestroy.length > 0){
			this.animationQueue.push(discsToDestroy);
		}
		
		// Destroy discs
		for (var j = 0; j < discsToDestroy.length; j++) {
			this.destroyDiscAnimation(discsToDestroy[j]);
			discsToDestroy[j].startTween();
		}

		// Animate updated discs
		if(discsToUpdate.length > 0){
			this.animationQueue.push(discsToUpdate);
		}

		for (var k = 0; k < discsToUpdate.length; k++) {
			this.updateDiscAnimation(discsToUpdate[k]);

			//Nothing being destroyed - animate updates
			if(discsToDestroy.length == 0){
				discsToUpdate[k].startTween();
			}
		}

		if(this.animationQueue.length > 0){
			this.isAnimating = true;
		}
		
	}

	destroyDiscAnimation(discSprite){
		discSprite.tweenToDestroy(this.handleDiscAnimationComplete, this);
	}

	updateDiscAnimation(discSprite){
		discSprite.tweenToWorldPosition(this.handleDiscAnimationComplete, this);
	}

	handleDiscAnimationComplete(discSprite, destroyOnComplete = false){
		var currentAnimationGroup =  this.animationQueue[0];

		if(currentAnimationGroup.includes(discSprite)){
			var index = currentAnimationGroup.indexOf(discSprite);
			currentAnimationGroup.splice(index, 1);
		}

		if(destroyOnComplete){
			discSprite.destroy();
		}

		if(currentAnimationGroup.length == 0){
			this.animationQueue.splice(0, 1);
			//Animations complete

			if(this.animationQueue.length == 0){
				this.isAnimating = false;
				// Check for chain combo
				this.game.commander.execute("GameplayCommand.EvaluateGridMatches", this.game);
			}else{
				for (var i = 0; i < this.animationQueue[0].length; i++) {
					var disc = this.animationQueue[0][i];
					disc.startTween();
				}
			}
		}
	}

	cancelDrop(){		
		this.nextDiscSprite = this.previousDiscSprite;
		this.isAnimating = false;
	}

	beginGameOverSequence(){
		this.inGameOverSequence = true;

		var timeline = new TimelineMax({onComplete:this.endGameplay.bind(this)});
		timeline.set("#game-over", {autoAlpha: 1})
		this.container.forEach(function(item) {
			timeline.to(item, 0.4, {alpha : 0.3}, 0);
		});

		timeline.from(".game-over-outline", 1, {drawSVG:"0%", ease:Sine.easeInOut})
		.add("Fill-in")
		.from("#game-over-fill", 1, {alpha:0}, "Fill-in")
		.to(".game-over-outline", 1, {alpha:0}, "Fill-in")
		.to("#game-over", 1, {autoAlpha: 0}, "+=2");
	}


	endGameplay(){
		this.game.commander.execute("GameplayCommand.HandleGameOver", this.game);
	}

}
