export default class extends Phaser.State{

	

	preload(){
		//TODO: restore model from save data
		this.game.model = {}

		window.WebFontConfig = {
			//  The Google Fonts we want to load (specify as many as you like in the array)
			google: {
			  families: ['Permanent Marker']
			}

		};

		this.game.load.audio('break_disc', ['assets/audio/break_disc.m4a', 'assets/audio/break_disc.ogg']);
		this.game.load.audio('drop_disc', ['assets/audio/drop_disc.m4a', 'assets/audio/drop_disc.ogg']);
		this.game.load.audio('game_over', ['assets/audio/game_over.m4a', 'assets/audio/game_over.ogg']);
		this.game.load.audio('new_game', ['assets/audio/new_game.m4a', 'assets/audio/new_game.ogg']);
		this.game.load.audio('new_row', ['assets/audio/break_disc.m4a', 'assets/audio/new_row.ogg']);
		this.game.load.audio('welcome', ['assets/audio/welcome.m4a', 'assets/audio/welcome.ogg']);
		this.game.load.audio('cancel', ['assets/audio/break_disc.m4a', 'assets/audio/cancel.ogg']);

		this.game.load.script('webfont', '//ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js');

		
	}

	create(){
		
		this.game.commander.execute("AppCommand.Bootstrap", this.game);
		this.game.stage.backgroundColor = "#01cdfe";
		this.game.state.start("Splash");
	}

}
