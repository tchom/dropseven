export default class {
	constructor(value, armour = 0){
		this.discValue = value;
		this._armour = armour;
		this._x = 0;
		this._y = 0;
		this._destroyed = false;

		this.onChanged = new Phaser.Signal(); // Triggered when position changes
		this.onArmourChanged = new Phaser.Signal(); // Triggered when armour changes
	}

	get x() {
		return this._x;
	}

	get y() {
		return this._y;
	}

	get destroyed() {
		return this._destroyed;
	}

	get armour() {
		return this._armour;
	}

	set x(value) {
		this._x = value;
	}

	set y(value) {
		this._y = value;
		this.onChanged.dispatch()
	}

	set destroyed(value) {
		this._destroyed = value;

		if(this._destroye){
			this.removeAll()
		}
	}

	set armour(value) {
		this._armour = value;

		if(this._armour < 0){
			this._armour = 0;
		}else{
			this.onArmourChanged.dispatch()
		}
	}
}