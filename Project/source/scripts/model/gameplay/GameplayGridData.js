import GameSettings from '../../settings/GameSettings'

export default class {
	constructor(){
		this.data = []
	}

	updateColumn(x){
		// Evaluate bottom to top
		// IGNORE BOTTOM ROW, can't move down any further
		for (var y = GameSettings.TRUE_GRID_HEIGHT - 2; y > 0; y--) {
			var currentDisc = this.getDiscAtPosition(x, y);
			var discBelow = this.getDiscAtPosition(x, y+1);
			
			if(currentDisc != null && discBelow == null){ // Drop disc down
				var newY = y;
				while(newY < GameSettings.GRID_HEIGHT && discBelow == null){ // Find lowest free slot
					newY += 1;
					var discBelow = this.getDiscAtPosition(x, newY+1);

				}

				this.moveDiscAtPosition(x, y, x, newY);
			}
		}
	}

	// Find matching discs vertically
	checkColumnsForMatches(){
		var matchingColumnDiscs = [];
		var groups = [];

		for (var x = 0; x < GameSettings.GRID_WIDTH; x++) {
			var currentGroup = [];
			var y = 1;

			while(y < GameSettings.TRUE_GRID_HEIGHT){
				var currentGridDisc = this.getDiscAtPosition(x, y);
				if(currentGridDisc != null){
					currentGroup.push(currentGridDisc);
				}

				y++;
			}

			if(currentGroup.length > 0){
				groups.push(currentGroup);
			}
		}

		return this.checkMatchesInGroups(groups);
	}

	// Find matching discs horizontally
	checkRowsForMatches(){
		var groups = [];
		//Create groups of connected discs IGNORE TOP ROW
		for (var y = 1; y < GameSettings.TRUE_GRID_HEIGHT; y++) {
			
			var currentGroup = [];
			var x = 0;

			while(x < GameSettings.GRID_WIDTH){
				var currentGridDisc = this.getDiscAtPosition(x, y);

				if(currentGridDisc == null){ // There is a break in connected discs
					if(currentGroup.length > 0){// Previous group had discs - add to list
						groups.push(currentGroup);
						currentGroup = []; // Create new group
					}
				}else{
					currentGroup.push(currentGridDisc);
				}
				x++;
			}

			if(currentGroup.length > 0){
				groups.push(currentGroup);
			}
		}

		return this.checkMatchesInGroups(groups);
	}

	checkMatchesInGroups(groups){
		var matchingRowDiscs = [];
		for (var i = 0; i < groups.length; i++) {
			var connectedGroup = groups[i];
			var matchValue = connectedGroup.length;

			for (var j = 0; j < connectedGroup.length; j++) {
				var disc = connectedGroup[j];
				if(matchValue == disc.discValue && disc.armour <= 0){
					matchingRowDiscs.push(disc);
				}
			}
		}

		return matchingRowDiscs;
	}

	dropInColumn(gridDiscData, x){
		var freeRow = this.firstFreeRow(x);
		if(freeRow == 0){
			//Row is full
			return false;
		}else{
			this.addDiscAtPosition(gridDiscData, x, freeRow)
			return true;
		}
	}

	firstFreeRow(x){
		var firstFreeRow = 0;
		for (var y = GameSettings.TRUE_GRID_HEIGHT - 1; y >= 0; y--) {
			if(this.getDiscAtPosition(x, y) == null){
				
				firstFreeRow = y;
				break;
			}
		}
		return firstFreeRow;
	}

	addDiscAtPosition(gridDiscData, x, y){
		gridDiscData.x = x;
		gridDiscData.y = y;
		this.data[x + y*GameSettings.GRID_WIDTH] = gridDiscData;
	}

	moveDiscAtPosition(x, y, newX, newY){
		var gridDiscData = this.getDiscAtPosition(x, y);
		this.removeDiscAtPosition(x, y);
		this.addDiscAtPosition(gridDiscData, newX, newY);
	}

	removeDiscAtPosition(x, y){
		this.data[x + y*GameSettings.GRID_WIDTH] = null;
	}

	getDiscAtPosition(x, y){
		if(!this.inBounds(x, y)){
			return null;
		}
		return this.data[x + y*GameSettings.GRID_WIDTH];
	}

	// Check coordinates are within the grid
	inBounds(x, y){
		if(x < 0 || x >= GameSettings.GRID_WIDTH ||
			y < 1 || y >= GameSettings.TRUE_GRID_HEIGHT){
			return false;
		}else{
			return true;
		}
	}

	// Move all grid items up one (end of round)
	shiftUpAll(){
		for (var i = 0; i < this.data.length; i++) {
			if(this.data[i] != null){
				this.moveDiscAtPosition(this.data[i].x, this.data[i].y, this.data[i].x, this.data[i].y-1);
			}
		}
	}

	//Print grid array to console (debugging)
	printGridToConsole(){
		for (var y = 0; y < GameSettings.TRUE_GRID_HEIGHT; y++) {
			var rowString = ""

			for (var x = 0; x < GameSettings.GRID_WIDTH; x++) {
				if(y == 0){
					rowString += "- ";
				}else{
					var discData = this.getDiscAtPosition(x, y)
					if(discData != null){
						rowString += discData.discValue + " ";
					}else{
						rowString += "x ";
					}
				}
			}

			console.log(rowString);
		}
	}
}