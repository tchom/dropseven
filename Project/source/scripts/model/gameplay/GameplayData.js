import GameSettings from '../../settings/GameSettings'

export default class {
	constructor(){
		this.score = 0;
		this.combo = 0;
		this.turnsUntilNextRound = GameSettings.TURNS_BETWEEN_ROUNDS;
	}

	increaseScore(){
		this.score += GameSettings.BASE_DISC_POINTS + (GameSettings.POINTS_MULTIPLIER * this.combo);
	}

	increaseCombo(){
		this.combo++;
	}

	resetCombo(){
		this.combo = 0;
	}

	getComboValue(){
		return  GameSettings.BASE_DISC_POINTS + (GameSettings.POINTS_MULTIPLIER * this.combo);
	}

	nextTurn(){
		this.turnsUntilNextRound--;
	}

	isRoundComplete(){
		return this.turnsUntilNextRound == 0;
	}

	resetRound(){
		this.turnsUntilNextRound = GameSettings.TURNS_BETWEEN_ROUNDS;
	}
}