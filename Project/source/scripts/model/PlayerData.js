
export default class {
	constructor(){
		this.highscore = 0;
	}

	compareScore(newScore){
		if(newScore > this.highscore){
			this.highscore = newScore;
		}
	}
}