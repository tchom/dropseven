export default class{


	static get GRID_WIDTH() {
		return 7;
	}

	static get GRID_HEIGHT() {
		return 7;
	}

	// The true grid height is one row taller than the display height. If there is 
	// a disc in the top row, the grid has overflowed and the game is over
	static get TRUE_GRID_HEIGHT() {
		return 8;
	}

	static get GRID_TILE_SIZE() {
		return 80;
	}

	static get GRID_TILE_SIZE() {
		return 80;
	}

	static get STARTING_DISCS() {
		return 12;
	}

	static get STARTING_COLUMN_HEIGHT_LIMIT() {
		return 4;
	}


	static get MIN_DISC_VALUE() {
		return 1;
	}

	static get MAX_DISC_VALUE() {
		return 7;
	}

	static get BASE_DISC_POINTS() {
		return 5;
	}

	static get POINTS_MULTIPLIER() {
		return 23;
	}

	static get CHANCE_OF_ARMOUR_DISC() {
		return 0.1;
	}

	static get DEFAULT_ARMOUR_VALUE() {
		return 2;
	}

	static get TURNS_BETWEEN_ROUNDS() {
		return 12;
	}
}