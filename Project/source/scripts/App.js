import BootState from './states/BootState'
import MainMenuState from './states/MainMenuState'
import GameplayState from './states/gameplay/GameplayState'
import SplashState from './states/SplashState'

import GameCommander from './commands/GameCommander'

class Game extends Phaser.Game {
	constructor(){
		/*var winW = window.innerWidth * window.devicePixelRatio;
		var winH = window.innerHeight * window.devicePixelRatio;*/

		var winW = 750;
		var winH = 1334;

		var config = {
			type: Phaser.AUTO,
			width: winW,
			height: winH,
			parent: "app-container"
		};

		super(config);

		this.state.add("Boot", BootState);
		this.state.add("MainMenu", MainMenuState);
		this.state.add("Gameplay", GameplayState);
		this.state.add("Splash", SplashState);

		this.commander = new GameCommander();

		this.state.start("Boot");

	}
}

var game = new Game();