import Command from '../Command'


export default class extends Command {

	constructor(gameplayState){
		super();
		this.gameplayState = gameplayState;
	}

	execute(game, gridDiscData, forceWorldPosition = false){
		var sprite = this.gameplayState.createNewGridDiscSprite(gridDiscData);

		if(forceWorldPosition){
			sprite.gridPositionToWorldPosition();
		}

		return true;
	}

	static get NAME() {
		return "GameplayCommand.CreateDiscSprite";
	}
}

