import Command from '../Command'


export default class extends Command {

	constructor(){
		super();
	}

	execute(game){
		
		game.model.gameplay.resetRound();
		game.commander.execute("AppCommand.PlaySound", "new_row");
		game.commander.execute("GameplayCommand.AddBottomRowDiscs", game);

		return true;
	}

	static get NAME() {
		return "GameplayCommand.HandleTurnEnd";
	}
}

