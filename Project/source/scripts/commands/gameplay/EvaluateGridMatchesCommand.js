import Command from '../Command'
import GameSettings from '../../settings/GameSettings'

export default class extends Command {

	constructor(){
		super();
		
	}

	execute(game, callback){
		// get row matches
		var grid = game.model.gridData;
		var rowMatches = grid.checkRowsForMatches();
		var columnMatches = grid.checkColumnsForMatches();
		var allMatches = rowMatches.concat(columnMatches);

		var columnsToUpdate = [];
		for (var i = 0; i < allMatches.length; i++) {

			game.model.gameplay.increaseScore();

			var matchedDisc = allMatches[i];
			if(!columnsToUpdate.includes(matchedDisc.x)){
				columnsToUpdate.push(matchedDisc.x);
			}

			grid.removeDiscAtPosition(matchedDisc.x, matchedDisc.y);
			this.breakAdjacentArmour(grid, matchedDisc.x, matchedDisc.y);
			matchedDisc.destroyed = true;
			game.commander.execute("GameplayCommand.CreatePointsSprite", game.model.gameplay.getComboValue(), matchedDisc.x, matchedDisc.y);
		}

		for (var j = 0; j < columnsToUpdate.length; j++) {
			var x = columnsToUpdate[j];
			grid.updateColumn(x);
		}

		game.commander.execute("GameplayCommand.CheckForGameOver", game);

		if(allMatches.length == 0){
			//Grid stabilised - add next disc
			if(game.model.gameplay.isRoundComplete()){
				game.commander.execute("GameplayCommand.HandleTurnEnd", game);
			}else{
				game.model.gameplay.nextTurn();
				game.commander.execute("GameplayCommand.CreateNextGridDisc", game);
				game.model.gameplay.resetCombo();
			}
		}else{
			game.commander.execute("AppCommand.PlaySound", "break_disc");
		}
		
		return allMatches.length > 0;
	}

	breakAdjacentArmour(grid, x, y){
		// get left
		if(x > 0){
			var leftData = grid.getDiscAtPosition(x-1, y);

			if(leftData != null){
				leftData.armour--;
			}
		}
		// get right
		if(x < GameSettings.GRID_WIDTH-1){
			var rightData = grid.getDiscAtPosition(x+1, y);

			if(rightData != null){
				rightData.armour--;
			}
		}
		//get top
		if(y > 0){
			var topData = grid.getDiscAtPosition(x, y-1);

			if(topData != null){
				topData.armour--;
			}
		}
		//get bottom
		if(y < GameSettings.TRUE_GRID_HEIGHT-1){
			var bottomData = grid.getDiscAtPosition(x, y+1);

			if(bottomData != null){
				bottomData.armour--;
			}
		}
	}

	static get NAME() {
		return "GameplayCommand.EvaluateGridMatches";
	}
}

