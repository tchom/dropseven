import Command from '../Command'

export default class extends Command {

	constructor(){
		super();
		
	}

	execute(game, discData, column, gamestate){
		discData.x = column;

		var dropSuccessful = game.commander.execute("GameplayCommand.DropGridDisc", game, discData, false);
		
		if(dropSuccessful){
			game.commander.execute("AppCommand.PlaySound", "drop_disc");
			gamestate.handleAnimations();
			return true;
		}else{
			game.commander.execute("AppCommand.PlaySound", "cancel");

			gamestate.cancelDrop();
			return false;
		}
	}

	static get NAME() {
		return "GameplayCommand.DropNextGridDisc";
	}
}

