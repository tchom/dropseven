import Command from '../Command'

export default class extends Command {

	constructor(){
		super();
	}

	execute(game){
		var score = game.model.gameplay.score;
		game.model.player.compareScore(score);
		game.state.start("MainMenu");

		return true;
	}

	static get NAME() {
		return "GameplayCommand.HandleGameOver";
	}
}

