import Command from '../Command'

import GameplayGridDiscData from '../../model/gameplay/GameplayGridDiscData'
import GameSettings from '../../settings/GameSettings'

export default class extends Command {

	constructor(gameplayState){
		super();
		this.gameplayState = gameplayState;
	}

	execute(game){
		var min = GameSettings.MIN_DISC_VALUE;
		var max = GameSettings.MAX_DISC_VALUE;

		var discValue = Math.floor(Math.random() * (max - min + 1)) + min;
		var armourValue = (Math.random() <  GameSettings.CHANCE_OF_ARMOUR_DISC)? GameSettings.DEFAULT_ARMOUR_VALUE : 0;


		var discData = new GameplayGridDiscData(discValue, armourValue);
		this.gameplayState.createNextDiscSprite(discData);

		return true;
	}

	static get NAME() {
		return "GameplayCommand.CreateNextGridDisc";
	}
}

