import Command from '../Command'

export default class extends Command {

	constructor(gameplayState){
		super();
		this.gameplayState = gameplayState;
	}

	execute(points, x, y){
		this.gameplayState.createPointsSprite(points, x, y);

		return true;
	}

	static get NAME() {
		return "GameplayCommand.CreatePointsSprite";
	}
}

