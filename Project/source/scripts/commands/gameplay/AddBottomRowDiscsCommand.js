import Command from '../Command'
import GameSettings from '../../settings/GameSettings'
import GameplayGridDiscData from '../../model/gameplay/GameplayGridDiscData'


export default class extends Command {

	constructor(gameplayState){
		super();
		this.gameplayState = gameplayState;
	}

	execute(game){
		var grid = game.model.gridData;
		grid.shiftUpAll();

		for (var x = 0; x < GameSettings.GRID_WIDTH; x++) {
			var discData = this.createDisc(grid, x, GameSettings.TRUE_GRID_HEIGHT-1);
			game.commander.execute("GameplayCommand.CreateDiscSprite", game, discData, true);
		}

		game.commander.execute("GameplayCommand.AnimateDiscs");

		return true;
	}

	createDisc(grid, x, y){
		var min = GameSettings.MIN_DISC_VALUE;
		var max = GameSettings.MAX_DISC_VALUE;

		var discValue = Math.floor(Math.random() * (max - min + 1)) + min;

		var discData = new GameplayGridDiscData(discValue, GameSettings.DEFAULT_ARMOUR_VALUE)
		grid.addDiscAtPosition(discData, x, y);

		return discData;

	}

	static get NAME() {
		return "GameplayCommand.AddBottomRowDiscs";
	}
}

