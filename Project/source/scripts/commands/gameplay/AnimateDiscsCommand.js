import Command from '../Command'


export default class extends Command {

	constructor(gameplayState){
		super();
		this.gameplaySprite = gameplayState;
	}

	execute(game){
		this.gameplaySprite.handleAnimations();

		return true;
	}

	static get NAME() {
		return "GameplayCommand.AnimateDiscs";
	}
}

