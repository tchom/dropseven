import Command from '../Command'
import GameSettings from '../../settings/GameSettings'

export default class extends Command {

	constructor(gameplayState){
		super();
		this.gameplayState = gameplayState;
	}

	execute(game){
		var grid = game.model.gridData;
		//Game over if grid full or discs in top row
		var isGameOver = true;

		for (var i = 0; i < GameSettings.GRID_WIDTH * GameSettings.TRUE_GRID_HEIGHT; i++) {
			var discData = grid.data[i];
			var x = i % GameSettings.GRID_WIDTH;
			var y = Math.floor(i / GameSettings.GRID_WIDTH);

			if(y == 0){
				if(discData != null){
					isGameOver = true;
					break;
				}
			}else{
				if(discData == null){
					isGameOver = false;
					break;
				}
			}
		}

		if(isGameOver){
			this.gameplayState.beginGameOverSequence();
			game.commander.execute("AppCommand.PlaySound", "game_over");

			return true;
		}else{
			game.model.gameplay.increaseCombo();
			game.commander.execute("GameplayCommand.AnimateDiscs");

			return false;
		}
	}

	static get NAME() {
		return "GameplayCommand.CheckForGameOver";
	}
}

