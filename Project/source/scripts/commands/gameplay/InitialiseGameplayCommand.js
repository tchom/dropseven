import Command from '../Command'
import CreateDiscSpriteCommand from './CreateDiscSpriteCommand'
import CreatePointsSpriteCommand from './CreatePointsSpriteCommand'
import AnimateDiscsCommand from './AnimateDiscsCommand'
import CreateNextGridDiscCommand from './CreateNextGridDiscCommand'
import AddBottomRowDiscsCommand from './AddBottomRowDiscsCommand'
import GameplayData from '../../model/gameplay/GameplayData'
import CheckForGameOverCommand from './CheckForGameOverCommand'



export default class extends Command {

	constructor(){
		super();

	}

	execute(game, gameplayState){
		game.commander.registerCommand(CreateDiscSpriteCommand.NAME, new CreateDiscSpriteCommand(gameplayState), true);
		game.commander.registerCommand(AnimateDiscsCommand.NAME, new AnimateDiscsCommand(gameplayState), true);
		game.commander.registerCommand(CreateNextGridDiscCommand.NAME, new CreateNextGridDiscCommand(gameplayState), true);
		game.commander.registerCommand(AddBottomRowDiscsCommand.NAME, new AddBottomRowDiscsCommand(gameplayState), true);
		game.commander.registerCommand(CheckForGameOverCommand.NAME, new CheckForGameOverCommand(gameplayState), true);
		game.commander.registerCommand(CreatePointsSpriteCommand.NAME, new CreatePointsSpriteCommand(gameplayState), true);
		
		game.model.gameplay = new GameplayData();
		game.commander.execute("GameplayCommand.GenerateGridData", game);
		game.commander.execute("GameplayCommand.EvaluateGridMatches", game);

		return true;
	}

	static get NAME() {
		return "GameplayCommand.InitialiseGameplay";
	}
}

