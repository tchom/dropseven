import Command from '../Command'
import GameplayGridData from '../../model/gameplay/GameplayGridData'
import GameplayGridDiscData from '../../model/gameplay/GameplayGridDiscData'
import GameSettings from '../../settings/GameSettings'

export default class extends Command {

	constructor(){
		super();
		
	}

	execute(game){
		var newGrid = new GameplayGridData();
		game.model.gridData = newGrid;

		for (var i = 0; i < GameSettings.STARTING_DISCS; i++) {
			var randomDisc = this.createRandomDisc(newGrid);
			game.commander.execute("GameplayCommand.CreateDiscSprite", game, randomDisc, true);
		}

		return true;
	}

	createRandomDisc(grid){
		var min = GameSettings.MIN_DISC_VALUE;
		var max = GameSettings.MAX_DISC_VALUE;

		var discValue = Math.floor(Math.random() * (max - min + 1)) + min;

		// Prevent grid from overflowing on first turn
		var lowestRow = GameSettings.TRUE_GRID_HEIGHT - GameSettings.STARTING_COLUMN_HEIGHT_LIMIT;

		var columnToDropIn = this.getRandomColumn();
		var lowestSlotInColumn = grid.firstFreeRow(columnToDropIn);
		
		while(lowestSlotInColumn < lowestRow){
			columnToDropIn = this.getRandomColumn();
			lowestSlotInColumn = grid.firstFreeRow(columnToDropIn)
		}

		var armourValue = (Math.random() <  GameSettings.CHANCE_OF_ARMOUR_DISC)? GameSettings.DEFAULT_ARMOUR_VALUE : 0;

		var discData = new GameplayGridDiscData(discValue, armourValue)
		grid.dropInColumn(discData, columnToDropIn);

		return discData;

	}

	getRandomColumn(){
		return Math.floor(Math.random() * GameSettings.GRID_WIDTH);
	}

	static get NAME() {
		return "GameplayCommand.GenerateGridData";
	}
}

