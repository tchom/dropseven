import Command from '../Command'
import GameplayGridDiscData from '../../model/gameplay/GameplayGridDiscData'

export default class extends Command {

	constructor(){
		super();
		
	}

	execute(game, discData, createSprite = false){
		console.log("Dropping a " + discData.discValue + " in column " + discData.x);

		if (game.model.gridData.dropInColumn(discData, discData.x)){
			if(createSprite){
				game.commander.execute("GameplayCommand.CreateDiscSprite", game, discData);
			}
			
			return true;
		 }else{
			return false;
		 }

		
	}

	static get NAME() {
		return "GameplayCommand.DropGridDisc";
	}
}

