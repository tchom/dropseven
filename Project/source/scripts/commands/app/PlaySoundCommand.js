import Command from '../Command'


export default class extends Command {

	constructor(game){
		super();

		this.sounds = {}

		this.sounds["break_disc"] = game.add.audio('new_game');
		this.sounds["drop_disc"] = game.add.audio('drop_disc');
		this.sounds["game_over"] = game.add.audio('game_over');
		this.sounds["new_game"] = game.add.audio('new_game');
		this.sounds["new_row"] = game.add.audio('new_row');
		this.sounds["welcome"] = game.add.audio('welcome');
		this.sounds["cancel"] = game.add.audio('cancel');
	}

	execute(soundName){
		if(soundName in this.sounds){
			this.sounds[soundName].play();
			return true;
		}else{
			console.log("Unknown sound: " + soundName);
			return false;
		}
	}

	static get NAME() {
		return "AppCommand.PlaySound";
	}
}

