import Command from '../Command'

import PlayerData from '../../model/PlayerData'
import GenerateGridDataCommand from '../gameplay/GenerateGridDataCommand'
import InitialiseGameplayCommand from '../gameplay/InitialiseGameplayCommand'
import CreateNextGridDiscCommand from '../gameplay/CreateNextGridDiscCommand'
import DropGridDiscCommand from '../gameplay/DropGridDiscCommand'
import DropNextGridDiscCommand from '../gameplay/DropNextGridDiscCommand'
import EvaluateGridMatchesCommand from '../gameplay/EvaluateGridMatchesCommand'
import HandleTurnEndCommand from '../gameplay/HandleTurnEndCommand'
import HandleGameOverCommand from '../gameplay/HandleGameOverCommand'
import PlaySoundCommand from './PlaySoundCommand'


export default class extends Command {

	constructor(){
		super();
		
		
	}

	execute(game){
		console.log("BootstrapCommand");
		game.model.player = new PlayerData();

		// create gamplay commands
		game.commander.registerCommand(PlaySoundCommand.NAME, new PlaySoundCommand(game));
		game.commander.registerCommand(InitialiseGameplayCommand.NAME, new InitialiseGameplayCommand(game));
		game.commander.registerCommand(GenerateGridDataCommand.NAME, new GenerateGridDataCommand());
		game.commander.registerCommand(DropGridDiscCommand.NAME, new DropGridDiscCommand());
		game.commander.registerCommand(DropNextGridDiscCommand.NAME, new DropNextGridDiscCommand());
		game.commander.registerCommand(EvaluateGridMatchesCommand.NAME, new EvaluateGridMatchesCommand());
		game.commander.registerCommand(HandleTurnEndCommand.NAME, new HandleTurnEndCommand());
		game.commander.registerCommand(HandleGameOverCommand.NAME, new HandleGameOverCommand());
		
		return true;

	}

	static get NAME() {
		return "AppCommand.Bootstrap";
	}
}

