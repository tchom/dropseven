import Command from './Command'
import BootstrapCommand from './app/BootstrapCommand'

export default class {
	constructor(){
		this.commands = {}
		this.registerCommand(BootstrapCommand.NAME, new BootstrapCommand());
	}

	registerCommand(name, command, override = false){

		if((name in this.commands && !override) || !command instanceof Command){
			return;
		}

		console.log("Registered " + name);

		this.commands[name] = command;
	}

	execute(name, ...args){
		if(name in this.commands){
			//console.log("Execute: " + name);
			return this.commands[name].execute(...args);
		}else{
			console.log("Unknown command: " + name);
			return false;
		}
		
	}
}