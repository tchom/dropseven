var del = require('del');
var gulp = require('gulp');
var path = require('path');
var argv = require('yargs').argv;
var gutil = require('gulp-util');
var source = require('vinyl-source-stream');
var buffer = require('gulp-buffer');
var uglify = require('gulp-uglify');
var gulpif = require('gulp-if');
var babelify = require('babelify');
var browserify = require('browserify');

var sass = require('gulp-sass');
var cssnano = require('gulp-cssnano');
var imagemin = require('gulp-imagemin');
var cache = require('gulp-cache');


/**
 * Using different folders/file names? Change these constants:
 */
var BUILD_PATH = './build';
var SOURCE_PATH = './source';
var LIBRARIES_PATH = '/lib';
var SCRIPTS_PATH = '/scripts';
var ASSETS_PATH = SOURCE_PATH + '/assets';
var ENTRY_FILE = SOURCE_PATH + SCRIPTS_PATH + '/App.js';
var OUTPUT_FILE = 'App.js';

var ASSETS_OUTPUT_PATH = BUILD_PATH+'/assets';
var ASSETS_IMAGES_OUTPUT_PATH = ASSETS_OUTPUT_PATH + '/images';
var ASSETS_AUDIO_OUTPUT_PATH = ASSETS_OUTPUT_PATH + '/audio';
var ASSETS_FONTS_OUTPUT_PATH = ASSETS_OUTPUT_PATH + '/fonts';


var keepFiles = false;

/**
 * Simple way to check for development/production mode.
 */
function isProduction() {
	return argv.production;
}

/**
 * Logs the current build mode on the console.
 */
function logBuildMode() {
	
	if (isProduction()) {
		gutil.log(gutil.colors.green('Running production build...'));
	} else {
		gutil.log(gutil.colors.yellow('Running development build...'));
	}

}

/**
 * Deletes all content inside the './build' folder.
 */
function clean() {
	return del(['build/**/*.*']);
}


/**
 * Transforms ES2015 code into ES5 code.
 */
function buildJS() {

	logBuildMode();

	return browserify({
			paths: [path.join(__dirname, 'src')],
			entries: ENTRY_FILE,
			debug: true,
			transform: [
				[
					babelify, {
						presets: ["es2015"]
					}
				]
			]
		})
		.transform(babelify)
		.bundle().on('error', function(error) {
			gutil.log(gutil.colors.red('[Build Error]', error.message));
			this.emit('end');
		})
		.pipe(source(OUTPUT_FILE))
		.pipe(buffer())
		.pipe(gulpif(isProduction(), uglify()))
		.pipe(gulp.dest(BUILD_PATH));
}

function packageHTML() {
	return gulp.src(SOURCE_PATH+'/*.html')
	.pipe(gulp.dest(BUILD_PATH))
}

function packageLibraries() {
	return gulp.src(SOURCE_PATH+ LIBRARIES_PATH+'/*.js')
	.pipe(gulp.dest(BUILD_PATH + LIBRARIES_PATH))
   //return tasks;
}

function packageSass(){
	return gulp.src(SOURCE_PATH+'/sass/**/*.scss')
		.pipe(sass().on('error', sass.logError))
		.pipe(cssnano())
		.pipe(gulp.dest(BUILD_PATH))
}

function packageAssetsImages(){
	return gulp.src(ASSETS_PATH+'/images/**/*.+(png|jpg|jpeg|gif|svg|json)')
  // Caching images that ran through imagemin
  .pipe(cache(imagemin({
      interlaced: true
    })))
  .pipe(gulp.dest(ASSETS_IMAGES_OUTPUT_PATH))
}

function packageAssetsAudio(){
	return gulp.src(ASSETS_PATH+'/audio/**/*.+(ogg|mp4|m4a)')
  .pipe(gulp.dest(ASSETS_AUDIO_OUTPUT_PATH))
}


gulp.task('clean', clean);
gulp.task('buildJS', buildJS);
gulp.task('package-html', packageHTML);
gulp.task('package-lib', packageLibraries);
gulp.task('package-sass', packageSass);
gulp.task('package-assets-images', packageAssetsImages);
gulp.task('package-assets-audio', packageAssetsAudio);
gulp.task('fastBuild', buildJS);

/**
 * 
 * Read more about task dependencies in Gulp: 
 * https://medium.com/@dave_lunny/task-dependencies-in-gulp-b885c1ab48f0
 */
gulp.task('full-build', ['clean'], function () {
	gulp.start(['package-html', 'package-lib', 'package-sass', 'package-assets-images', 'package-assets-audio', 'buildJS']);
});

gulp.task('default', ['buildJS']);