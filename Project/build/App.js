(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
'use strict';

var _BootState = require('./states/BootState');

var _BootState2 = _interopRequireDefault(_BootState);

var _MainMenuState = require('./states/MainMenuState');

var _MainMenuState2 = _interopRequireDefault(_MainMenuState);

var _GameplayState = require('./states/gameplay/GameplayState');

var _GameplayState2 = _interopRequireDefault(_GameplayState);

var _SplashState = require('./states/SplashState');

var _SplashState2 = _interopRequireDefault(_SplashState);

var _GameCommander = require('./commands/GameCommander');

var _GameCommander2 = _interopRequireDefault(_GameCommander);

function _interopRequireDefault(obj) {
	return obj && obj.__esModule ? obj : { default: obj };
}

function _classCallCheck(instance, Constructor) {
	if (!(instance instanceof Constructor)) {
		throw new TypeError("Cannot call a class as a function");
	}
}

function _possibleConstructorReturn(self, call) {
	if (!self) {
		throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
	}return call && (typeof call === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
	if (typeof superClass !== "function" && superClass !== null) {
		throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
	}subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var Game = function (_Phaser$Game) {
	_inherits(Game, _Phaser$Game);

	function Game() {
		_classCallCheck(this, Game);

		/*var winW = window.innerWidth * window.devicePixelRatio;
  var winH = window.innerHeight * window.devicePixelRatio;*/

		var winW = 750;
		var winH = 1334;

		var config = {
			type: Phaser.AUTO,
			width: winW,
			height: winH,
			parent: "app-container"
		};

		var _this = _possibleConstructorReturn(this, (Game.__proto__ || Object.getPrototypeOf(Game)).call(this, config));

		_this.state.add("Boot", _BootState2.default);
		_this.state.add("MainMenu", _MainMenuState2.default);
		_this.state.add("Gameplay", _GameplayState2.default);
		_this.state.add("Splash", _SplashState2.default);

		_this.commander = new _GameCommander2.default();

		_this.state.start("Boot");

		return _this;
	}

	return Game;
}(Phaser.Game);

var game = new Game();

},{"./commands/GameCommander":3,"./states/BootState":24,"./states/MainMenuState":25,"./states/SplashState":26,"./states/gameplay/GameplayState":27}],2:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () {
	function defineProperties(target, props) {
		for (var i = 0; i < props.length; i++) {
			var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
		}
	}return function (Constructor, protoProps, staticProps) {
		if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
	};
}();

function _classCallCheck(instance, Constructor) {
	if (!(instance instanceof Constructor)) {
		throw new TypeError("Cannot call a class as a function");
	}
}

/* ABSTRACT CLASS */
var _class = function () {
	function _class() {
		_classCallCheck(this, _class);
	}

	_createClass(_class, [{
		key: "execute",
		value: function execute() {
			return false; // Return 'success'
		}
	}], [{
		key: "NAME",
		get: function get() {
			return "abstract_class";
		}
	}]);

	return _class;
}();

exports.default = _class;

},{}],3:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () {
	function defineProperties(target, props) {
		for (var i = 0; i < props.length; i++) {
			var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
		}
	}return function (Constructor, protoProps, staticProps) {
		if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
	};
}();

var _Command = require('./Command');

var _Command2 = _interopRequireDefault(_Command);

var _BootstrapCommand = require('./app/BootstrapCommand');

var _BootstrapCommand2 = _interopRequireDefault(_BootstrapCommand);

function _interopRequireDefault(obj) {
	return obj && obj.__esModule ? obj : { default: obj };
}

function _classCallCheck(instance, Constructor) {
	if (!(instance instanceof Constructor)) {
		throw new TypeError("Cannot call a class as a function");
	}
}

var _class = function () {
	function _class() {
		_classCallCheck(this, _class);

		this.commands = {};
		this.registerCommand(_BootstrapCommand2.default.NAME, new _BootstrapCommand2.default());
	}

	_createClass(_class, [{
		key: 'registerCommand',
		value: function registerCommand(name, command) {
			var override = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

			if (name in this.commands && !override || !command instanceof _Command2.default) {
				return;
			}

			console.log("Registered " + name);

			this.commands[name] = command;
		}
	}, {
		key: 'execute',
		value: function execute(name) {
			if (name in this.commands) {
				var _commands$name;

				for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
					args[_key - 1] = arguments[_key];
				}

				//console.log("Execute: " + name);
				return (_commands$name = this.commands[name]).execute.apply(_commands$name, args);
			} else {
				console.log("Unknown command: " + name);
				return false;
			}
		}
	}]);

	return _class;
}();

exports.default = _class;

},{"./Command":2,"./app/BootstrapCommand":4}],4:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () {
	function defineProperties(target, props) {
		for (var i = 0; i < props.length; i++) {
			var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
		}
	}return function (Constructor, protoProps, staticProps) {
		if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
	};
}();

var _Command2 = require('../Command');

var _Command3 = _interopRequireDefault(_Command2);

var _PlayerData = require('../../model/PlayerData');

var _PlayerData2 = _interopRequireDefault(_PlayerData);

var _GenerateGridDataCommand = require('../gameplay/GenerateGridDataCommand');

var _GenerateGridDataCommand2 = _interopRequireDefault(_GenerateGridDataCommand);

var _InitialiseGameplayCommand = require('../gameplay/InitialiseGameplayCommand');

var _InitialiseGameplayCommand2 = _interopRequireDefault(_InitialiseGameplayCommand);

var _CreateNextGridDiscCommand = require('../gameplay/CreateNextGridDiscCommand');

var _CreateNextGridDiscCommand2 = _interopRequireDefault(_CreateNextGridDiscCommand);

var _DropGridDiscCommand = require('../gameplay/DropGridDiscCommand');

var _DropGridDiscCommand2 = _interopRequireDefault(_DropGridDiscCommand);

var _DropNextGridDiscCommand = require('../gameplay/DropNextGridDiscCommand');

var _DropNextGridDiscCommand2 = _interopRequireDefault(_DropNextGridDiscCommand);

var _EvaluateGridMatchesCommand = require('../gameplay/EvaluateGridMatchesCommand');

var _EvaluateGridMatchesCommand2 = _interopRequireDefault(_EvaluateGridMatchesCommand);

var _HandleTurnEndCommand = require('../gameplay/HandleTurnEndCommand');

var _HandleTurnEndCommand2 = _interopRequireDefault(_HandleTurnEndCommand);

var _HandleGameOverCommand = require('../gameplay/HandleGameOverCommand');

var _HandleGameOverCommand2 = _interopRequireDefault(_HandleGameOverCommand);

var _PlaySoundCommand = require('./PlaySoundCommand');

var _PlaySoundCommand2 = _interopRequireDefault(_PlaySoundCommand);

function _interopRequireDefault(obj) {
	return obj && obj.__esModule ? obj : { default: obj };
}

function _classCallCheck(instance, Constructor) {
	if (!(instance instanceof Constructor)) {
		throw new TypeError("Cannot call a class as a function");
	}
}

function _possibleConstructorReturn(self, call) {
	if (!self) {
		throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
	}return call && (typeof call === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
	if (typeof superClass !== "function" && superClass !== null) {
		throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
	}subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var _class = function (_Command) {
	_inherits(_class, _Command);

	function _class() {
		_classCallCheck(this, _class);

		return _possibleConstructorReturn(this, (_class.__proto__ || Object.getPrototypeOf(_class)).call(this));
	}

	_createClass(_class, [{
		key: 'execute',
		value: function execute(game) {
			console.log("BootstrapCommand");
			game.model.player = new _PlayerData2.default();

			// create gamplay commands
			game.commander.registerCommand(_PlaySoundCommand2.default.NAME, new _PlaySoundCommand2.default(game));
			game.commander.registerCommand(_InitialiseGameplayCommand2.default.NAME, new _InitialiseGameplayCommand2.default(game));
			game.commander.registerCommand(_GenerateGridDataCommand2.default.NAME, new _GenerateGridDataCommand2.default());
			game.commander.registerCommand(_DropGridDiscCommand2.default.NAME, new _DropGridDiscCommand2.default());
			game.commander.registerCommand(_DropNextGridDiscCommand2.default.NAME, new _DropNextGridDiscCommand2.default());
			game.commander.registerCommand(_EvaluateGridMatchesCommand2.default.NAME, new _EvaluateGridMatchesCommand2.default());
			game.commander.registerCommand(_HandleTurnEndCommand2.default.NAME, new _HandleTurnEndCommand2.default());
			game.commander.registerCommand(_HandleGameOverCommand2.default.NAME, new _HandleGameOverCommand2.default());

			return true;
		}
	}], [{
		key: 'NAME',
		get: function get() {
			return "AppCommand.Bootstrap";
		}
	}]);

	return _class;
}(_Command3.default);

exports.default = _class;

},{"../../model/PlayerData":19,"../Command":2,"../gameplay/CreateNextGridDiscCommand":10,"../gameplay/DropGridDiscCommand":12,"../gameplay/DropNextGridDiscCommand":13,"../gameplay/EvaluateGridMatchesCommand":14,"../gameplay/GenerateGridDataCommand":15,"../gameplay/HandleGameOverCommand":16,"../gameplay/HandleTurnEndCommand":17,"../gameplay/InitialiseGameplayCommand":18,"./PlaySoundCommand":5}],5:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () {
	function defineProperties(target, props) {
		for (var i = 0; i < props.length; i++) {
			var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
		}
	}return function (Constructor, protoProps, staticProps) {
		if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
	};
}();

var _Command2 = require('../Command');

var _Command3 = _interopRequireDefault(_Command2);

function _interopRequireDefault(obj) {
	return obj && obj.__esModule ? obj : { default: obj };
}

function _classCallCheck(instance, Constructor) {
	if (!(instance instanceof Constructor)) {
		throw new TypeError("Cannot call a class as a function");
	}
}

function _possibleConstructorReturn(self, call) {
	if (!self) {
		throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
	}return call && (typeof call === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
	if (typeof superClass !== "function" && superClass !== null) {
		throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
	}subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var _class = function (_Command) {
	_inherits(_class, _Command);

	function _class(game) {
		_classCallCheck(this, _class);

		var _this = _possibleConstructorReturn(this, (_class.__proto__ || Object.getPrototypeOf(_class)).call(this));

		_this.sounds = {};

		_this.sounds["break_disc"] = game.add.audio('new_game');
		_this.sounds["drop_disc"] = game.add.audio('drop_disc');
		_this.sounds["game_over"] = game.add.audio('game_over');
		_this.sounds["new_game"] = game.add.audio('new_game');
		_this.sounds["new_row"] = game.add.audio('new_row');
		_this.sounds["welcome"] = game.add.audio('welcome');
		_this.sounds["cancel"] = game.add.audio('cancel');
		return _this;
	}

	_createClass(_class, [{
		key: 'execute',
		value: function execute(soundName) {
			if (soundName in this.sounds) {
				this.sounds[soundName].play();
				return true;
			} else {
				console.log("Unknown sound: " + soundName);
				return false;
			}
		}
	}], [{
		key: 'NAME',
		get: function get() {
			return "AppCommand.PlaySound";
		}
	}]);

	return _class;
}(_Command3.default);

exports.default = _class;

},{"../Command":2}],6:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () {
	function defineProperties(target, props) {
		for (var i = 0; i < props.length; i++) {
			var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
		}
	}return function (Constructor, protoProps, staticProps) {
		if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
	};
}();

var _Command2 = require('../Command');

var _Command3 = _interopRequireDefault(_Command2);

var _GameSettings = require('../../settings/GameSettings');

var _GameSettings2 = _interopRequireDefault(_GameSettings);

var _GameplayGridDiscData = require('../../model/gameplay/GameplayGridDiscData');

var _GameplayGridDiscData2 = _interopRequireDefault(_GameplayGridDiscData);

function _interopRequireDefault(obj) {
	return obj && obj.__esModule ? obj : { default: obj };
}

function _classCallCheck(instance, Constructor) {
	if (!(instance instanceof Constructor)) {
		throw new TypeError("Cannot call a class as a function");
	}
}

function _possibleConstructorReturn(self, call) {
	if (!self) {
		throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
	}return call && (typeof call === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
	if (typeof superClass !== "function" && superClass !== null) {
		throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
	}subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var _class = function (_Command) {
	_inherits(_class, _Command);

	function _class(gameplayState) {
		_classCallCheck(this, _class);

		var _this = _possibleConstructorReturn(this, (_class.__proto__ || Object.getPrototypeOf(_class)).call(this));

		_this.gameplayState = gameplayState;
		return _this;
	}

	_createClass(_class, [{
		key: 'execute',
		value: function execute(game) {
			var grid = game.model.gridData;
			grid.shiftUpAll();

			for (var x = 0; x < _GameSettings2.default.GRID_WIDTH; x++) {
				var discData = this.createDisc(grid, x, _GameSettings2.default.TRUE_GRID_HEIGHT - 1);
				game.commander.execute("GameplayCommand.CreateDiscSprite", game, discData, true);
			}

			game.commander.execute("GameplayCommand.AnimateDiscs");

			return true;
		}
	}, {
		key: 'createDisc',
		value: function createDisc(grid, x, y) {
			var min = _GameSettings2.default.MIN_DISC_VALUE;
			var max = _GameSettings2.default.MAX_DISC_VALUE;

			var discValue = Math.floor(Math.random() * (max - min + 1)) + min;

			var discData = new _GameplayGridDiscData2.default(discValue, _GameSettings2.default.DEFAULT_ARMOUR_VALUE);
			grid.addDiscAtPosition(discData, x, y);

			return discData;
		}
	}], [{
		key: 'NAME',
		get: function get() {
			return "GameplayCommand.AddBottomRowDiscs";
		}
	}]);

	return _class;
}(_Command3.default);

exports.default = _class;

},{"../../model/gameplay/GameplayGridDiscData":22,"../../settings/GameSettings":23,"../Command":2}],7:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () {
	function defineProperties(target, props) {
		for (var i = 0; i < props.length; i++) {
			var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
		}
	}return function (Constructor, protoProps, staticProps) {
		if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
	};
}();

var _Command2 = require("../Command");

var _Command3 = _interopRequireDefault(_Command2);

function _interopRequireDefault(obj) {
	return obj && obj.__esModule ? obj : { default: obj };
}

function _classCallCheck(instance, Constructor) {
	if (!(instance instanceof Constructor)) {
		throw new TypeError("Cannot call a class as a function");
	}
}

function _possibleConstructorReturn(self, call) {
	if (!self) {
		throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
	}return call && (typeof call === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
	if (typeof superClass !== "function" && superClass !== null) {
		throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
	}subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var _class = function (_Command) {
	_inherits(_class, _Command);

	function _class(gameplayState) {
		_classCallCheck(this, _class);

		var _this = _possibleConstructorReturn(this, (_class.__proto__ || Object.getPrototypeOf(_class)).call(this));

		_this.gameplaySprite = gameplayState;
		return _this;
	}

	_createClass(_class, [{
		key: "execute",
		value: function execute(game) {
			this.gameplaySprite.handleAnimations();

			return true;
		}
	}], [{
		key: "NAME",
		get: function get() {
			return "GameplayCommand.AnimateDiscs";
		}
	}]);

	return _class;
}(_Command3.default);

exports.default = _class;

},{"../Command":2}],8:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () {
	function defineProperties(target, props) {
		for (var i = 0; i < props.length; i++) {
			var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
		}
	}return function (Constructor, protoProps, staticProps) {
		if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
	};
}();

var _Command2 = require('../Command');

var _Command3 = _interopRequireDefault(_Command2);

var _GameSettings = require('../../settings/GameSettings');

var _GameSettings2 = _interopRequireDefault(_GameSettings);

function _interopRequireDefault(obj) {
	return obj && obj.__esModule ? obj : { default: obj };
}

function _classCallCheck(instance, Constructor) {
	if (!(instance instanceof Constructor)) {
		throw new TypeError("Cannot call a class as a function");
	}
}

function _possibleConstructorReturn(self, call) {
	if (!self) {
		throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
	}return call && (typeof call === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
	if (typeof superClass !== "function" && superClass !== null) {
		throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
	}subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var _class = function (_Command) {
	_inherits(_class, _Command);

	function _class(gameplayState) {
		_classCallCheck(this, _class);

		var _this = _possibleConstructorReturn(this, (_class.__proto__ || Object.getPrototypeOf(_class)).call(this));

		_this.gameplayState = gameplayState;
		return _this;
	}

	_createClass(_class, [{
		key: 'execute',
		value: function execute(game) {
			var grid = game.model.gridData;
			//Game over if grid full or discs in top row
			var isGameOver = true;

			for (var i = 0; i < _GameSettings2.default.GRID_WIDTH * _GameSettings2.default.TRUE_GRID_HEIGHT; i++) {
				var discData = grid.data[i];
				var x = i % _GameSettings2.default.GRID_WIDTH;
				var y = Math.floor(i / _GameSettings2.default.GRID_WIDTH);

				if (y == 0) {
					if (discData != null) {
						isGameOver = true;
						break;
					}
				} else {
					if (discData == null) {
						isGameOver = false;
						break;
					}
				}
			}

			if (isGameOver) {
				this.gameplayState.beginGameOverSequence();
				game.commander.execute("AppCommand.PlaySound", "game_over");

				return true;
			} else {
				game.model.gameplay.increaseCombo();
				game.commander.execute("GameplayCommand.AnimateDiscs");

				return false;
			}
		}
	}], [{
		key: 'NAME',
		get: function get() {
			return "GameplayCommand.CheckForGameOver";
		}
	}]);

	return _class;
}(_Command3.default);

exports.default = _class;

},{"../../settings/GameSettings":23,"../Command":2}],9:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () {
	function defineProperties(target, props) {
		for (var i = 0; i < props.length; i++) {
			var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
		}
	}return function (Constructor, protoProps, staticProps) {
		if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
	};
}();

var _Command2 = require("../Command");

var _Command3 = _interopRequireDefault(_Command2);

function _interopRequireDefault(obj) {
	return obj && obj.__esModule ? obj : { default: obj };
}

function _classCallCheck(instance, Constructor) {
	if (!(instance instanceof Constructor)) {
		throw new TypeError("Cannot call a class as a function");
	}
}

function _possibleConstructorReturn(self, call) {
	if (!self) {
		throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
	}return call && (typeof call === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
	if (typeof superClass !== "function" && superClass !== null) {
		throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
	}subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var _class = function (_Command) {
	_inherits(_class, _Command);

	function _class(gameplayState) {
		_classCallCheck(this, _class);

		var _this = _possibleConstructorReturn(this, (_class.__proto__ || Object.getPrototypeOf(_class)).call(this));

		_this.gameplayState = gameplayState;
		return _this;
	}

	_createClass(_class, [{
		key: "execute",
		value: function execute(game, gridDiscData) {
			var forceWorldPosition = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

			var sprite = this.gameplayState.createNewGridDiscSprite(gridDiscData);

			if (forceWorldPosition) {
				sprite.gridPositionToWorldPosition();
			}

			return true;
		}
	}], [{
		key: "NAME",
		get: function get() {
			return "GameplayCommand.CreateDiscSprite";
		}
	}]);

	return _class;
}(_Command3.default);

exports.default = _class;

},{"../Command":2}],10:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () {
	function defineProperties(target, props) {
		for (var i = 0; i < props.length; i++) {
			var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
		}
	}return function (Constructor, protoProps, staticProps) {
		if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
	};
}();

var _Command2 = require('../Command');

var _Command3 = _interopRequireDefault(_Command2);

var _GameplayGridDiscData = require('../../model/gameplay/GameplayGridDiscData');

var _GameplayGridDiscData2 = _interopRequireDefault(_GameplayGridDiscData);

var _GameSettings = require('../../settings/GameSettings');

var _GameSettings2 = _interopRequireDefault(_GameSettings);

function _interopRequireDefault(obj) {
	return obj && obj.__esModule ? obj : { default: obj };
}

function _classCallCheck(instance, Constructor) {
	if (!(instance instanceof Constructor)) {
		throw new TypeError("Cannot call a class as a function");
	}
}

function _possibleConstructorReturn(self, call) {
	if (!self) {
		throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
	}return call && (typeof call === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
	if (typeof superClass !== "function" && superClass !== null) {
		throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
	}subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var _class = function (_Command) {
	_inherits(_class, _Command);

	function _class(gameplayState) {
		_classCallCheck(this, _class);

		var _this = _possibleConstructorReturn(this, (_class.__proto__ || Object.getPrototypeOf(_class)).call(this));

		_this.gameplayState = gameplayState;
		return _this;
	}

	_createClass(_class, [{
		key: 'execute',
		value: function execute(game) {
			var min = _GameSettings2.default.MIN_DISC_VALUE;
			var max = _GameSettings2.default.MAX_DISC_VALUE;

			var discValue = Math.floor(Math.random() * (max - min + 1)) + min;
			var armourValue = Math.random() < _GameSettings2.default.CHANCE_OF_ARMOUR_DISC ? _GameSettings2.default.DEFAULT_ARMOUR_VALUE : 0;

			var discData = new _GameplayGridDiscData2.default(discValue, armourValue);
			this.gameplayState.createNextDiscSprite(discData);

			return true;
		}
	}], [{
		key: 'NAME',
		get: function get() {
			return "GameplayCommand.CreateNextGridDisc";
		}
	}]);

	return _class;
}(_Command3.default);

exports.default = _class;

},{"../../model/gameplay/GameplayGridDiscData":22,"../../settings/GameSettings":23,"../Command":2}],11:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () {
	function defineProperties(target, props) {
		for (var i = 0; i < props.length; i++) {
			var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
		}
	}return function (Constructor, protoProps, staticProps) {
		if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
	};
}();

var _Command2 = require("../Command");

var _Command3 = _interopRequireDefault(_Command2);

function _interopRequireDefault(obj) {
	return obj && obj.__esModule ? obj : { default: obj };
}

function _classCallCheck(instance, Constructor) {
	if (!(instance instanceof Constructor)) {
		throw new TypeError("Cannot call a class as a function");
	}
}

function _possibleConstructorReturn(self, call) {
	if (!self) {
		throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
	}return call && (typeof call === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
	if (typeof superClass !== "function" && superClass !== null) {
		throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
	}subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var _class = function (_Command) {
	_inherits(_class, _Command);

	function _class(gameplayState) {
		_classCallCheck(this, _class);

		var _this = _possibleConstructorReturn(this, (_class.__proto__ || Object.getPrototypeOf(_class)).call(this));

		_this.gameplayState = gameplayState;
		return _this;
	}

	_createClass(_class, [{
		key: "execute",
		value: function execute(points, x, y) {
			this.gameplayState.createPointsSprite(points, x, y);

			return true;
		}
	}], [{
		key: "NAME",
		get: function get() {
			return "GameplayCommand.CreatePointsSprite";
		}
	}]);

	return _class;
}(_Command3.default);

exports.default = _class;

},{"../Command":2}],12:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () {
	function defineProperties(target, props) {
		for (var i = 0; i < props.length; i++) {
			var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
		}
	}return function (Constructor, protoProps, staticProps) {
		if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
	};
}();

var _Command2 = require('../Command');

var _Command3 = _interopRequireDefault(_Command2);

var _GameplayGridDiscData = require('../../model/gameplay/GameplayGridDiscData');

var _GameplayGridDiscData2 = _interopRequireDefault(_GameplayGridDiscData);

function _interopRequireDefault(obj) {
	return obj && obj.__esModule ? obj : { default: obj };
}

function _classCallCheck(instance, Constructor) {
	if (!(instance instanceof Constructor)) {
		throw new TypeError("Cannot call a class as a function");
	}
}

function _possibleConstructorReturn(self, call) {
	if (!self) {
		throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
	}return call && (typeof call === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
	if (typeof superClass !== "function" && superClass !== null) {
		throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
	}subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var _class = function (_Command) {
	_inherits(_class, _Command);

	function _class() {
		_classCallCheck(this, _class);

		return _possibleConstructorReturn(this, (_class.__proto__ || Object.getPrototypeOf(_class)).call(this));
	}

	_createClass(_class, [{
		key: 'execute',
		value: function execute(game, discData) {
			var createSprite = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

			console.log("Dropping a " + discData.discValue + " in column " + discData.x);

			if (game.model.gridData.dropInColumn(discData, discData.x)) {
				if (createSprite) {
					game.commander.execute("GameplayCommand.CreateDiscSprite", game, discData);
				}

				return true;
			} else {
				return false;
			}
		}
	}], [{
		key: 'NAME',
		get: function get() {
			return "GameplayCommand.DropGridDisc";
		}
	}]);

	return _class;
}(_Command3.default);

exports.default = _class;

},{"../../model/gameplay/GameplayGridDiscData":22,"../Command":2}],13:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () {
	function defineProperties(target, props) {
		for (var i = 0; i < props.length; i++) {
			var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
		}
	}return function (Constructor, protoProps, staticProps) {
		if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
	};
}();

var _Command2 = require("../Command");

var _Command3 = _interopRequireDefault(_Command2);

function _interopRequireDefault(obj) {
	return obj && obj.__esModule ? obj : { default: obj };
}

function _classCallCheck(instance, Constructor) {
	if (!(instance instanceof Constructor)) {
		throw new TypeError("Cannot call a class as a function");
	}
}

function _possibleConstructorReturn(self, call) {
	if (!self) {
		throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
	}return call && (typeof call === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
	if (typeof superClass !== "function" && superClass !== null) {
		throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
	}subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var _class = function (_Command) {
	_inherits(_class, _Command);

	function _class() {
		_classCallCheck(this, _class);

		return _possibleConstructorReturn(this, (_class.__proto__ || Object.getPrototypeOf(_class)).call(this));
	}

	_createClass(_class, [{
		key: "execute",
		value: function execute(game, discData, column, gamestate) {
			discData.x = column;

			var dropSuccessful = game.commander.execute("GameplayCommand.DropGridDisc", game, discData, false);

			if (dropSuccessful) {
				game.commander.execute("AppCommand.PlaySound", "drop_disc");
				gamestate.handleAnimations();
				return true;
			} else {
				game.commander.execute("AppCommand.PlaySound", "cancel");

				gamestate.cancelDrop();
				return false;
			}
		}
	}], [{
		key: "NAME",
		get: function get() {
			return "GameplayCommand.DropNextGridDisc";
		}
	}]);

	return _class;
}(_Command3.default);

exports.default = _class;

},{"../Command":2}],14:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () {
	function defineProperties(target, props) {
		for (var i = 0; i < props.length; i++) {
			var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
		}
	}return function (Constructor, protoProps, staticProps) {
		if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
	};
}();

var _Command2 = require('../Command');

var _Command3 = _interopRequireDefault(_Command2);

var _GameSettings = require('../../settings/GameSettings');

var _GameSettings2 = _interopRequireDefault(_GameSettings);

function _interopRequireDefault(obj) {
	return obj && obj.__esModule ? obj : { default: obj };
}

function _classCallCheck(instance, Constructor) {
	if (!(instance instanceof Constructor)) {
		throw new TypeError("Cannot call a class as a function");
	}
}

function _possibleConstructorReturn(self, call) {
	if (!self) {
		throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
	}return call && (typeof call === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
	if (typeof superClass !== "function" && superClass !== null) {
		throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
	}subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var _class = function (_Command) {
	_inherits(_class, _Command);

	function _class() {
		_classCallCheck(this, _class);

		return _possibleConstructorReturn(this, (_class.__proto__ || Object.getPrototypeOf(_class)).call(this));
	}

	_createClass(_class, [{
		key: 'execute',
		value: function execute(game, callback) {
			// get row matches
			var grid = game.model.gridData;
			var rowMatches = grid.checkRowsForMatches();
			var columnMatches = grid.checkColumnsForMatches();
			var allMatches = rowMatches.concat(columnMatches);

			var columnsToUpdate = [];
			for (var i = 0; i < allMatches.length; i++) {

				game.model.gameplay.increaseScore();

				var matchedDisc = allMatches[i];
				if (!columnsToUpdate.includes(matchedDisc.x)) {
					columnsToUpdate.push(matchedDisc.x);
				}

				grid.removeDiscAtPosition(matchedDisc.x, matchedDisc.y);
				this.breakAdjacentArmour(grid, matchedDisc.x, matchedDisc.y);
				matchedDisc.destroyed = true;
				game.commander.execute("GameplayCommand.CreatePointsSprite", game.model.gameplay.getComboValue(), matchedDisc.x, matchedDisc.y);
			}

			for (var j = 0; j < columnsToUpdate.length; j++) {
				var x = columnsToUpdate[j];
				grid.updateColumn(x);
			}

			game.commander.execute("GameplayCommand.CheckForGameOver", game);

			if (allMatches.length == 0) {
				//Grid stabilised - add next disc
				if (game.model.gameplay.isRoundComplete()) {
					game.commander.execute("GameplayCommand.HandleTurnEnd", game);
				} else {
					game.model.gameplay.nextTurn();
					game.commander.execute("GameplayCommand.CreateNextGridDisc", game);
					game.model.gameplay.resetCombo();
				}
			} else {
				game.commander.execute("AppCommand.PlaySound", "break_disc");
			}

			return allMatches.length > 0;
		}
	}, {
		key: 'breakAdjacentArmour',
		value: function breakAdjacentArmour(grid, x, y) {
			// get left
			if (x > 0) {
				var leftData = grid.getDiscAtPosition(x - 1, y);

				if (leftData != null) {
					leftData.armour--;
				}
			}
			// get right
			if (x < _GameSettings2.default.GRID_WIDTH - 1) {
				var rightData = grid.getDiscAtPosition(x + 1, y);

				if (rightData != null) {
					rightData.armour--;
				}
			}
			//get top
			if (y > 0) {
				var topData = grid.getDiscAtPosition(x, y - 1);

				if (topData != null) {
					topData.armour--;
				}
			}
			//get bottom
			if (y < _GameSettings2.default.TRUE_GRID_HEIGHT - 1) {
				var bottomData = grid.getDiscAtPosition(x, y + 1);

				if (bottomData != null) {
					bottomData.armour--;
				}
			}
		}
	}], [{
		key: 'NAME',
		get: function get() {
			return "GameplayCommand.EvaluateGridMatches";
		}
	}]);

	return _class;
}(_Command3.default);

exports.default = _class;

},{"../../settings/GameSettings":23,"../Command":2}],15:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () {
	function defineProperties(target, props) {
		for (var i = 0; i < props.length; i++) {
			var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
		}
	}return function (Constructor, protoProps, staticProps) {
		if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
	};
}();

var _Command2 = require('../Command');

var _Command3 = _interopRequireDefault(_Command2);

var _GameplayGridData = require('../../model/gameplay/GameplayGridData');

var _GameplayGridData2 = _interopRequireDefault(_GameplayGridData);

var _GameplayGridDiscData = require('../../model/gameplay/GameplayGridDiscData');

var _GameplayGridDiscData2 = _interopRequireDefault(_GameplayGridDiscData);

var _GameSettings = require('../../settings/GameSettings');

var _GameSettings2 = _interopRequireDefault(_GameSettings);

function _interopRequireDefault(obj) {
	return obj && obj.__esModule ? obj : { default: obj };
}

function _classCallCheck(instance, Constructor) {
	if (!(instance instanceof Constructor)) {
		throw new TypeError("Cannot call a class as a function");
	}
}

function _possibleConstructorReturn(self, call) {
	if (!self) {
		throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
	}return call && (typeof call === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
	if (typeof superClass !== "function" && superClass !== null) {
		throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
	}subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var _class = function (_Command) {
	_inherits(_class, _Command);

	function _class() {
		_classCallCheck(this, _class);

		return _possibleConstructorReturn(this, (_class.__proto__ || Object.getPrototypeOf(_class)).call(this));
	}

	_createClass(_class, [{
		key: 'execute',
		value: function execute(game) {
			var newGrid = new _GameplayGridData2.default();
			game.model.gridData = newGrid;

			for (var i = 0; i < _GameSettings2.default.STARTING_DISCS; i++) {
				var randomDisc = this.createRandomDisc(newGrid);
				game.commander.execute("GameplayCommand.CreateDiscSprite", game, randomDisc, true);
			}

			return true;
		}
	}, {
		key: 'createRandomDisc',
		value: function createRandomDisc(grid) {
			var min = _GameSettings2.default.MIN_DISC_VALUE;
			var max = _GameSettings2.default.MAX_DISC_VALUE;

			var discValue = Math.floor(Math.random() * (max - min + 1)) + min;

			// Prevent grid from overflowing on first turn
			var lowestRow = _GameSettings2.default.TRUE_GRID_HEIGHT - _GameSettings2.default.STARTING_COLUMN_HEIGHT_LIMIT;

			var columnToDropIn = this.getRandomColumn();
			var lowestSlotInColumn = grid.firstFreeRow(columnToDropIn);

			while (lowestSlotInColumn < lowestRow) {
				columnToDropIn = this.getRandomColumn();
				lowestSlotInColumn = grid.firstFreeRow(columnToDropIn);
			}

			var armourValue = Math.random() < _GameSettings2.default.CHANCE_OF_ARMOUR_DISC ? _GameSettings2.default.DEFAULT_ARMOUR_VALUE : 0;

			var discData = new _GameplayGridDiscData2.default(discValue, armourValue);
			grid.dropInColumn(discData, columnToDropIn);

			return discData;
		}
	}, {
		key: 'getRandomColumn',
		value: function getRandomColumn() {
			return Math.floor(Math.random() * _GameSettings2.default.GRID_WIDTH);
		}
	}], [{
		key: 'NAME',
		get: function get() {
			return "GameplayCommand.GenerateGridData";
		}
	}]);

	return _class;
}(_Command3.default);

exports.default = _class;

},{"../../model/gameplay/GameplayGridData":21,"../../model/gameplay/GameplayGridDiscData":22,"../../settings/GameSettings":23,"../Command":2}],16:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () {
	function defineProperties(target, props) {
		for (var i = 0; i < props.length; i++) {
			var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
		}
	}return function (Constructor, protoProps, staticProps) {
		if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
	};
}();

var _Command2 = require("../Command");

var _Command3 = _interopRequireDefault(_Command2);

function _interopRequireDefault(obj) {
	return obj && obj.__esModule ? obj : { default: obj };
}

function _classCallCheck(instance, Constructor) {
	if (!(instance instanceof Constructor)) {
		throw new TypeError("Cannot call a class as a function");
	}
}

function _possibleConstructorReturn(self, call) {
	if (!self) {
		throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
	}return call && (typeof call === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
	if (typeof superClass !== "function" && superClass !== null) {
		throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
	}subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var _class = function (_Command) {
	_inherits(_class, _Command);

	function _class() {
		_classCallCheck(this, _class);

		return _possibleConstructorReturn(this, (_class.__proto__ || Object.getPrototypeOf(_class)).call(this));
	}

	_createClass(_class, [{
		key: "execute",
		value: function execute(game) {
			var score = game.model.gameplay.score;
			game.model.player.compareScore(score);
			game.state.start("MainMenu");

			return true;
		}
	}], [{
		key: "NAME",
		get: function get() {
			return "GameplayCommand.HandleGameOver";
		}
	}]);

	return _class;
}(_Command3.default);

exports.default = _class;

},{"../Command":2}],17:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () {
	function defineProperties(target, props) {
		for (var i = 0; i < props.length; i++) {
			var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
		}
	}return function (Constructor, protoProps, staticProps) {
		if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
	};
}();

var _Command2 = require("../Command");

var _Command3 = _interopRequireDefault(_Command2);

function _interopRequireDefault(obj) {
	return obj && obj.__esModule ? obj : { default: obj };
}

function _classCallCheck(instance, Constructor) {
	if (!(instance instanceof Constructor)) {
		throw new TypeError("Cannot call a class as a function");
	}
}

function _possibleConstructorReturn(self, call) {
	if (!self) {
		throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
	}return call && (typeof call === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
	if (typeof superClass !== "function" && superClass !== null) {
		throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
	}subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var _class = function (_Command) {
	_inherits(_class, _Command);

	function _class() {
		_classCallCheck(this, _class);

		return _possibleConstructorReturn(this, (_class.__proto__ || Object.getPrototypeOf(_class)).call(this));
	}

	_createClass(_class, [{
		key: "execute",
		value: function execute(game) {

			game.model.gameplay.resetRound();
			game.commander.execute("AppCommand.PlaySound", "new_row");
			game.commander.execute("GameplayCommand.AddBottomRowDiscs", game);

			return true;
		}
	}], [{
		key: "NAME",
		get: function get() {
			return "GameplayCommand.HandleTurnEnd";
		}
	}]);

	return _class;
}(_Command3.default);

exports.default = _class;

},{"../Command":2}],18:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () {
	function defineProperties(target, props) {
		for (var i = 0; i < props.length; i++) {
			var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
		}
	}return function (Constructor, protoProps, staticProps) {
		if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
	};
}();

var _Command2 = require('../Command');

var _Command3 = _interopRequireDefault(_Command2);

var _CreateDiscSpriteCommand = require('./CreateDiscSpriteCommand');

var _CreateDiscSpriteCommand2 = _interopRequireDefault(_CreateDiscSpriteCommand);

var _CreatePointsSpriteCommand = require('./CreatePointsSpriteCommand');

var _CreatePointsSpriteCommand2 = _interopRequireDefault(_CreatePointsSpriteCommand);

var _AnimateDiscsCommand = require('./AnimateDiscsCommand');

var _AnimateDiscsCommand2 = _interopRequireDefault(_AnimateDiscsCommand);

var _CreateNextGridDiscCommand = require('./CreateNextGridDiscCommand');

var _CreateNextGridDiscCommand2 = _interopRequireDefault(_CreateNextGridDiscCommand);

var _AddBottomRowDiscsCommand = require('./AddBottomRowDiscsCommand');

var _AddBottomRowDiscsCommand2 = _interopRequireDefault(_AddBottomRowDiscsCommand);

var _GameplayData = require('../../model/gameplay/GameplayData');

var _GameplayData2 = _interopRequireDefault(_GameplayData);

var _CheckForGameOverCommand = require('./CheckForGameOverCommand');

var _CheckForGameOverCommand2 = _interopRequireDefault(_CheckForGameOverCommand);

function _interopRequireDefault(obj) {
	return obj && obj.__esModule ? obj : { default: obj };
}

function _classCallCheck(instance, Constructor) {
	if (!(instance instanceof Constructor)) {
		throw new TypeError("Cannot call a class as a function");
	}
}

function _possibleConstructorReturn(self, call) {
	if (!self) {
		throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
	}return call && (typeof call === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
	if (typeof superClass !== "function" && superClass !== null) {
		throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
	}subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var _class = function (_Command) {
	_inherits(_class, _Command);

	function _class() {
		_classCallCheck(this, _class);

		return _possibleConstructorReturn(this, (_class.__proto__ || Object.getPrototypeOf(_class)).call(this));
	}

	_createClass(_class, [{
		key: 'execute',
		value: function execute(game, gameplayState) {
			game.commander.registerCommand(_CreateDiscSpriteCommand2.default.NAME, new _CreateDiscSpriteCommand2.default(gameplayState), true);
			game.commander.registerCommand(_AnimateDiscsCommand2.default.NAME, new _AnimateDiscsCommand2.default(gameplayState), true);
			game.commander.registerCommand(_CreateNextGridDiscCommand2.default.NAME, new _CreateNextGridDiscCommand2.default(gameplayState), true);
			game.commander.registerCommand(_AddBottomRowDiscsCommand2.default.NAME, new _AddBottomRowDiscsCommand2.default(gameplayState), true);
			game.commander.registerCommand(_CheckForGameOverCommand2.default.NAME, new _CheckForGameOverCommand2.default(gameplayState), true);
			game.commander.registerCommand(_CreatePointsSpriteCommand2.default.NAME, new _CreatePointsSpriteCommand2.default(gameplayState), true);

			game.model.gameplay = new _GameplayData2.default();
			game.commander.execute("GameplayCommand.GenerateGridData", game);
			game.commander.execute("GameplayCommand.EvaluateGridMatches", game);

			return true;
		}
	}], [{
		key: 'NAME',
		get: function get() {
			return "GameplayCommand.InitialiseGameplay";
		}
	}]);

	return _class;
}(_Command3.default);

exports.default = _class;

},{"../../model/gameplay/GameplayData":20,"../Command":2,"./AddBottomRowDiscsCommand":6,"./AnimateDiscsCommand":7,"./CheckForGameOverCommand":8,"./CreateDiscSpriteCommand":9,"./CreateNextGridDiscCommand":10,"./CreatePointsSpriteCommand":11}],19:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () {
	function defineProperties(target, props) {
		for (var i = 0; i < props.length; i++) {
			var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
		}
	}return function (Constructor, protoProps, staticProps) {
		if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
	};
}();

function _classCallCheck(instance, Constructor) {
	if (!(instance instanceof Constructor)) {
		throw new TypeError("Cannot call a class as a function");
	}
}

var _class = function () {
	function _class() {
		_classCallCheck(this, _class);

		this.highscore = 0;
	}

	_createClass(_class, [{
		key: "compareScore",
		value: function compareScore(newScore) {
			if (newScore > this.highscore) {
				this.highscore = newScore;
			}
		}
	}]);

	return _class;
}();

exports.default = _class;

},{}],20:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () {
	function defineProperties(target, props) {
		for (var i = 0; i < props.length; i++) {
			var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
		}
	}return function (Constructor, protoProps, staticProps) {
		if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
	};
}();

var _GameSettings = require('../../settings/GameSettings');

var _GameSettings2 = _interopRequireDefault(_GameSettings);

function _interopRequireDefault(obj) {
	return obj && obj.__esModule ? obj : { default: obj };
}

function _classCallCheck(instance, Constructor) {
	if (!(instance instanceof Constructor)) {
		throw new TypeError("Cannot call a class as a function");
	}
}

var _class = function () {
	function _class() {
		_classCallCheck(this, _class);

		this.score = 0;
		this.combo = 0;
		this.turnsUntilNextRound = _GameSettings2.default.TURNS_BETWEEN_ROUNDS;
	}

	_createClass(_class, [{
		key: 'increaseScore',
		value: function increaseScore() {
			this.score += _GameSettings2.default.BASE_DISC_POINTS + _GameSettings2.default.POINTS_MULTIPLIER * this.combo;
		}
	}, {
		key: 'increaseCombo',
		value: function increaseCombo() {
			this.combo++;
		}
	}, {
		key: 'resetCombo',
		value: function resetCombo() {
			this.combo = 0;
		}
	}, {
		key: 'getComboValue',
		value: function getComboValue() {
			return _GameSettings2.default.BASE_DISC_POINTS + _GameSettings2.default.POINTS_MULTIPLIER * this.combo;
		}
	}, {
		key: 'nextTurn',
		value: function nextTurn() {
			this.turnsUntilNextRound--;
		}
	}, {
		key: 'isRoundComplete',
		value: function isRoundComplete() {
			return this.turnsUntilNextRound == 0;
		}
	}, {
		key: 'resetRound',
		value: function resetRound() {
			this.turnsUntilNextRound = _GameSettings2.default.TURNS_BETWEEN_ROUNDS;
		}
	}]);

	return _class;
}();

exports.default = _class;

},{"../../settings/GameSettings":23}],21:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () {
	function defineProperties(target, props) {
		for (var i = 0; i < props.length; i++) {
			var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
		}
	}return function (Constructor, protoProps, staticProps) {
		if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
	};
}();

var _GameSettings = require("../../settings/GameSettings");

var _GameSettings2 = _interopRequireDefault(_GameSettings);

function _interopRequireDefault(obj) {
	return obj && obj.__esModule ? obj : { default: obj };
}

function _classCallCheck(instance, Constructor) {
	if (!(instance instanceof Constructor)) {
		throw new TypeError("Cannot call a class as a function");
	}
}

var _class = function () {
	function _class() {
		_classCallCheck(this, _class);

		this.data = [];
	}

	_createClass(_class, [{
		key: "updateColumn",
		value: function updateColumn(x) {
			// Evaluate bottom to top
			// IGNORE BOTTOM ROW, can't move down any further
			for (var y = _GameSettings2.default.TRUE_GRID_HEIGHT - 2; y > 0; y--) {
				var currentDisc = this.getDiscAtPosition(x, y);
				var discBelow = this.getDiscAtPosition(x, y + 1);

				if (currentDisc != null && discBelow == null) {
					// Drop disc down
					var newY = y;
					while (newY < _GameSettings2.default.GRID_HEIGHT && discBelow == null) {
						// Find lowest free slot
						newY += 1;
						var discBelow = this.getDiscAtPosition(x, newY + 1);
					}

					this.moveDiscAtPosition(x, y, x, newY);
				}
			}
		}

		// Find matching discs vertically

	}, {
		key: "checkColumnsForMatches",
		value: function checkColumnsForMatches() {
			var matchingColumnDiscs = [];
			var groups = [];

			for (var x = 0; x < _GameSettings2.default.GRID_WIDTH; x++) {
				var currentGroup = [];
				var y = 1;

				while (y < _GameSettings2.default.TRUE_GRID_HEIGHT) {
					var currentGridDisc = this.getDiscAtPosition(x, y);
					if (currentGridDisc != null) {
						currentGroup.push(currentGridDisc);
					}

					y++;
				}

				if (currentGroup.length > 0) {
					groups.push(currentGroup);
				}
			}

			return this.checkMatchesInGroups(groups);
		}

		// Find matching discs horizontally

	}, {
		key: "checkRowsForMatches",
		value: function checkRowsForMatches() {
			var groups = [];
			//Create groups of connected discs IGNORE TOP ROW
			for (var y = 1; y < _GameSettings2.default.TRUE_GRID_HEIGHT; y++) {

				var currentGroup = [];
				var x = 0;

				while (x < _GameSettings2.default.GRID_WIDTH) {
					var currentGridDisc = this.getDiscAtPosition(x, y);

					if (currentGridDisc == null) {
						// There is a break in connected discs
						if (currentGroup.length > 0) {
							// Previous group had discs - add to list
							groups.push(currentGroup);
							currentGroup = []; // Create new group
						}
					} else {
						currentGroup.push(currentGridDisc);
					}
					x++;
				}

				if (currentGroup.length > 0) {
					groups.push(currentGroup);
				}
			}

			return this.checkMatchesInGroups(groups);
		}
	}, {
		key: "checkMatchesInGroups",
		value: function checkMatchesInGroups(groups) {
			var matchingRowDiscs = [];
			for (var i = 0; i < groups.length; i++) {
				var connectedGroup = groups[i];
				var matchValue = connectedGroup.length;

				for (var j = 0; j < connectedGroup.length; j++) {
					var disc = connectedGroup[j];
					if (matchValue == disc.discValue && disc.armour <= 0) {
						matchingRowDiscs.push(disc);
					}
				}
			}

			return matchingRowDiscs;
		}
	}, {
		key: "dropInColumn",
		value: function dropInColumn(gridDiscData, x) {
			var freeRow = this.firstFreeRow(x);
			if (freeRow == 0) {
				//Row is full
				return false;
			} else {
				this.addDiscAtPosition(gridDiscData, x, freeRow);
				return true;
			}
		}
	}, {
		key: "firstFreeRow",
		value: function firstFreeRow(x) {
			var firstFreeRow = 0;
			for (var y = _GameSettings2.default.TRUE_GRID_HEIGHT - 1; y >= 0; y--) {
				if (this.getDiscAtPosition(x, y) == null) {

					firstFreeRow = y;
					break;
				}
			}
			return firstFreeRow;
		}
	}, {
		key: "addDiscAtPosition",
		value: function addDiscAtPosition(gridDiscData, x, y) {
			gridDiscData.x = x;
			gridDiscData.y = y;
			this.data[x + y * _GameSettings2.default.GRID_WIDTH] = gridDiscData;
		}
	}, {
		key: "moveDiscAtPosition",
		value: function moveDiscAtPosition(x, y, newX, newY) {
			var gridDiscData = this.getDiscAtPosition(x, y);
			this.removeDiscAtPosition(x, y);
			this.addDiscAtPosition(gridDiscData, newX, newY);
		}
	}, {
		key: "removeDiscAtPosition",
		value: function removeDiscAtPosition(x, y) {
			this.data[x + y * _GameSettings2.default.GRID_WIDTH] = null;
		}
	}, {
		key: "getDiscAtPosition",
		value: function getDiscAtPosition(x, y) {
			if (!this.inBounds(x, y)) {
				return null;
			}
			return this.data[x + y * _GameSettings2.default.GRID_WIDTH];
		}

		// Check coordinates are within the grid

	}, {
		key: "inBounds",
		value: function inBounds(x, y) {
			if (x < 0 || x >= _GameSettings2.default.GRID_WIDTH || y < 1 || y >= _GameSettings2.default.TRUE_GRID_HEIGHT) {
				return false;
			} else {
				return true;
			}
		}

		// Move all grid items up one (end of round)

	}, {
		key: "shiftUpAll",
		value: function shiftUpAll() {
			for (var i = 0; i < this.data.length; i++) {
				if (this.data[i] != null) {
					this.moveDiscAtPosition(this.data[i].x, this.data[i].y, this.data[i].x, this.data[i].y - 1);
				}
			}
		}

		//Print grid array to console (debugging)

	}, {
		key: "printGridToConsole",
		value: function printGridToConsole() {
			for (var y = 0; y < _GameSettings2.default.TRUE_GRID_HEIGHT; y++) {
				var rowString = "";

				for (var x = 0; x < _GameSettings2.default.GRID_WIDTH; x++) {
					if (y == 0) {
						rowString += "- ";
					} else {
						var discData = this.getDiscAtPosition(x, y);
						if (discData != null) {
							rowString += discData.discValue + " ";
						} else {
							rowString += "x ";
						}
					}
				}

				console.log(rowString);
			}
		}
	}]);

	return _class;
}();

exports.default = _class;

},{"../../settings/GameSettings":23}],22:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () {
	function defineProperties(target, props) {
		for (var i = 0; i < props.length; i++) {
			var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
		}
	}return function (Constructor, protoProps, staticProps) {
		if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
	};
}();

function _classCallCheck(instance, Constructor) {
	if (!(instance instanceof Constructor)) {
		throw new TypeError("Cannot call a class as a function");
	}
}

var _class = function () {
	function _class(value) {
		var armour = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;

		_classCallCheck(this, _class);

		this.discValue = value;
		this._armour = armour;
		this._x = 0;
		this._y = 0;
		this._destroyed = false;

		this.onChanged = new Phaser.Signal(); // Triggered when position changes
		this.onArmourChanged = new Phaser.Signal(); // Triggered when armour changes
	}

	_createClass(_class, [{
		key: "x",
		get: function get() {
			return this._x;
		},
		set: function set(value) {
			this._x = value;
		}
	}, {
		key: "y",
		get: function get() {
			return this._y;
		},
		set: function set(value) {
			this._y = value;
			this.onChanged.dispatch();
		}
	}, {
		key: "destroyed",
		get: function get() {
			return this._destroyed;
		},
		set: function set(value) {
			this._destroyed = value;

			if (this._destroye) {
				this.removeAll();
			}
		}
	}, {
		key: "armour",
		get: function get() {
			return this._armour;
		},
		set: function set(value) {
			this._armour = value;

			if (this._armour < 0) {
				this._armour = 0;
			} else {
				this.onArmourChanged.dispatch();
			}
		}
	}]);

	return _class;
}();

exports.default = _class;

},{}],23:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () {
	function defineProperties(target, props) {
		for (var i = 0; i < props.length; i++) {
			var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
		}
	}return function (Constructor, protoProps, staticProps) {
		if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
	};
}();

function _classCallCheck(instance, Constructor) {
	if (!(instance instanceof Constructor)) {
		throw new TypeError("Cannot call a class as a function");
	}
}

var _class = function () {
	function _class() {
		_classCallCheck(this, _class);
	}

	_createClass(_class, null, [{
		key: "GRID_WIDTH",
		get: function get() {
			return 7;
		}
	}, {
		key: "GRID_HEIGHT",
		get: function get() {
			return 7;
		}

		// The true grid height is one row taller than the display height. If there is 
		// a disc in the top row, the grid has overflowed and the game is over

	}, {
		key: "TRUE_GRID_HEIGHT",
		get: function get() {
			return 8;
		}
	}, {
		key: "GRID_TILE_SIZE",
		get: function get() {
			return 80;
		}
	}, {
		key: "STARTING_DISCS",
		get: function get() {
			return 12;
		}
	}, {
		key: "STARTING_COLUMN_HEIGHT_LIMIT",
		get: function get() {
			return 4;
		}
	}, {
		key: "MIN_DISC_VALUE",
		get: function get() {
			return 1;
		}
	}, {
		key: "MAX_DISC_VALUE",
		get: function get() {
			return 7;
		}
	}, {
		key: "BASE_DISC_POINTS",
		get: function get() {
			return 5;
		}
	}, {
		key: "POINTS_MULTIPLIER",
		get: function get() {
			return 23;
		}
	}, {
		key: "CHANCE_OF_ARMOUR_DISC",
		get: function get() {
			return 0.1;
		}
	}, {
		key: "DEFAULT_ARMOUR_VALUE",
		get: function get() {
			return 2;
		}
	}, {
		key: "TURNS_BETWEEN_ROUNDS",
		get: function get() {
			return 12;
		}
	}]);

	return _class;
}();

exports.default = _class;

},{}],24:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () {
	function defineProperties(target, props) {
		for (var i = 0; i < props.length; i++) {
			var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
		}
	}return function (Constructor, protoProps, staticProps) {
		if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
	};
}();

function _classCallCheck(instance, Constructor) {
	if (!(instance instanceof Constructor)) {
		throw new TypeError("Cannot call a class as a function");
	}
}

function _possibleConstructorReturn(self, call) {
	if (!self) {
		throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
	}return call && (typeof call === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
	if (typeof superClass !== "function" && superClass !== null) {
		throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
	}subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var _class = function (_Phaser$State) {
	_inherits(_class, _Phaser$State);

	function _class() {
		_classCallCheck(this, _class);

		return _possibleConstructorReturn(this, (_class.__proto__ || Object.getPrototypeOf(_class)).apply(this, arguments));
	}

	_createClass(_class, [{
		key: 'preload',
		value: function preload() {
			//TODO: restore model from save data
			this.game.model = {};

			window.WebFontConfig = {
				//  The Google Fonts we want to load (specify as many as you like in the array)
				google: {
					families: ['Permanent Marker']
				}

			};

			this.game.load.audio('break_disc', ['assets/audio/break_disc.m4a', 'assets/audio/break_disc.ogg']);
			this.game.load.audio('drop_disc', ['assets/audio/drop_disc.m4a', 'assets/audio/drop_disc.ogg']);
			this.game.load.audio('game_over', ['assets/audio/game_over.m4a', 'assets/audio/game_over.ogg']);
			this.game.load.audio('new_game', ['assets/audio/new_game.m4a', 'assets/audio/new_game.ogg']);
			this.game.load.audio('new_row', ['assets/audio/break_disc.m4a', 'assets/audio/new_row.ogg']);
			this.game.load.audio('welcome', ['assets/audio/welcome.m4a', 'assets/audio/welcome.ogg']);
			this.game.load.audio('cancel', ['assets/audio/break_disc.m4a', 'assets/audio/cancel.ogg']);

			this.game.load.script('webfont', '//ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js');
		}
	}, {
		key: 'create',
		value: function create() {

			this.game.commander.execute("AppCommand.Bootstrap", this.game);
			this.game.stage.backgroundColor = "#01cdfe";
			this.game.state.start("Splash");
		}
	}]);

	return _class;
}(Phaser.State);

exports.default = _class;

},{}],25:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () {
	function defineProperties(target, props) {
		for (var i = 0; i < props.length; i++) {
			var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
		}
	}return function (Constructor, protoProps, staticProps) {
		if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
	};
}();

function _classCallCheck(instance, Constructor) {
	if (!(instance instanceof Constructor)) {
		throw new TypeError("Cannot call a class as a function");
	}
}

function _possibleConstructorReturn(self, call) {
	if (!self) {
		throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
	}return call && (typeof call === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
	if (typeof superClass !== "function" && superClass !== null) {
		throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
	}subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var _class = function (_Phaser$State) {
	_inherits(_class, _Phaser$State);

	function _class() {
		_classCallCheck(this, _class);

		return _possibleConstructorReturn(this, (_class.__proto__ || Object.getPrototypeOf(_class)).apply(this, arguments));
	}

	_createClass(_class, [{
		key: 'preload',
		value: function preload() {
			this.game.load.image('drop7-logo-image', 'assets/images/mainMenu/drop7-logo.png');
			this.game.load.spritesheet('main-menu-button', 'assets/images/mainMenu/main-menu-button.png', 236, 80);
		}
	}, {
		key: 'create',
		value: function create() {
			this.game.stage.backgroundColor = "#ffffff";
			var logoImage = this.game.add.sprite(this.game.world.centerX, 100, 'drop7-logo-image');
			logoImage.anchor.setTo(0.5, 0);

			var style1 = { font: "40px Permanent Marker", fill: "#b05dff", align: "center" };
			this.highscoreTextfield = this.game.add.text(0, 0, "High score: " + this.game.model.player.highscore, style1);
			this.highscoreTextfield.anchor.setTo(0.5, 0);
			this.highscoreTextfield.x = this.game.world.centerX;
			this.highscoreTextfield.y = 425;

			this.button = this.game.add.button(this.game.world.centerX, 550, 'main-menu-button', this.handleClick, this, 1, 0, 0);
			this.button.anchor.setTo(0.5, 0.5);

			var style2 = { font: "50px Permanent Marker", fill: "#FFFFFF", align: "center" };
			var label = this.game.add.text(0, 0, "play", style2);
			this.button.addChild(label);
			label.anchor.set(0.5);

			this.game.commander.execute("AppCommand.PlaySound", "welcome");
		}
	}, {
		key: 'handleClick',
		value: function handleClick() {
			/*this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;
   this.game.scale.startFullScreen(false);*/
			this.game.state.start("Gameplay");
		}
	}]);

	return _class;
}(Phaser.State);

exports.default = _class;

},{}],26:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () {
	function defineProperties(target, props) {
		for (var i = 0; i < props.length; i++) {
			var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
		}
	}return function (Constructor, protoProps, staticProps) {
		if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
	};
}();

function _classCallCheck(instance, Constructor) {
	if (!(instance instanceof Constructor)) {
		throw new TypeError("Cannot call a class as a function");
	}
}

function _possibleConstructorReturn(self, call) {
	if (!self) {
		throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
	}return call && (typeof call === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
	if (typeof superClass !== "function" && superClass !== null) {
		throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
	}subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var _class = function (_Phaser$State) {
	_inherits(_class, _Phaser$State);

	function _class() {
		_classCallCheck(this, _class);

		return _possibleConstructorReturn(this, (_class.__proto__ || Object.getPrototypeOf(_class)).apply(this, arguments));
	}

	_createClass(_class, [{
		key: 'preload',
		value: function preload() {
			this.game.load.image('splash-image', 'assets/images/splash/splash-image.png');
		}
	}, {
		key: 'create',
		value: function create() {
			var splashImage = this.game.add.sprite(this.game.world.centerX, 20, 'splash-image');
			splashImage.anchor.setTo(0.5, 0);
			splashImage.alpha = 0;

			var tweenAnimateIn = this.game.add.tween(splashImage).to({ alpha: 1 }, 2000, "Linear");
			var tweenAnimateOut = this.game.add.tween(splashImage).to({ alpha: 0 }, 2000, "Linear", false, 2000);

			tweenAnimateIn.chain(tweenAnimateOut);
			tweenAnimateOut.onComplete.add(this.handleTweenComplete, this);
			tweenAnimateIn.start();
		}
	}, {
		key: 'update',
		value: function update() {
			if (this.game.input.activePointer.isDown) {
				this.goToMainMenuScreen();
			}
		}
	}, {
		key: 'handleTweenComplete',
		value: function handleTweenComplete() {
			this.goToMainMenuScreen();
		}
	}, {
		key: 'goToMainMenuScreen',
		value: function goToMainMenuScreen() {
			this.game.state.start("MainMenu");
		}
	}]);

	return _class;
}(Phaser.State);

exports.default = _class;

},{}],27:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () {
	function defineProperties(target, props) {
		for (var i = 0; i < props.length; i++) {
			var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
		}
	}return function (Constructor, protoProps, staticProps) {
		if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
	};
}();

var _GridSprite = require('./GridSprite');

var _GridSprite2 = _interopRequireDefault(_GridSprite);

var _GridDiscSprite = require('./GridDiscSprite');

var _GridDiscSprite2 = _interopRequireDefault(_GridDiscSprite);

var _PointsSprite = require('./PointsSprite');

var _PointsSprite2 = _interopRequireDefault(_PointsSprite);

var _GameSettings = require('../../settings/GameSettings');

var _GameSettings2 = _interopRequireDefault(_GameSettings);

function _interopRequireDefault(obj) {
	return obj && obj.__esModule ? obj : { default: obj };
}

function _classCallCheck(instance, Constructor) {
	if (!(instance instanceof Constructor)) {
		throw new TypeError("Cannot call a class as a function");
	}
}

function _possibleConstructorReturn(self, call) {
	if (!self) {
		throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
	}return call && (typeof call === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
	if (typeof superClass !== "function" && superClass !== null) {
		throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
	}subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var _class = function (_Phaser$State) {
	_inherits(_class, _Phaser$State);

	function _class() {
		_classCallCheck(this, _class);

		return _possibleConstructorReturn(this, (_class.__proto__ || Object.getPrototypeOf(_class)).apply(this, arguments));
	}

	_createClass(_class, [{
		key: 'preload',
		value: function preload() {
			this.game.load.atlasJSONHash('discsprites', 'assets/images/gameplay/disc_sprites.png', 'assets/images/gameplay/disc_sprites.json');
		}
	}, {
		key: 'create',
		value: function create() {
			this.discSprites = [];
			this.animationQueue = [];
			this.isAnimating = false;
			this.inGameOverSequence = false;
			this.container = this.game.add.group();
			this.container.x = 100;
			this.container.y = 100;

			this.discsGroup = this.game.add.group();

			this.previousDiscSprite = null;
			this.nextDiscSprite = null;

			this.gridSprite = new _GridSprite2.default(this.game);
			this.gridSprite.events.onInputUp.add(this.handleGridClicked, this);
			this.container.add(this.gridSprite);
			this.container.add(this.discsGroup);

			this.effects = this.game.add.group();
			this.effects.x = this.container.x;
			this.effects.y = this.container.y;

			var style1 = { font: "40px Permanent Marker", fill: "#b05dff", align: "left" };
			this.scoreTextfield = this.game.add.text(0, 0, "Score", style1);
			this.scoreTextfield.x = 20;
			this.scoreTextfield.y = 10;

			var style2 = { font: "40px Permanent Marker", fill: "#b05dff", align: "right" };
			this.countdownTextfield = this.game.add.text(0, 0, "Next round", style2);
			this.countdownTextfield.x = 730;
			this.countdownTextfield.y = 10;
			this.countdownTextfield.anchor.set(1, 0);

			this.game.commander.execute("GameplayCommand.InitialiseGameplay", this.game, this);
			this.game.commander.execute("AppCommand.PlaySound", "new_game");
		}
	}, {
		key: 'update',
		value: function update() {
			if (this.nextDiscSprite != null && !this.inGameOverSequence) {
				this.handleGridMove();
			}

			this.discsGroup.sort('y', Phaser.Group.SORT_DESCENDING);
			this.scoreTextfield.text = "Score: " + this.game.model.gameplay.score;
			this.countdownTextfield.text = "Next round: " + this.game.model.gameplay.turnsUntilNextRound + " turns";
		}
	}, {
		key: 'handleGridClicked',
		value: function handleGridClicked(owner, pointer) {
			if (!this.isAnimating && !this.inGameOverSequence) {
				//Determine row clicked
				var localX = pointer.x - this.container.x;
				var columnX = Math.floor(localX / _GameSettings2.default.GRID_TILE_SIZE);

				//Bound to grid
				if (columnX < 0) {
					columnX = 0;
				}
				if (columnX > _GameSettings2.default.GRID_WIDTH - 1) {
					columnX = _GameSettings2.default.GRID_WIDTH - 1;
				}

				this.previousDiscSprite = this.nextDiscSprite;
				this.nextDiscSprite = null;

				this.game.commander.execute("GameplayCommand.DropNextGridDisc", this.game, this.previousDiscSprite.data, columnX, this);
			}
		}
	}, {
		key: 'handleGridMove',
		value: function handleGridMove() {
			//Determine row clicked
			var localX = this.game.input.x - this.container.x;
			//var localY = pointer.y - this.container.y;
			var columnX = Math.floor(localX / _GameSettings2.default.GRID_TILE_SIZE);

			//Bound to grid
			if (columnX < 0) {
				columnX = 0;
			}
			if (columnX > _GameSettings2.default.GRID_WIDTH - 1) {
				columnX = _GameSettings2.default.GRID_WIDTH - 1;
			}

			var columnWorldX = columnX * _GameSettings2.default.GRID_TILE_SIZE + _GameSettings2.default.GRID_TILE_SIZE * 0.5;

			this.nextDiscSprite.x = columnWorldX;
			this.nextDiscSprite.y = _GameSettings2.default.GRID_TILE_SIZE * 0.5;
		}
	}, {
		key: 'createNextDiscSprite',
		value: function createNextDiscSprite(gridDiscData) {
			this.nextDiscSprite = this.createNewGridDiscSprite(gridDiscData);
		}
	}, {
		key: 'createNewGridDiscSprite',
		value: function createNewGridDiscSprite(gridDiscData) {
			var newGridDiscSprite = new _GridDiscSprite2.default(this.game, gridDiscData);
			this.discsGroup.add(newGridDiscSprite);
			this.discSprites.push(newGridDiscSprite);
			return newGridDiscSprite;
		}
	}, {
		key: 'createPointsSprite',
		value: function createPointsSprite(value, x, y) {
			var newPointsSprite = new _PointsSprite2.default(this.game, value, x, y);
			this.effects.add(newPointsSprite);
			newPointsSprite.animate();
		}
	}, {
		key: 'handleAnimations',
		value: function handleAnimations() {
			var discsToDestroy = [];
			var discsToUpdate = [];
			//determine discs to animate
			for (var i = 0; i < this.discSprites.length; i++) {
				var discSprite = this.discSprites[i];

				if (discSprite.destroyed) {
					discsToDestroy.push(discSprite);
				} else if (discSprite.updated) {
					discsToUpdate.push(discSprite);
				}
			}

			//remove destroyed sprites from main array
			this.discSprites = this.discSprites.filter(function (el) {
				return !discsToDestroy.includes(el);
			});

			if (discsToDestroy.length > 0) {
				this.animationQueue.push(discsToDestroy);
			}

			// Destroy discs
			for (var j = 0; j < discsToDestroy.length; j++) {
				this.destroyDiscAnimation(discsToDestroy[j]);
				discsToDestroy[j].startTween();
			}

			// Animate updated discs
			if (discsToUpdate.length > 0) {
				this.animationQueue.push(discsToUpdate);
			}

			for (var k = 0; k < discsToUpdate.length; k++) {
				this.updateDiscAnimation(discsToUpdate[k]);

				//Nothing being destroyed - animate updates
				if (discsToDestroy.length == 0) {
					discsToUpdate[k].startTween();
				}
			}

			if (this.animationQueue.length > 0) {
				this.isAnimating = true;
			}
		}
	}, {
		key: 'destroyDiscAnimation',
		value: function destroyDiscAnimation(discSprite) {
			discSprite.tweenToDestroy(this.handleDiscAnimationComplete, this);
		}
	}, {
		key: 'updateDiscAnimation',
		value: function updateDiscAnimation(discSprite) {
			discSprite.tweenToWorldPosition(this.handleDiscAnimationComplete, this);
		}
	}, {
		key: 'handleDiscAnimationComplete',
		value: function handleDiscAnimationComplete(discSprite) {
			var destroyOnComplete = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

			var currentAnimationGroup = this.animationQueue[0];

			if (currentAnimationGroup.includes(discSprite)) {
				var index = currentAnimationGroup.indexOf(discSprite);
				currentAnimationGroup.splice(index, 1);
			}

			if (destroyOnComplete) {
				discSprite.destroy();
			}

			if (currentAnimationGroup.length == 0) {
				this.animationQueue.splice(0, 1);
				//Animations complete

				if (this.animationQueue.length == 0) {
					this.isAnimating = false;
					// Check for chain combo
					this.game.commander.execute("GameplayCommand.EvaluateGridMatches", this.game);
				} else {
					for (var i = 0; i < this.animationQueue[0].length; i++) {
						var disc = this.animationQueue[0][i];
						disc.startTween();
					}
				}
			}
		}
	}, {
		key: 'cancelDrop',
		value: function cancelDrop() {
			this.nextDiscSprite = this.previousDiscSprite;
			this.isAnimating = false;
		}
	}, {
		key: 'beginGameOverSequence',
		value: function beginGameOverSequence() {
			this.inGameOverSequence = true;

			var timeline = new TimelineMax({ onComplete: this.endGameplay.bind(this) });
			timeline.set("#game-over", { autoAlpha: 1 });
			this.container.forEach(function (item) {
				timeline.to(item, 0.4, { alpha: 0.3 }, 0);
			});

			timeline.from(".game-over-outline", 1, { drawSVG: "0%", ease: Sine.easeInOut }).add("Fill-in").from("#game-over-fill", 1, { alpha: 0 }, "Fill-in").to(".game-over-outline", 1, { alpha: 0 }, "Fill-in").to("#game-over", 1, { autoAlpha: 0 }, "+=2");
		}
	}, {
		key: 'endGameplay',
		value: function endGameplay() {
			this.game.commander.execute("GameplayCommand.HandleGameOver", this.game);
		}
	}]);

	return _class;
}(Phaser.State);

exports.default = _class;

},{"../../settings/GameSettings":23,"./GridDiscSprite":28,"./GridSprite":29,"./PointsSprite":30}],28:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () {
	function defineProperties(target, props) {
		for (var i = 0; i < props.length; i++) {
			var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
		}
	}return function (Constructor, protoProps, staticProps) {
		if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
	};
}();

var _get = function get(object, property, receiver) {
	if (object === null) object = Function.prototype;var desc = Object.getOwnPropertyDescriptor(object, property);if (desc === undefined) {
		var parent = Object.getPrototypeOf(object);if (parent === null) {
			return undefined;
		} else {
			return get(parent, property, receiver);
		}
	} else if ("value" in desc) {
		return desc.value;
	} else {
		var getter = desc.get;if (getter === undefined) {
			return undefined;
		}return getter.call(receiver);
	}
};

var _GameSettings = require('../../settings/GameSettings');

var _GameSettings2 = _interopRequireDefault(_GameSettings);

function _interopRequireDefault(obj) {
	return obj && obj.__esModule ? obj : { default: obj };
}

function _classCallCheck(instance, Constructor) {
	if (!(instance instanceof Constructor)) {
		throw new TypeError("Cannot call a class as a function");
	}
}

function _possibleConstructorReturn(self, call) {
	if (!self) {
		throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
	}return call && (typeof call === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
	if (typeof superClass !== "function" && superClass !== null) {
		throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
	}subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var _class = function (_Phaser$Sprite) {
	_inherits(_class, _Phaser$Sprite);

	function _class(game, gridDiscData) {
		_classCallCheck(this, _class);

		var sprite;
		if (gridDiscData.armour >= 2) {
			sprite = game.make.sprite(0, 0, 'discsprites', "armour2");
		} else if (gridDiscData.armour == 1) {
			sprite = game.make.sprite(0, 0, 'discsprites', "armour1");
		} else {
			sprite = game.make.sprite(0, 0, 'discsprites', gridDiscData.discValue.toString());
		}

		var _this = _possibleConstructorReturn(this, (_class.__proto__ || Object.getPrototypeOf(_class)).call(this, game, 0, 0, sprite.texture));

		_this.armour2Sprite = game.make.sprite(0, 0, 'discsprites', "armour2");
		_this.armour1Sprite = game.make.sprite(0, 0, 'discsprites', "armour1");
		_this.valueSprite = game.make.sprite(0, 0, 'discsprites', gridDiscData.discValue.toString());

		_this.anchor.set(0.5);
		_this.data = gridDiscData;
		_this.data.onChanged.add(_this._handlePositionUpdated, _this);
		_this.data.onArmourChanged.add(_this._handleArmourUpdated, _this);
		_this.updated = false;
		_this.tween = null;
		return _this;
	}

	_createClass(_class, [{
		key: 'update',
		value: function update() {
			this.z = -this.y;
		}
	}, {
		key: 'changeSprite',
		value: function changeSprite() {
			if (this.data.armour >= 2) {
				this.loadTexture('discsprites', "armour2");
			} else if (this.data.armour == 1) {
				this.loadTexture('discsprites', "armour1");
			} else {
				this.loadTexture('discsprites', this.data.discValue.toString());
			}
		}
	}, {
		key: '_handlePositionUpdated',
		value: function _handlePositionUpdated() {
			this.updated = true;
		}
	}, {
		key: '_handleArmourUpdated',
		value: function _handleArmourUpdated() {
			this.changeSprite();
		}
	}, {
		key: 'gridPositionToWorldPosition',
		value: function gridPositionToWorldPosition() {
			this.x = this.data.x * _GameSettings2.default.GRID_TILE_SIZE + _GameSettings2.default.GRID_TILE_SIZE * 0.5;
			this.y = this.data.y * _GameSettings2.default.GRID_TILE_SIZE + _GameSettings2.default.GRID_TILE_SIZE * 0.5;
		}
	}, {
		key: 'tweenToDestroy',
		value: function tweenToDestroy(onCompleteCallback, scope) {
			this.tween = TweenMax.to(this.scale, 0.5, { x: 0, y: 0, ease: Back.easeIn.config(1.7), onComplete: onCompleteCallback.bind(scope), onCompleteParams: [this, true], paused: true });
		}
	}, {
		key: 'tweenToWorldPosition',
		value: function tweenToWorldPosition(onCompleteCallback, scope) {
			var worldX = this.data.x * _GameSettings2.default.GRID_TILE_SIZE + _GameSettings2.default.GRID_TILE_SIZE * 0.5;
			var worldY = this.data.y * _GameSettings2.default.GRID_TILE_SIZE + _GameSettings2.default.GRID_TILE_SIZE * 0.5;

			this.tween = TweenMax.to(this, 0.25, { x: worldX, y: worldY, ease: Sine.easeInOut, onComplete: onCompleteCallback.bind(scope), onCompleteParams: [this], paused: true });
			this.updated = false;
		}
	}, {
		key: 'destroy',
		value: function destroy() {
			this.data.onChanged.remove(this._handlePositionUpdated, this);
			_get(_class.prototype.__proto__ || Object.getPrototypeOf(_class.prototype), 'destroy', this).call(this);
		}
	}, {
		key: 'startTween',
		value: function startTween() {
			this.tween.play();
		}
	}, {
		key: 'destroyed',
		get: function get() {
			return this.data.destroyed;
		}
	}]);

	return _class;
}(Phaser.Sprite);

exports.default = _class;

},{"../../settings/GameSettings":23}],29:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _GameSettings = require('../../settings/GameSettings');

var _GameSettings2 = _interopRequireDefault(_GameSettings);

function _interopRequireDefault(obj) {
	return obj && obj.__esModule ? obj : { default: obj };
}

function _classCallCheck(instance, Constructor) {
	if (!(instance instanceof Constructor)) {
		throw new TypeError("Cannot call a class as a function");
	}
}

function _possibleConstructorReturn(self, call) {
	if (!self) {
		throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
	}return call && (typeof call === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
	if (typeof superClass !== "function" && superClass !== null) {
		throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
	}subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var _class = function (_Phaser$Sprite) {
	_inherits(_class, _Phaser$Sprite);

	function _class(game) {
		_classCallCheck(this, _class);

		var graphics = game.add.graphics(0, 0);

		graphics.beginFill(0xFF0000, 1);

		for (var y = 0; y < _GameSettings2.default.TRUE_GRID_HEIGHT; y++) {
			for (var x = 0; x < _GameSettings2.default.GRID_WIDTH; x++) {
				//add extra row for the overflow
				if (y == 0) {
					graphics.beginFill(0xFF71ce, 0.1);
				} else {
					graphics.beginFill(0xFF71ce, 1);
				}

				graphics.lineStyle(1, 0x05ffa1, 1);
				graphics.drawRect(x * _GameSettings2.default.GRID_TILE_SIZE, y * _GameSettings2.default.GRID_TILE_SIZE, _GameSettings2.default.GRID_TILE_SIZE, _GameSettings2.default.GRID_TILE_SIZE);
			}
		}

		var _this = _possibleConstructorReturn(this, (_class.__proto__ || Object.getPrototypeOf(_class)).call(this, game, 0, 0, graphics.generateTexture()));

		graphics.destroy();

		_this.inputEnabled = true;
		return _this;
	}

	return _class;
}(Phaser.Sprite);

exports.default = _class;

},{"../../settings/GameSettings":23}],30:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () {
	function defineProperties(target, props) {
		for (var i = 0; i < props.length; i++) {
			var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
		}
	}return function (Constructor, protoProps, staticProps) {
		if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
	};
}();

var _GameSettings = require("../../settings/GameSettings");

var _GameSettings2 = _interopRequireDefault(_GameSettings);

function _interopRequireDefault(obj) {
	return obj && obj.__esModule ? obj : { default: obj };
}

function _classCallCheck(instance, Constructor) {
	if (!(instance instanceof Constructor)) {
		throw new TypeError("Cannot call a class as a function");
	}
}

function _possibleConstructorReturn(self, call) {
	if (!self) {
		throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
	}return call && (typeof call === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
	if (typeof superClass !== "function" && superClass !== null) {
		throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
	}subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var _class = function (_Phaser$Sprite) {
	_inherits(_class, _Phaser$Sprite);

	function _class(game, pointsValue, x, y) {
		_classCallCheck(this, _class);

		var style1 = { font: "40px Permanent Marker", fill: "#b05dff", align: "center" };

		var text = game.make.text(0, 0, "+" + pointsValue, style1);
		text.stroke = '#ffffff';
		text.strokeThickness = 6;

		var _this = _possibleConstructorReturn(this, (_class.__proto__ || Object.getPrototypeOf(_class)).call(this, game, 0, 0, text.generateTexture()));

		_this.anchor.set(0.5);
		_this.x = x * _GameSettings2.default.GRID_TILE_SIZE + _GameSettings2.default.GRID_TILE_SIZE * 0.5;
		_this.y = y * _GameSettings2.default.GRID_TILE_SIZE + _GameSettings2.default.GRID_TILE_SIZE * 0.5;
		return _this;
	}

	_createClass(_class, [{
		key: "animate",
		value: function animate() {
			var timeline = new TimelineMax({ onComplete: this.destroy.bind(this) }).to(this, 1, { y: "-=150", ease: Quad.easeIn }).to(this, 0.5, { alpha: 0 }, 0.5);
		}
	}]);

	return _class;
}(Phaser.Sprite);

exports.default = _class;

},{"../../settings/GameSettings":23}]},{},[1])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJzb3VyY2Uvc2NyaXB0cy9BcHAuanMiLCJzb3VyY2Uvc2NyaXB0cy9jb21tYW5kcy9Db21tYW5kLmpzIiwic291cmNlL3NjcmlwdHMvY29tbWFuZHMvR2FtZUNvbW1hbmRlci5qcyIsInNvdXJjZS9zY3JpcHRzL2NvbW1hbmRzL2FwcC9Cb290c3RyYXBDb21tYW5kLmpzIiwic291cmNlL3NjcmlwdHMvY29tbWFuZHMvYXBwL1BsYXlTb3VuZENvbW1hbmQuanMiLCJzb3VyY2Uvc2NyaXB0cy9jb21tYW5kcy9nYW1lcGxheS9BZGRCb3R0b21Sb3dEaXNjc0NvbW1hbmQuanMiLCJzb3VyY2Uvc2NyaXB0cy9jb21tYW5kcy9nYW1lcGxheS9BbmltYXRlRGlzY3NDb21tYW5kLmpzIiwic291cmNlL3NjcmlwdHMvY29tbWFuZHMvZ2FtZXBsYXkvQ2hlY2tGb3JHYW1lT3ZlckNvbW1hbmQuanMiLCJzb3VyY2Uvc2NyaXB0cy9jb21tYW5kcy9nYW1lcGxheS9DcmVhdGVEaXNjU3ByaXRlQ29tbWFuZC5qcyIsInNvdXJjZS9zY3JpcHRzL2NvbW1hbmRzL2dhbWVwbGF5L0NyZWF0ZU5leHRHcmlkRGlzY0NvbW1hbmQuanMiLCJzb3VyY2Uvc2NyaXB0cy9jb21tYW5kcy9nYW1lcGxheS9DcmVhdGVQb2ludHNTcHJpdGVDb21tYW5kLmpzIiwic291cmNlL3NjcmlwdHMvY29tbWFuZHMvZ2FtZXBsYXkvRHJvcEdyaWREaXNjQ29tbWFuZC5qcyIsInNvdXJjZS9zY3JpcHRzL2NvbW1hbmRzL2dhbWVwbGF5L0Ryb3BOZXh0R3JpZERpc2NDb21tYW5kLmpzIiwic291cmNlL3NjcmlwdHMvY29tbWFuZHMvZ2FtZXBsYXkvRXZhbHVhdGVHcmlkTWF0Y2hlc0NvbW1hbmQuanMiLCJzb3VyY2Uvc2NyaXB0cy9jb21tYW5kcy9nYW1lcGxheS9HZW5lcmF0ZUdyaWREYXRhQ29tbWFuZC5qcyIsInNvdXJjZS9zY3JpcHRzL2NvbW1hbmRzL2dhbWVwbGF5L0hhbmRsZUdhbWVPdmVyQ29tbWFuZC5qcyIsInNvdXJjZS9zY3JpcHRzL2NvbW1hbmRzL2dhbWVwbGF5L0hhbmRsZVR1cm5FbmRDb21tYW5kLmpzIiwic291cmNlL3NjcmlwdHMvY29tbWFuZHMvZ2FtZXBsYXkvSW5pdGlhbGlzZUdhbWVwbGF5Q29tbWFuZC5qcyIsInNvdXJjZS9zY3JpcHRzL21vZGVsL1BsYXllckRhdGEuanMiLCJzb3VyY2Uvc2NyaXB0cy9tb2RlbC9nYW1lcGxheS9HYW1lcGxheURhdGEuanMiLCJzb3VyY2Uvc2NyaXB0cy9tb2RlbC9nYW1lcGxheS9HYW1lcGxheUdyaWREYXRhLmpzIiwic291cmNlL3NjcmlwdHMvbW9kZWwvZ2FtZXBsYXkvR2FtZXBsYXlHcmlkRGlzY0RhdGEuanMiLCJzb3VyY2Uvc2NyaXB0cy9zZXR0aW5ncy9HYW1lU2V0dGluZ3MuanMiLCJzb3VyY2Uvc2NyaXB0cy9zdGF0ZXMvQm9vdFN0YXRlLmpzIiwic291cmNlL3NjcmlwdHMvc3RhdGVzL01haW5NZW51U3RhdGUuanMiLCJzb3VyY2Uvc2NyaXB0cy9zdGF0ZXMvU3BsYXNoU3RhdGUuanMiLCJzb3VyY2Uvc2NyaXB0cy9zdGF0ZXMvZ2FtZXBsYXkvR2FtZXBsYXlTdGF0ZS5qcyIsInNvdXJjZS9zY3JpcHRzL3N0YXRlcy9nYW1lcGxheS9HcmlkRGlzY1Nwcml0ZS5qcyIsInNvdXJjZS9zY3JpcHRzL3N0YXRlcy9nYW1lcGxheS9HcmlkU3ByaXRlLmpzIiwic291cmNlL3NjcmlwdHMvc3RhdGVzL2dhbWVwbGF5L1BvaW50c1Nwcml0ZS5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7O0FDQUEsSUFBQSxhQUFBLFFBQUEsb0JBQUEsQ0FBQTs7OztBQUNBLElBQUEsaUJBQUEsUUFBQSx3QkFBQSxDQUFBOzs7O0FBQ0EsSUFBQSxpQkFBQSxRQUFBLGlDQUFBLENBQUE7Ozs7QUFDQSxJQUFBLGVBQUEsUUFBQSxzQkFBQSxDQUFBOzs7O0FBRUEsSUFBQSxpQkFBQSxRQUFBLDBCQUFBLENBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0lBRU0sTzs7O0FBQ0wsVUFBQSxJQUFBLEdBQWE7QUFBQSxrQkFBQSxJQUFBLEVBQUEsSUFBQTs7QUFDWjs7O0FBR0EsTUFBSSxPQUFKLEdBQUE7QUFDQSxNQUFJLE9BQUosSUFBQTs7QUFFQSxNQUFJLFNBQVM7QUFDWixTQUFNLE9BRE0sSUFBQTtBQUVaLFVBRlksSUFBQTtBQUdaLFdBSFksSUFBQTtBQUlaLFdBQVE7QUFKSSxHQUFiOztBQVBZLE1BQUEsUUFBQSwyQkFBQSxJQUFBLEVBQUEsQ0FBQSxLQUFBLFNBQUEsSUFBQSxPQUFBLGNBQUEsQ0FBQSxJQUFBLENBQUEsRUFBQSxJQUFBLENBQUEsSUFBQSxFQUFBLE1BQUEsQ0FBQSxDQUFBOztBQWdCWixRQUFBLEtBQUEsQ0FBQSxHQUFBLENBQUEsTUFBQSxFQUF1QixZQUF2QixPQUFBO0FBQ0EsUUFBQSxLQUFBLENBQUEsR0FBQSxDQUFBLFVBQUEsRUFBMkIsZ0JBQTNCLE9BQUE7QUFDQSxRQUFBLEtBQUEsQ0FBQSxHQUFBLENBQUEsVUFBQSxFQUEyQixnQkFBM0IsT0FBQTtBQUNBLFFBQUEsS0FBQSxDQUFBLEdBQUEsQ0FBQSxRQUFBLEVBQXlCLGNBQXpCLE9BQUE7O0FBRUEsUUFBQSxTQUFBLEdBQWlCLElBQUksZ0JBQXJCLE9BQWlCLEVBQWpCOztBQUVBLFFBQUEsS0FBQSxDQUFBLEtBQUEsQ0FBQSxNQUFBOztBQXZCWSxTQUFBLEtBQUE7QUF5Qlo7OztFQTFCaUIsT0FBTyxJOztBQTZCMUIsSUFBSSxPQUFPLElBQVgsSUFBVyxFQUFYOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDcENBOztBQUdDLFVBQUEsTUFBQSxHQUFhO0FBQUEsa0JBQUEsSUFBQSxFQUFBLE1BQUE7QUFFWjs7Ozs0QkFFZTtBQUNmLFVBRGUsS0FDZixDQURlLENBQ0Q7QUFDZDs7O3NCQUVpQjtBQUNqQixVQUFBLGdCQUFBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNiRixJQUFBLFdBQUEsUUFBQSxXQUFBLENBQUE7Ozs7QUFDQSxJQUFBLG9CQUFBLFFBQUEsd0JBQUEsQ0FBQTs7Ozs7Ozs7Ozs7Ozs7O0FBR0MsVUFBQSxNQUFBLEdBQWE7QUFBQSxrQkFBQSxJQUFBLEVBQUEsTUFBQTs7QUFDWixPQUFBLFFBQUEsR0FBQSxFQUFBO0FBQ0EsT0FBQSxlQUFBLENBQXFCLG1CQUFBLE9BQUEsQ0FBckIsSUFBQSxFQUE0QyxJQUFJLG1CQUFoRCxPQUE0QyxFQUE1QztBQUNBOzs7O2tDQUVlLEksRUFBTSxPLEVBQTBCO0FBQUEsT0FBakIsV0FBaUIsVUFBQSxNQUFBLEdBQUEsQ0FBQSxJQUFBLFVBQUEsQ0FBQSxNQUFBLFNBQUEsR0FBQSxVQUFBLENBQUEsQ0FBQSxHQUFOLEtBQU07O0FBRS9DLE9BQUksUUFBUSxLQUFSLFFBQUEsSUFBeUIsQ0FBMUIsUUFBQyxJQUF1QyxDQUFBLE9BQUEsWUFBb0IsVUFBL0QsT0FBQSxFQUF1RTtBQUN0RTtBQUNBOztBQUVELFdBQUEsR0FBQSxDQUFZLGdCQUFaLElBQUE7O0FBRUEsUUFBQSxRQUFBLENBQUEsSUFBQSxJQUFBLE9BQUE7QUFDQTs7OzBCQUVPLEksRUFBYztBQUNyQixPQUFHLFFBQVEsS0FBWCxRQUFBLEVBQXlCO0FBQUEsUUFBQSxjQUFBOztBQUFBLFNBQUEsSUFBQSxPQUFBLFVBQUEsTUFBQSxFQURULE9BQ1MsTUFBQSxPQUFBLENBQUEsR0FBQSxPQUFBLENBQUEsR0FBQSxDQUFBLENBQUEsRUFBQSxPQUFBLENBQUEsRUFBQSxPQUFBLElBQUEsRUFBQSxNQUFBLEVBQUE7QUFEVCxVQUNTLE9BQUEsQ0FEVCxJQUNTLFVBQUEsSUFBQSxDQURUO0FBQ1M7O0FBQ3hCO0FBQ0EsV0FBTyxDQUFBLGlCQUFBLEtBQUEsUUFBQSxDQUFBLElBQUEsQ0FBQSxFQUFBLE9BQUEsQ0FBQSxLQUFBLENBQUEsY0FBQSxFQUFQLElBQU8sQ0FBUDtBQUZELElBQUEsTUFHSztBQUNKLFlBQUEsR0FBQSxDQUFZLHNCQUFaLElBQUE7QUFDQSxXQUFBLEtBQUE7QUFDQTtBQUVEOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDN0JGLElBQUEsWUFBQSxRQUFBLFlBQUEsQ0FBQTs7OztBQUVBLElBQUEsY0FBQSxRQUFBLHdCQUFBLENBQUE7Ozs7QUFDQSxJQUFBLDJCQUFBLFFBQUEscUNBQUEsQ0FBQTs7OztBQUNBLElBQUEsNkJBQUEsUUFBQSx1Q0FBQSxDQUFBOzs7O0FBQ0EsSUFBQSw2QkFBQSxRQUFBLHVDQUFBLENBQUE7Ozs7QUFDQSxJQUFBLHVCQUFBLFFBQUEsaUNBQUEsQ0FBQTs7OztBQUNBLElBQUEsMkJBQUEsUUFBQSxxQ0FBQSxDQUFBOzs7O0FBQ0EsSUFBQSw4QkFBQSxRQUFBLHdDQUFBLENBQUE7Ozs7QUFDQSxJQUFBLHdCQUFBLFFBQUEsa0NBQUEsQ0FBQTs7OztBQUNBLElBQUEseUJBQUEsUUFBQSxtQ0FBQSxDQUFBOzs7O0FBQ0EsSUFBQSxvQkFBQSxRQUFBLG9CQUFBLENBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBS0MsVUFBQSxNQUFBLEdBQWE7QUFBQSxrQkFBQSxJQUFBLEVBQUEsTUFBQTs7QUFBQSxTQUFBLDJCQUFBLElBQUEsRUFBQSxDQUFBLE9BQUEsU0FBQSxJQUFBLE9BQUEsY0FBQSxDQUFBLE1BQUEsQ0FBQSxFQUFBLElBQUEsQ0FBQSxJQUFBLENBQUEsQ0FBQTtBQUlaOzs7OzBCQUVPLEksRUFBSztBQUNaLFdBQUEsR0FBQSxDQUFBLGtCQUFBO0FBQ0EsUUFBQSxLQUFBLENBQUEsTUFBQSxHQUFvQixJQUFJLGFBQXhCLE9BQW9CLEVBQXBCOztBQUVBO0FBQ0EsUUFBQSxTQUFBLENBQUEsZUFBQSxDQUErQixtQkFBQSxPQUFBLENBQS9CLElBQUEsRUFBc0QsSUFBSSxtQkFBSixPQUFBLENBQXRELElBQXNELENBQXREO0FBQ0EsUUFBQSxTQUFBLENBQUEsZUFBQSxDQUErQiw0QkFBQSxPQUFBLENBQS9CLElBQUEsRUFBK0QsSUFBSSw0QkFBSixPQUFBLENBQS9ELElBQStELENBQS9EO0FBQ0EsUUFBQSxTQUFBLENBQUEsZUFBQSxDQUErQiwwQkFBQSxPQUFBLENBQS9CLElBQUEsRUFBNkQsSUFBSSwwQkFBakUsT0FBNkQsRUFBN0Q7QUFDQSxRQUFBLFNBQUEsQ0FBQSxlQUFBLENBQStCLHNCQUFBLE9BQUEsQ0FBL0IsSUFBQSxFQUF5RCxJQUFJLHNCQUE3RCxPQUF5RCxFQUF6RDtBQUNBLFFBQUEsU0FBQSxDQUFBLGVBQUEsQ0FBK0IsMEJBQUEsT0FBQSxDQUEvQixJQUFBLEVBQTZELElBQUksMEJBQWpFLE9BQTZELEVBQTdEO0FBQ0EsUUFBQSxTQUFBLENBQUEsZUFBQSxDQUErQiw2QkFBQSxPQUFBLENBQS9CLElBQUEsRUFBZ0UsSUFBSSw2QkFBcEUsT0FBZ0UsRUFBaEU7QUFDQSxRQUFBLFNBQUEsQ0FBQSxlQUFBLENBQStCLHVCQUFBLE9BQUEsQ0FBL0IsSUFBQSxFQUEwRCxJQUFJLHVCQUE5RCxPQUEwRCxFQUExRDtBQUNBLFFBQUEsU0FBQSxDQUFBLGVBQUEsQ0FBK0Isd0JBQUEsT0FBQSxDQUEvQixJQUFBLEVBQTJELElBQUksd0JBQS9ELE9BQTJELEVBQTNEOztBQUVBLFVBQUEsSUFBQTtBQUVBOzs7c0JBRWlCO0FBQ2pCLFVBQUEsc0JBQUE7QUFDQTs7OztFQTVCMkIsVUFBQSxPOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNkN0IsSUFBQSxZQUFBLFFBQUEsWUFBQSxDQUFBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUtDLFVBQUEsTUFBQSxDQUFBLElBQUEsRUFBaUI7QUFBQSxrQkFBQSxJQUFBLEVBQUEsTUFBQTs7QUFBQSxNQUFBLFFBQUEsMkJBQUEsSUFBQSxFQUFBLENBQUEsT0FBQSxTQUFBLElBQUEsT0FBQSxjQUFBLENBQUEsTUFBQSxDQUFBLEVBQUEsSUFBQSxDQUFBLElBQUEsQ0FBQSxDQUFBOztBQUdoQixRQUFBLE1BQUEsR0FBQSxFQUFBOztBQUVBLFFBQUEsTUFBQSxDQUFBLFlBQUEsSUFBNEIsS0FBQSxHQUFBLENBQUEsS0FBQSxDQUE1QixVQUE0QixDQUE1QjtBQUNBLFFBQUEsTUFBQSxDQUFBLFdBQUEsSUFBMkIsS0FBQSxHQUFBLENBQUEsS0FBQSxDQUEzQixXQUEyQixDQUEzQjtBQUNBLFFBQUEsTUFBQSxDQUFBLFdBQUEsSUFBMkIsS0FBQSxHQUFBLENBQUEsS0FBQSxDQUEzQixXQUEyQixDQUEzQjtBQUNBLFFBQUEsTUFBQSxDQUFBLFVBQUEsSUFBMEIsS0FBQSxHQUFBLENBQUEsS0FBQSxDQUExQixVQUEwQixDQUExQjtBQUNBLFFBQUEsTUFBQSxDQUFBLFNBQUEsSUFBeUIsS0FBQSxHQUFBLENBQUEsS0FBQSxDQUF6QixTQUF5QixDQUF6QjtBQUNBLFFBQUEsTUFBQSxDQUFBLFNBQUEsSUFBeUIsS0FBQSxHQUFBLENBQUEsS0FBQSxDQUF6QixTQUF5QixDQUF6QjtBQUNBLFFBQUEsTUFBQSxDQUFBLFFBQUEsSUFBd0IsS0FBQSxHQUFBLENBQUEsS0FBQSxDQUF4QixRQUF3QixDQUF4QjtBQVhnQixTQUFBLEtBQUE7QUFZaEI7Ozs7MEJBRU8sUyxFQUFVO0FBQ2pCLE9BQUcsYUFBYSxLQUFoQixNQUFBLEVBQTRCO0FBQzNCLFNBQUEsTUFBQSxDQUFBLFNBQUEsRUFBQSxJQUFBO0FBQ0EsV0FBQSxJQUFBO0FBRkQsSUFBQSxNQUdLO0FBQ0osWUFBQSxHQUFBLENBQVksb0JBQVosU0FBQTtBQUNBLFdBQUEsS0FBQTtBQUNBO0FBQ0Q7OztzQkFFaUI7QUFDakIsVUFBQSxzQkFBQTtBQUNBOzs7O0VBNUIyQixVQUFBLE87Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ0g3QixJQUFBLFlBQUEsUUFBQSxZQUFBLENBQUE7Ozs7QUFDQSxJQUFBLGdCQUFBLFFBQUEsNkJBQUEsQ0FBQTs7OztBQUNBLElBQUEsd0JBQUEsUUFBQSwyQ0FBQSxDQUFBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUtDLFVBQUEsTUFBQSxDQUFBLGFBQUEsRUFBMEI7QUFBQSxrQkFBQSxJQUFBLEVBQUEsTUFBQTs7QUFBQSxNQUFBLFFBQUEsMkJBQUEsSUFBQSxFQUFBLENBQUEsT0FBQSxTQUFBLElBQUEsT0FBQSxjQUFBLENBQUEsTUFBQSxDQUFBLEVBQUEsSUFBQSxDQUFBLElBQUEsQ0FBQSxDQUFBOztBQUV6QixRQUFBLGFBQUEsR0FBQSxhQUFBO0FBRnlCLFNBQUEsS0FBQTtBQUd6Qjs7OzswQkFFTyxJLEVBQUs7QUFDWixPQUFJLE9BQU8sS0FBQSxLQUFBLENBQVgsUUFBQTtBQUNBLFFBQUEsVUFBQTs7QUFFQSxRQUFLLElBQUksSUFBVCxDQUFBLEVBQWdCLElBQUksZUFBQSxPQUFBLENBQXBCLFVBQUEsRUFBQSxHQUFBLEVBQWtEO0FBQ2pELFFBQUksV0FBVyxLQUFBLFVBQUEsQ0FBQSxJQUFBLEVBQUEsQ0FBQSxFQUF5QixlQUFBLE9BQUEsQ0FBQSxnQkFBQSxHQUF4QyxDQUFlLENBQWY7QUFDQSxTQUFBLFNBQUEsQ0FBQSxPQUFBLENBQUEsa0NBQUEsRUFBQSxJQUFBLEVBQUEsUUFBQSxFQUFBLElBQUE7QUFDQTs7QUFFRCxRQUFBLFNBQUEsQ0FBQSxPQUFBLENBQUEsOEJBQUE7O0FBRUEsVUFBQSxJQUFBO0FBQ0E7Ozs2QkFFVSxJLEVBQU0sQyxFQUFHLEMsRUFBRTtBQUNyQixPQUFJLE1BQU0sZUFBQSxPQUFBLENBQVYsY0FBQTtBQUNBLE9BQUksTUFBTSxlQUFBLE9BQUEsQ0FBVixjQUFBOztBQUVBLE9BQUksWUFBWSxLQUFBLEtBQUEsQ0FBVyxLQUFBLE1BQUEsTUFBaUIsTUFBQSxHQUFBLEdBQTVCLENBQVcsQ0FBWCxJQUFoQixHQUFBOztBQUVBLE9BQUksV0FBVyxJQUFJLHVCQUFKLE9BQUEsQ0FBQSxTQUFBLEVBQW9DLGVBQUEsT0FBQSxDQUFuRCxvQkFBZSxDQUFmO0FBQ0EsUUFBQSxpQkFBQSxDQUFBLFFBQUEsRUFBQSxDQUFBLEVBQUEsQ0FBQTs7QUFFQSxVQUFBLFFBQUE7QUFFQTs7O3NCQUVpQjtBQUNqQixVQUFBLG1DQUFBO0FBQ0E7Ozs7RUFwQzJCLFVBQUEsTzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDTDdCLElBQUEsWUFBQSxRQUFBLFlBQUEsQ0FBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFLQyxVQUFBLE1BQUEsQ0FBQSxhQUFBLEVBQTBCO0FBQUEsa0JBQUEsSUFBQSxFQUFBLE1BQUE7O0FBQUEsTUFBQSxRQUFBLDJCQUFBLElBQUEsRUFBQSxDQUFBLE9BQUEsU0FBQSxJQUFBLE9BQUEsY0FBQSxDQUFBLE1BQUEsQ0FBQSxFQUFBLElBQUEsQ0FBQSxJQUFBLENBQUEsQ0FBQTs7QUFFekIsUUFBQSxjQUFBLEdBQUEsYUFBQTtBQUZ5QixTQUFBLEtBQUE7QUFHekI7Ozs7MEJBRU8sSSxFQUFLO0FBQ1osUUFBQSxjQUFBLENBQUEsZ0JBQUE7O0FBRUEsVUFBQSxJQUFBO0FBQ0E7OztzQkFFaUI7QUFDakIsVUFBQSw4QkFBQTtBQUNBOzs7O0VBZjJCLFVBQUEsTzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDSDdCLElBQUEsWUFBQSxRQUFBLFlBQUEsQ0FBQTs7OztBQUNBLElBQUEsZ0JBQUEsUUFBQSw2QkFBQSxDQUFBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUlDLFVBQUEsTUFBQSxDQUFBLGFBQUEsRUFBMEI7QUFBQSxrQkFBQSxJQUFBLEVBQUEsTUFBQTs7QUFBQSxNQUFBLFFBQUEsMkJBQUEsSUFBQSxFQUFBLENBQUEsT0FBQSxTQUFBLElBQUEsT0FBQSxjQUFBLENBQUEsTUFBQSxDQUFBLEVBQUEsSUFBQSxDQUFBLElBQUEsQ0FBQSxDQUFBOztBQUV6QixRQUFBLGFBQUEsR0FBQSxhQUFBO0FBRnlCLFNBQUEsS0FBQTtBQUd6Qjs7OzswQkFFTyxJLEVBQUs7QUFDWixPQUFJLE9BQU8sS0FBQSxLQUFBLENBQVgsUUFBQTtBQUNBO0FBQ0EsT0FBSSxhQUFKLElBQUE7O0FBRUEsUUFBSyxJQUFJLElBQVQsQ0FBQSxFQUFnQixJQUFJLGVBQUEsT0FBQSxDQUFBLFVBQUEsR0FBMEIsZUFBQSxPQUFBLENBQTlDLGdCQUFBLEVBQUEsR0FBQSxFQUFrRjtBQUNqRixRQUFJLFdBQVcsS0FBQSxJQUFBLENBQWYsQ0FBZSxDQUFmO0FBQ0EsUUFBSSxJQUFJLElBQUksZUFBQSxPQUFBLENBQVosVUFBQTtBQUNBLFFBQUksSUFBSSxLQUFBLEtBQUEsQ0FBVyxJQUFJLGVBQUEsT0FBQSxDQUF2QixVQUFRLENBQVI7O0FBRUEsUUFBRyxLQUFILENBQUEsRUFBVTtBQUNULFNBQUcsWUFBSCxJQUFBLEVBQW9CO0FBQ25CLG1CQUFBLElBQUE7QUFDQTtBQUNBO0FBSkYsS0FBQSxNQUtLO0FBQ0osU0FBRyxZQUFILElBQUEsRUFBb0I7QUFDbkIsbUJBQUEsS0FBQTtBQUNBO0FBQ0E7QUFDRDtBQUNEOztBQUVELE9BQUEsVUFBQSxFQUFjO0FBQ2IsU0FBQSxhQUFBLENBQUEscUJBQUE7QUFDQSxTQUFBLFNBQUEsQ0FBQSxPQUFBLENBQUEsc0JBQUEsRUFBQSxXQUFBOztBQUVBLFdBQUEsSUFBQTtBQUpELElBQUEsTUFLSztBQUNKLFNBQUEsS0FBQSxDQUFBLFFBQUEsQ0FBQSxhQUFBO0FBQ0EsU0FBQSxTQUFBLENBQUEsT0FBQSxDQUFBLDhCQUFBOztBQUVBLFdBQUEsS0FBQTtBQUNBO0FBQ0Q7OztzQkFFaUI7QUFDakIsVUFBQSxrQ0FBQTtBQUNBOzs7O0VBN0MyQixVQUFBLE87Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ0g3QixJQUFBLFlBQUEsUUFBQSxZQUFBLENBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBS0MsVUFBQSxNQUFBLENBQUEsYUFBQSxFQUEwQjtBQUFBLGtCQUFBLElBQUEsRUFBQSxNQUFBOztBQUFBLE1BQUEsUUFBQSwyQkFBQSxJQUFBLEVBQUEsQ0FBQSxPQUFBLFNBQUEsSUFBQSxPQUFBLGNBQUEsQ0FBQSxNQUFBLENBQUEsRUFBQSxJQUFBLENBQUEsSUFBQSxDQUFBLENBQUE7O0FBRXpCLFFBQUEsYUFBQSxHQUFBLGFBQUE7QUFGeUIsU0FBQSxLQUFBO0FBR3pCOzs7OzBCQUVPLEksRUFBTSxZLEVBQXlDO0FBQUEsT0FBM0IscUJBQTJCLFVBQUEsTUFBQSxHQUFBLENBQUEsSUFBQSxVQUFBLENBQUEsTUFBQSxTQUFBLEdBQUEsVUFBQSxDQUFBLENBQUEsR0FBTixLQUFNOztBQUN0RCxPQUFJLFNBQVMsS0FBQSxhQUFBLENBQUEsdUJBQUEsQ0FBYixZQUFhLENBQWI7O0FBRUEsT0FBQSxrQkFBQSxFQUFzQjtBQUNyQixXQUFBLDJCQUFBO0FBQ0E7O0FBRUQsVUFBQSxJQUFBO0FBQ0E7OztzQkFFaUI7QUFDakIsVUFBQSxrQ0FBQTtBQUNBOzs7O0VBbkIyQixVQUFBLE87Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ0g3QixJQUFBLFlBQUEsUUFBQSxZQUFBLENBQUE7Ozs7QUFFQSxJQUFBLHdCQUFBLFFBQUEsMkNBQUEsQ0FBQTs7OztBQUNBLElBQUEsZ0JBQUEsUUFBQSw2QkFBQSxDQUFBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUlDLFVBQUEsTUFBQSxDQUFBLGFBQUEsRUFBMEI7QUFBQSxrQkFBQSxJQUFBLEVBQUEsTUFBQTs7QUFBQSxNQUFBLFFBQUEsMkJBQUEsSUFBQSxFQUFBLENBQUEsT0FBQSxTQUFBLElBQUEsT0FBQSxjQUFBLENBQUEsTUFBQSxDQUFBLEVBQUEsSUFBQSxDQUFBLElBQUEsQ0FBQSxDQUFBOztBQUV6QixRQUFBLGFBQUEsR0FBQSxhQUFBO0FBRnlCLFNBQUEsS0FBQTtBQUd6Qjs7OzswQkFFTyxJLEVBQUs7QUFDWixPQUFJLE1BQU0sZUFBQSxPQUFBLENBQVYsY0FBQTtBQUNBLE9BQUksTUFBTSxlQUFBLE9BQUEsQ0FBVixjQUFBOztBQUVBLE9BQUksWUFBWSxLQUFBLEtBQUEsQ0FBVyxLQUFBLE1BQUEsTUFBaUIsTUFBQSxHQUFBLEdBQTVCLENBQVcsQ0FBWCxJQUFoQixHQUFBO0FBQ0EsT0FBSSxjQUFlLEtBQUEsTUFBQSxLQUFpQixlQUFBLE9BQUEsQ0FBbEIscUJBQUMsR0FBc0QsZUFBQSxPQUFBLENBQXZELG9CQUFDLEdBQW5CLENBQUE7O0FBR0EsT0FBSSxXQUFXLElBQUksdUJBQUosT0FBQSxDQUFBLFNBQUEsRUFBZixXQUFlLENBQWY7QUFDQSxRQUFBLGFBQUEsQ0FBQSxvQkFBQSxDQUFBLFFBQUE7O0FBRUEsVUFBQSxJQUFBO0FBQ0E7OztzQkFFaUI7QUFDakIsVUFBQSxvQ0FBQTtBQUNBOzs7O0VBdkIyQixVQUFBLE87Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ0w3QixJQUFBLFlBQUEsUUFBQSxZQUFBLENBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBSUMsVUFBQSxNQUFBLENBQUEsYUFBQSxFQUEwQjtBQUFBLGtCQUFBLElBQUEsRUFBQSxNQUFBOztBQUFBLE1BQUEsUUFBQSwyQkFBQSxJQUFBLEVBQUEsQ0FBQSxPQUFBLFNBQUEsSUFBQSxPQUFBLGNBQUEsQ0FBQSxNQUFBLENBQUEsRUFBQSxJQUFBLENBQUEsSUFBQSxDQUFBLENBQUE7O0FBRXpCLFFBQUEsYUFBQSxHQUFBLGFBQUE7QUFGeUIsU0FBQSxLQUFBO0FBR3pCOzs7OzBCQUVPLE0sRUFBUSxDLEVBQUcsQyxFQUFFO0FBQ3BCLFFBQUEsYUFBQSxDQUFBLGtCQUFBLENBQUEsTUFBQSxFQUFBLENBQUEsRUFBQSxDQUFBOztBQUVBLFVBQUEsSUFBQTtBQUNBOzs7c0JBRWlCO0FBQ2pCLFVBQUEsb0NBQUE7QUFDQTs7OztFQWYyQixVQUFBLE87Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ0Y3QixJQUFBLFlBQUEsUUFBQSxZQUFBLENBQUE7Ozs7QUFDQSxJQUFBLHdCQUFBLFFBQUEsMkNBQUEsQ0FBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFJQyxVQUFBLE1BQUEsR0FBYTtBQUFBLGtCQUFBLElBQUEsRUFBQSxNQUFBOztBQUFBLFNBQUEsMkJBQUEsSUFBQSxFQUFBLENBQUEsT0FBQSxTQUFBLElBQUEsT0FBQSxjQUFBLENBQUEsTUFBQSxDQUFBLEVBQUEsSUFBQSxDQUFBLElBQUEsQ0FBQSxDQUFBO0FBR1o7Ozs7MEJBRU8sSSxFQUFNLFEsRUFBK0I7QUFBQSxPQUFyQixlQUFxQixVQUFBLE1BQUEsR0FBQSxDQUFBLElBQUEsVUFBQSxDQUFBLE1BQUEsU0FBQSxHQUFBLFVBQUEsQ0FBQSxDQUFBLEdBQU4sS0FBTTs7QUFDNUMsV0FBQSxHQUFBLENBQVksZ0JBQWdCLFNBQWhCLFNBQUEsR0FBQSxhQUFBLEdBQXFELFNBQWpFLENBQUE7O0FBRUEsT0FBSSxLQUFBLEtBQUEsQ0FBQSxRQUFBLENBQUEsWUFBQSxDQUFBLFFBQUEsRUFBMkMsU0FBL0MsQ0FBSSxDQUFKLEVBQTJEO0FBQzFELFFBQUEsWUFBQSxFQUFnQjtBQUNmLFVBQUEsU0FBQSxDQUFBLE9BQUEsQ0FBQSxrQ0FBQSxFQUFBLElBQUEsRUFBQSxRQUFBO0FBQ0E7O0FBRUQsV0FBQSxJQUFBO0FBTEQsSUFBQSxNQU1NO0FBQ0wsV0FBQSxLQUFBO0FBQ0M7QUFHRjs7O3NCQUVpQjtBQUNqQixVQUFBLDhCQUFBO0FBQ0E7Ozs7RUF6QjJCLFVBQUEsTzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDSDdCLElBQUEsWUFBQSxRQUFBLFlBQUEsQ0FBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFJQyxVQUFBLE1BQUEsR0FBYTtBQUFBLGtCQUFBLElBQUEsRUFBQSxNQUFBOztBQUFBLFNBQUEsMkJBQUEsSUFBQSxFQUFBLENBQUEsT0FBQSxTQUFBLElBQUEsT0FBQSxjQUFBLENBQUEsTUFBQSxDQUFBLEVBQUEsSUFBQSxDQUFBLElBQUEsQ0FBQSxDQUFBO0FBR1o7Ozs7MEJBRU8sSSxFQUFNLFEsRUFBVSxNLEVBQVEsUyxFQUFVO0FBQ3pDLFlBQUEsQ0FBQSxHQUFBLE1BQUE7O0FBRUEsT0FBSSxpQkFBaUIsS0FBQSxTQUFBLENBQUEsT0FBQSxDQUFBLDhCQUFBLEVBQUEsSUFBQSxFQUFBLFFBQUEsRUFBckIsS0FBcUIsQ0FBckI7O0FBRUEsT0FBQSxjQUFBLEVBQWtCO0FBQ2pCLFNBQUEsU0FBQSxDQUFBLE9BQUEsQ0FBQSxzQkFBQSxFQUFBLFdBQUE7QUFDQSxjQUFBLGdCQUFBO0FBQ0EsV0FBQSxJQUFBO0FBSEQsSUFBQSxNQUlLO0FBQ0osU0FBQSxTQUFBLENBQUEsT0FBQSxDQUFBLHNCQUFBLEVBQUEsUUFBQTs7QUFFQSxjQUFBLFVBQUE7QUFDQSxXQUFBLEtBQUE7QUFDQTtBQUNEOzs7c0JBRWlCO0FBQ2pCLFVBQUEsa0NBQUE7QUFDQTs7OztFQTFCMkIsVUFBQSxPOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNGN0IsSUFBQSxZQUFBLFFBQUEsWUFBQSxDQUFBOzs7O0FBQ0EsSUFBQSxnQkFBQSxRQUFBLDZCQUFBLENBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBSUMsVUFBQSxNQUFBLEdBQWE7QUFBQSxrQkFBQSxJQUFBLEVBQUEsTUFBQTs7QUFBQSxTQUFBLDJCQUFBLElBQUEsRUFBQSxDQUFBLE9BQUEsU0FBQSxJQUFBLE9BQUEsY0FBQSxDQUFBLE1BQUEsQ0FBQSxFQUFBLElBQUEsQ0FBQSxJQUFBLENBQUEsQ0FBQTtBQUdaOzs7OzBCQUVPLEksRUFBTSxRLEVBQVM7QUFDdEI7QUFDQSxPQUFJLE9BQU8sS0FBQSxLQUFBLENBQVgsUUFBQTtBQUNBLE9BQUksYUFBYSxLQUFqQixtQkFBaUIsRUFBakI7QUFDQSxPQUFJLGdCQUFnQixLQUFwQixzQkFBb0IsRUFBcEI7QUFDQSxPQUFJLGFBQWEsV0FBQSxNQUFBLENBQWpCLGFBQWlCLENBQWpCOztBQUVBLE9BQUksa0JBQUosRUFBQTtBQUNBLFFBQUssSUFBSSxJQUFULENBQUEsRUFBZ0IsSUFBSSxXQUFwQixNQUFBLEVBQUEsR0FBQSxFQUE0Qzs7QUFFM0MsU0FBQSxLQUFBLENBQUEsUUFBQSxDQUFBLGFBQUE7O0FBRUEsUUFBSSxjQUFjLFdBQWxCLENBQWtCLENBQWxCO0FBQ0EsUUFBRyxDQUFDLGdCQUFBLFFBQUEsQ0FBeUIsWUFBN0IsQ0FBSSxDQUFKLEVBQTRDO0FBQzNDLHFCQUFBLElBQUEsQ0FBcUIsWUFBckIsQ0FBQTtBQUNBOztBQUVELFNBQUEsb0JBQUEsQ0FBMEIsWUFBMUIsQ0FBQSxFQUF5QyxZQUF6QyxDQUFBO0FBQ0EsU0FBQSxtQkFBQSxDQUFBLElBQUEsRUFBK0IsWUFBL0IsQ0FBQSxFQUE4QyxZQUE5QyxDQUFBO0FBQ0EsZ0JBQUEsU0FBQSxHQUFBLElBQUE7QUFDQSxTQUFBLFNBQUEsQ0FBQSxPQUFBLENBQUEsb0NBQUEsRUFBNkQsS0FBQSxLQUFBLENBQUEsUUFBQSxDQUE3RCxhQUE2RCxFQUE3RCxFQUFrRyxZQUFsRyxDQUFBLEVBQWlILFlBQWpILENBQUE7QUFDQTs7QUFFRCxRQUFLLElBQUksSUFBVCxDQUFBLEVBQWdCLElBQUksZ0JBQXBCLE1BQUEsRUFBQSxHQUFBLEVBQWlEO0FBQ2hELFFBQUksSUFBSSxnQkFBUixDQUFRLENBQVI7QUFDQSxTQUFBLFlBQUEsQ0FBQSxDQUFBO0FBQ0E7O0FBRUQsUUFBQSxTQUFBLENBQUEsT0FBQSxDQUFBLGtDQUFBLEVBQUEsSUFBQTs7QUFFQSxPQUFHLFdBQUEsTUFBQSxJQUFILENBQUEsRUFBMEI7QUFDekI7QUFDQSxRQUFHLEtBQUEsS0FBQSxDQUFBLFFBQUEsQ0FBSCxlQUFHLEVBQUgsRUFBeUM7QUFDeEMsVUFBQSxTQUFBLENBQUEsT0FBQSxDQUFBLCtCQUFBLEVBQUEsSUFBQTtBQURELEtBQUEsTUFFSztBQUNKLFVBQUEsS0FBQSxDQUFBLFFBQUEsQ0FBQSxRQUFBO0FBQ0EsVUFBQSxTQUFBLENBQUEsT0FBQSxDQUFBLG9DQUFBLEVBQUEsSUFBQTtBQUNBLFVBQUEsS0FBQSxDQUFBLFFBQUEsQ0FBQSxVQUFBO0FBQ0E7QUFSRixJQUFBLE1BU0s7QUFDSixTQUFBLFNBQUEsQ0FBQSxPQUFBLENBQUEsc0JBQUEsRUFBQSxZQUFBO0FBQ0E7O0FBRUQsVUFBTyxXQUFBLE1BQUEsR0FBUCxDQUFBO0FBQ0E7OztzQ0FFbUIsSSxFQUFNLEMsRUFBRyxDLEVBQUU7QUFDOUI7QUFDQSxPQUFHLElBQUgsQ0FBQSxFQUFTO0FBQ1IsUUFBSSxXQUFXLEtBQUEsaUJBQUEsQ0FBdUIsSUFBdkIsQ0FBQSxFQUFmLENBQWUsQ0FBZjs7QUFFQSxRQUFHLFlBQUgsSUFBQSxFQUFvQjtBQUNuQixjQUFBLE1BQUE7QUFDQTtBQUNEO0FBQ0Q7QUFDQSxPQUFHLElBQUksZUFBQSxPQUFBLENBQUEsVUFBQSxHQUFQLENBQUEsRUFBaUM7QUFDaEMsUUFBSSxZQUFZLEtBQUEsaUJBQUEsQ0FBdUIsSUFBdkIsQ0FBQSxFQUFoQixDQUFnQixDQUFoQjs7QUFFQSxRQUFHLGFBQUgsSUFBQSxFQUFxQjtBQUNwQixlQUFBLE1BQUE7QUFDQTtBQUNEO0FBQ0Q7QUFDQSxPQUFHLElBQUgsQ0FBQSxFQUFTO0FBQ1IsUUFBSSxVQUFVLEtBQUEsaUJBQUEsQ0FBQSxDQUFBLEVBQTBCLElBQXhDLENBQWMsQ0FBZDs7QUFFQSxRQUFHLFdBQUgsSUFBQSxFQUFtQjtBQUNsQixhQUFBLE1BQUE7QUFDQTtBQUNEO0FBQ0Q7QUFDQSxPQUFHLElBQUksZUFBQSxPQUFBLENBQUEsZ0JBQUEsR0FBUCxDQUFBLEVBQXVDO0FBQ3RDLFFBQUksYUFBYSxLQUFBLGlCQUFBLENBQUEsQ0FBQSxFQUEwQixJQUEzQyxDQUFpQixDQUFqQjs7QUFFQSxRQUFHLGNBQUgsSUFBQSxFQUFzQjtBQUNyQixnQkFBQSxNQUFBO0FBQ0E7QUFDRDtBQUNEOzs7c0JBRWlCO0FBQ2pCLFVBQUEscUNBQUE7QUFDQTs7OztFQTFGMkIsVUFBQSxPOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNIN0IsSUFBQSxZQUFBLFFBQUEsWUFBQSxDQUFBOzs7O0FBQ0EsSUFBQSxvQkFBQSxRQUFBLHVDQUFBLENBQUE7Ozs7QUFDQSxJQUFBLHdCQUFBLFFBQUEsMkNBQUEsQ0FBQTs7OztBQUNBLElBQUEsZ0JBQUEsUUFBQSw2QkFBQSxDQUFBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUlDLFVBQUEsTUFBQSxHQUFhO0FBQUEsa0JBQUEsSUFBQSxFQUFBLE1BQUE7O0FBQUEsU0FBQSwyQkFBQSxJQUFBLEVBQUEsQ0FBQSxPQUFBLFNBQUEsSUFBQSxPQUFBLGNBQUEsQ0FBQSxNQUFBLENBQUEsRUFBQSxJQUFBLENBQUEsSUFBQSxDQUFBLENBQUE7QUFHWjs7OzswQkFFTyxJLEVBQUs7QUFDWixPQUFJLFVBQVUsSUFBSSxtQkFBbEIsT0FBYyxFQUFkO0FBQ0EsUUFBQSxLQUFBLENBQUEsUUFBQSxHQUFBLE9BQUE7O0FBRUEsUUFBSyxJQUFJLElBQVQsQ0FBQSxFQUFnQixJQUFJLGVBQUEsT0FBQSxDQUFwQixjQUFBLEVBQUEsR0FBQSxFQUFzRDtBQUNyRCxRQUFJLGFBQWEsS0FBQSxnQkFBQSxDQUFqQixPQUFpQixDQUFqQjtBQUNBLFNBQUEsU0FBQSxDQUFBLE9BQUEsQ0FBQSxrQ0FBQSxFQUFBLElBQUEsRUFBQSxVQUFBLEVBQUEsSUFBQTtBQUNBOztBQUVELFVBQUEsSUFBQTtBQUNBOzs7bUNBRWdCLEksRUFBSztBQUNyQixPQUFJLE1BQU0sZUFBQSxPQUFBLENBQVYsY0FBQTtBQUNBLE9BQUksTUFBTSxlQUFBLE9BQUEsQ0FBVixjQUFBOztBQUVBLE9BQUksWUFBWSxLQUFBLEtBQUEsQ0FBVyxLQUFBLE1BQUEsTUFBaUIsTUFBQSxHQUFBLEdBQTVCLENBQVcsQ0FBWCxJQUFoQixHQUFBOztBQUVBO0FBQ0EsT0FBSSxZQUFZLGVBQUEsT0FBQSxDQUFBLGdCQUFBLEdBQWdDLGVBQUEsT0FBQSxDQUFoRCw0QkFBQTs7QUFFQSxPQUFJLGlCQUFpQixLQUFyQixlQUFxQixFQUFyQjtBQUNBLE9BQUkscUJBQXFCLEtBQUEsWUFBQSxDQUF6QixjQUF5QixDQUF6Qjs7QUFFQSxVQUFNLHFCQUFOLFNBQUEsRUFBcUM7QUFDcEMscUJBQWlCLEtBQWpCLGVBQWlCLEVBQWpCO0FBQ0EseUJBQXFCLEtBQUEsWUFBQSxDQUFyQixjQUFxQixDQUFyQjtBQUNBOztBQUVELE9BQUksY0FBZSxLQUFBLE1BQUEsS0FBaUIsZUFBQSxPQUFBLENBQWxCLHFCQUFDLEdBQXNELGVBQUEsT0FBQSxDQUF2RCxvQkFBQyxHQUFuQixDQUFBOztBQUVBLE9BQUksV0FBVyxJQUFJLHVCQUFKLE9BQUEsQ0FBQSxTQUFBLEVBQWYsV0FBZSxDQUFmO0FBQ0EsUUFBQSxZQUFBLENBQUEsUUFBQSxFQUFBLGNBQUE7O0FBRUEsVUFBQSxRQUFBO0FBRUE7OztvQ0FFZ0I7QUFDaEIsVUFBTyxLQUFBLEtBQUEsQ0FBVyxLQUFBLE1BQUEsS0FBZ0IsZUFBQSxPQUFBLENBQWxDLFVBQU8sQ0FBUDtBQUNBOzs7c0JBRWlCO0FBQ2pCLFVBQUEsa0NBQUE7QUFDQTs7OztFQW5EMkIsVUFBQSxPOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNMN0IsSUFBQSxZQUFBLFFBQUEsWUFBQSxDQUFBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUlDLFVBQUEsTUFBQSxHQUFhO0FBQUEsa0JBQUEsSUFBQSxFQUFBLE1BQUE7O0FBQUEsU0FBQSwyQkFBQSxJQUFBLEVBQUEsQ0FBQSxPQUFBLFNBQUEsSUFBQSxPQUFBLGNBQUEsQ0FBQSxNQUFBLENBQUEsRUFBQSxJQUFBLENBQUEsSUFBQSxDQUFBLENBQUE7QUFFWjs7OzswQkFFTyxJLEVBQUs7QUFDWixPQUFJLFFBQVEsS0FBQSxLQUFBLENBQUEsUUFBQSxDQUFaLEtBQUE7QUFDQSxRQUFBLEtBQUEsQ0FBQSxNQUFBLENBQUEsWUFBQSxDQUFBLEtBQUE7QUFDQSxRQUFBLEtBQUEsQ0FBQSxLQUFBLENBQUEsVUFBQTs7QUFFQSxVQUFBLElBQUE7QUFDQTs7O3NCQUVpQjtBQUNqQixVQUFBLGdDQUFBO0FBQ0E7Ozs7RUFoQjJCLFVBQUEsTzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDRjdCLElBQUEsWUFBQSxRQUFBLFlBQUEsQ0FBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFLQyxVQUFBLE1BQUEsR0FBYTtBQUFBLGtCQUFBLElBQUEsRUFBQSxNQUFBOztBQUFBLFNBQUEsMkJBQUEsSUFBQSxFQUFBLENBQUEsT0FBQSxTQUFBLElBQUEsT0FBQSxjQUFBLENBQUEsTUFBQSxDQUFBLEVBQUEsSUFBQSxDQUFBLElBQUEsQ0FBQSxDQUFBO0FBRVo7Ozs7MEJBRU8sSSxFQUFLOztBQUVaLFFBQUEsS0FBQSxDQUFBLFFBQUEsQ0FBQSxVQUFBO0FBQ0EsUUFBQSxTQUFBLENBQUEsT0FBQSxDQUFBLHNCQUFBLEVBQUEsU0FBQTtBQUNBLFFBQUEsU0FBQSxDQUFBLE9BQUEsQ0FBQSxtQ0FBQSxFQUFBLElBQUE7O0FBRUEsVUFBQSxJQUFBO0FBQ0E7OztzQkFFaUI7QUFDakIsVUFBQSwrQkFBQTtBQUNBOzs7O0VBakIyQixVQUFBLE87Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ0g3QixJQUFBLFlBQUEsUUFBQSxZQUFBLENBQUE7Ozs7QUFDQSxJQUFBLDJCQUFBLFFBQUEsMkJBQUEsQ0FBQTs7OztBQUNBLElBQUEsNkJBQUEsUUFBQSw2QkFBQSxDQUFBOzs7O0FBQ0EsSUFBQSx1QkFBQSxRQUFBLHVCQUFBLENBQUE7Ozs7QUFDQSxJQUFBLDZCQUFBLFFBQUEsNkJBQUEsQ0FBQTs7OztBQUNBLElBQUEsNEJBQUEsUUFBQSw0QkFBQSxDQUFBOzs7O0FBQ0EsSUFBQSxnQkFBQSxRQUFBLG1DQUFBLENBQUE7Ozs7QUFDQSxJQUFBLDJCQUFBLFFBQUEsMkJBQUEsQ0FBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFNQyxVQUFBLE1BQUEsR0FBYTtBQUFBLGtCQUFBLElBQUEsRUFBQSxNQUFBOztBQUFBLFNBQUEsMkJBQUEsSUFBQSxFQUFBLENBQUEsT0FBQSxTQUFBLElBQUEsT0FBQSxjQUFBLENBQUEsTUFBQSxDQUFBLEVBQUEsSUFBQSxDQUFBLElBQUEsQ0FBQSxDQUFBO0FBR1o7Ozs7MEJBRU8sSSxFQUFNLGEsRUFBYztBQUMzQixRQUFBLFNBQUEsQ0FBQSxlQUFBLENBQStCLDBCQUFBLE9BQUEsQ0FBL0IsSUFBQSxFQUE2RCxJQUFJLDBCQUFKLE9BQUEsQ0FBN0QsYUFBNkQsQ0FBN0QsRUFBQSxJQUFBO0FBQ0EsUUFBQSxTQUFBLENBQUEsZUFBQSxDQUErQixzQkFBQSxPQUFBLENBQS9CLElBQUEsRUFBeUQsSUFBSSxzQkFBSixPQUFBLENBQXpELGFBQXlELENBQXpELEVBQUEsSUFBQTtBQUNBLFFBQUEsU0FBQSxDQUFBLGVBQUEsQ0FBK0IsNEJBQUEsT0FBQSxDQUEvQixJQUFBLEVBQStELElBQUksNEJBQUosT0FBQSxDQUEvRCxhQUErRCxDQUEvRCxFQUFBLElBQUE7QUFDQSxRQUFBLFNBQUEsQ0FBQSxlQUFBLENBQStCLDJCQUFBLE9BQUEsQ0FBL0IsSUFBQSxFQUE4RCxJQUFJLDJCQUFKLE9BQUEsQ0FBOUQsYUFBOEQsQ0FBOUQsRUFBQSxJQUFBO0FBQ0EsUUFBQSxTQUFBLENBQUEsZUFBQSxDQUErQiwwQkFBQSxPQUFBLENBQS9CLElBQUEsRUFBNkQsSUFBSSwwQkFBSixPQUFBLENBQTdELGFBQTZELENBQTdELEVBQUEsSUFBQTtBQUNBLFFBQUEsU0FBQSxDQUFBLGVBQUEsQ0FBK0IsNEJBQUEsT0FBQSxDQUEvQixJQUFBLEVBQStELElBQUksNEJBQUosT0FBQSxDQUEvRCxhQUErRCxDQUEvRCxFQUFBLElBQUE7O0FBRUEsUUFBQSxLQUFBLENBQUEsUUFBQSxHQUFzQixJQUFJLGVBQTFCLE9BQXNCLEVBQXRCO0FBQ0EsUUFBQSxTQUFBLENBQUEsT0FBQSxDQUFBLGtDQUFBLEVBQUEsSUFBQTtBQUNBLFFBQUEsU0FBQSxDQUFBLE9BQUEsQ0FBQSxxQ0FBQSxFQUFBLElBQUE7O0FBRUEsVUFBQSxJQUFBO0FBQ0E7OztzQkFFaUI7QUFDakIsVUFBQSxvQ0FBQTtBQUNBOzs7O0VBeEIyQixVQUFBLE87Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNUNUIsVUFBQSxNQUFBLEdBQWE7QUFBQSxrQkFBQSxJQUFBLEVBQUEsTUFBQTs7QUFDWixPQUFBLFNBQUEsR0FBQSxDQUFBO0FBQ0E7Ozs7K0JBRVksUSxFQUFTO0FBQ3JCLE9BQUcsV0FBVyxLQUFkLFNBQUEsRUFBNkI7QUFDNUIsU0FBQSxTQUFBLEdBQUEsUUFBQTtBQUNBO0FBQ0Q7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNWRixJQUFBLGdCQUFBLFFBQUEsNkJBQUEsQ0FBQTs7Ozs7Ozs7Ozs7Ozs7O0FBR0MsVUFBQSxNQUFBLEdBQWE7QUFBQSxrQkFBQSxJQUFBLEVBQUEsTUFBQTs7QUFDWixPQUFBLEtBQUEsR0FBQSxDQUFBO0FBQ0EsT0FBQSxLQUFBLEdBQUEsQ0FBQTtBQUNBLE9BQUEsbUJBQUEsR0FBMkIsZUFBQSxPQUFBLENBQTNCLG9CQUFBO0FBQ0E7Ozs7a0NBRWM7QUFDZCxRQUFBLEtBQUEsSUFBYyxlQUFBLE9BQUEsQ0FBQSxnQkFBQSxHQUFpQyxlQUFBLE9BQUEsQ0FBQSxpQkFBQSxHQUFpQyxLQUFoRixLQUFBO0FBQ0E7OztrQ0FFYztBQUNkLFFBQUEsS0FBQTtBQUNBOzs7K0JBRVc7QUFDWCxRQUFBLEtBQUEsR0FBQSxDQUFBO0FBQ0E7OztrQ0FFYztBQUNkLFVBQVEsZUFBQSxPQUFBLENBQUEsZ0JBQUEsR0FBaUMsZUFBQSxPQUFBLENBQUEsaUJBQUEsR0FBaUMsS0FBMUUsS0FBQTtBQUNBOzs7NkJBRVM7QUFDVCxRQUFBLG1CQUFBO0FBQ0E7OztvQ0FFZ0I7QUFDaEIsVUFBTyxLQUFBLG1CQUFBLElBQVAsQ0FBQTtBQUNBOzs7K0JBRVc7QUFDWCxRQUFBLG1CQUFBLEdBQTJCLGVBQUEsT0FBQSxDQUEzQixvQkFBQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDbkNGLElBQUEsZ0JBQUEsUUFBQSw2QkFBQSxDQUFBOzs7Ozs7Ozs7Ozs7Ozs7QUFHQyxVQUFBLE1BQUEsR0FBYTtBQUFBLGtCQUFBLElBQUEsRUFBQSxNQUFBOztBQUNaLE9BQUEsSUFBQSxHQUFBLEVBQUE7QUFDQTs7OzsrQkFFWSxDLEVBQUU7QUFDZDtBQUNBO0FBQ0EsUUFBSyxJQUFJLElBQUksZUFBQSxPQUFBLENBQUEsZ0JBQUEsR0FBYixDQUFBLEVBQWdELElBQWhELENBQUEsRUFBQSxHQUFBLEVBQTREO0FBQzNELFFBQUksY0FBYyxLQUFBLGlCQUFBLENBQUEsQ0FBQSxFQUFsQixDQUFrQixDQUFsQjtBQUNBLFFBQUksWUFBWSxLQUFBLGlCQUFBLENBQUEsQ0FBQSxFQUEwQixJQUExQyxDQUFnQixDQUFoQjs7QUFFQSxRQUFHLGVBQUEsSUFBQSxJQUF1QixhQUExQixJQUFBLEVBQTRDO0FBQUU7QUFDN0MsU0FBSSxPQUFKLENBQUE7QUFDQSxZQUFNLE9BQU8sZUFBQSxPQUFBLENBQVAsV0FBQSxJQUFtQyxhQUF6QyxJQUFBLEVBQTJEO0FBQUU7QUFDNUQsY0FBQSxDQUFBO0FBQ0EsVUFBSSxZQUFZLEtBQUEsaUJBQUEsQ0FBQSxDQUFBLEVBQTBCLE9BQTFDLENBQWdCLENBQWhCO0FBRUE7O0FBRUQsVUFBQSxrQkFBQSxDQUFBLENBQUEsRUFBQSxDQUFBLEVBQUEsQ0FBQSxFQUFBLElBQUE7QUFDQTtBQUNEO0FBQ0Q7O0FBRUQ7Ozs7MkNBQ3dCO0FBQ3ZCLE9BQUksc0JBQUosRUFBQTtBQUNBLE9BQUksU0FBSixFQUFBOztBQUVBLFFBQUssSUFBSSxJQUFULENBQUEsRUFBZ0IsSUFBSSxlQUFBLE9BQUEsQ0FBcEIsVUFBQSxFQUFBLEdBQUEsRUFBa0Q7QUFDakQsUUFBSSxlQUFKLEVBQUE7QUFDQSxRQUFJLElBQUosQ0FBQTs7QUFFQSxXQUFNLElBQUksZUFBQSxPQUFBLENBQVYsZ0JBQUEsRUFBd0M7QUFDdkMsU0FBSSxrQkFBa0IsS0FBQSxpQkFBQSxDQUFBLENBQUEsRUFBdEIsQ0FBc0IsQ0FBdEI7QUFDQSxTQUFHLG1CQUFILElBQUEsRUFBMkI7QUFDMUIsbUJBQUEsSUFBQSxDQUFBLGVBQUE7QUFDQTs7QUFFRDtBQUNBOztBQUVELFFBQUcsYUFBQSxNQUFBLEdBQUgsQ0FBQSxFQUEyQjtBQUMxQixZQUFBLElBQUEsQ0FBQSxZQUFBO0FBQ0E7QUFDRDs7QUFFRCxVQUFPLEtBQUEsb0JBQUEsQ0FBUCxNQUFPLENBQVA7QUFDQTs7QUFFRDs7Ozt3Q0FDcUI7QUFDcEIsT0FBSSxTQUFKLEVBQUE7QUFDQTtBQUNBLFFBQUssSUFBSSxJQUFULENBQUEsRUFBZ0IsSUFBSSxlQUFBLE9BQUEsQ0FBcEIsZ0JBQUEsRUFBQSxHQUFBLEVBQXdEOztBQUV2RCxRQUFJLGVBQUosRUFBQTtBQUNBLFFBQUksSUFBSixDQUFBOztBQUVBLFdBQU0sSUFBSSxlQUFBLE9BQUEsQ0FBVixVQUFBLEVBQWtDO0FBQ2pDLFNBQUksa0JBQWtCLEtBQUEsaUJBQUEsQ0FBQSxDQUFBLEVBQXRCLENBQXNCLENBQXRCOztBQUVBLFNBQUcsbUJBQUgsSUFBQSxFQUEyQjtBQUFFO0FBQzVCLFVBQUcsYUFBQSxNQUFBLEdBQUgsQ0FBQSxFQUEyQjtBQUFDO0FBQzNCLGNBQUEsSUFBQSxDQUFBLFlBQUE7QUFDQSxzQkFGMEIsRUFFMUIsQ0FGMEIsQ0FFUDtBQUNuQjtBQUpGLE1BQUEsTUFLSztBQUNKLG1CQUFBLElBQUEsQ0FBQSxlQUFBO0FBQ0E7QUFDRDtBQUNBOztBQUVELFFBQUcsYUFBQSxNQUFBLEdBQUgsQ0FBQSxFQUEyQjtBQUMxQixZQUFBLElBQUEsQ0FBQSxZQUFBO0FBQ0E7QUFDRDs7QUFFRCxVQUFPLEtBQUEsb0JBQUEsQ0FBUCxNQUFPLENBQVA7QUFDQTs7O3VDQUVvQixNLEVBQU87QUFDM0IsT0FBSSxtQkFBSixFQUFBO0FBQ0EsUUFBSyxJQUFJLElBQVQsQ0FBQSxFQUFnQixJQUFJLE9BQXBCLE1BQUEsRUFBQSxHQUFBLEVBQXdDO0FBQ3ZDLFFBQUksaUJBQWlCLE9BQXJCLENBQXFCLENBQXJCO0FBQ0EsUUFBSSxhQUFhLGVBQWpCLE1BQUE7O0FBRUEsU0FBSyxJQUFJLElBQVQsQ0FBQSxFQUFnQixJQUFJLGVBQXBCLE1BQUEsRUFBQSxHQUFBLEVBQWdEO0FBQy9DLFNBQUksT0FBTyxlQUFYLENBQVcsQ0FBWDtBQUNBLFNBQUcsY0FBYyxLQUFkLFNBQUEsSUFBZ0MsS0FBQSxNQUFBLElBQW5DLENBQUEsRUFBb0Q7QUFDbkQsdUJBQUEsSUFBQSxDQUFBLElBQUE7QUFDQTtBQUNEO0FBQ0Q7O0FBRUQsVUFBQSxnQkFBQTtBQUNBOzs7K0JBRVksWSxFQUFjLEMsRUFBRTtBQUM1QixPQUFJLFVBQVUsS0FBQSxZQUFBLENBQWQsQ0FBYyxDQUFkO0FBQ0EsT0FBRyxXQUFILENBQUEsRUFBZ0I7QUFDZjtBQUNBLFdBQUEsS0FBQTtBQUZELElBQUEsTUFHSztBQUNKLFNBQUEsaUJBQUEsQ0FBQSxZQUFBLEVBQUEsQ0FBQSxFQUFBLE9BQUE7QUFDQSxXQUFBLElBQUE7QUFDQTtBQUNEOzs7K0JBRVksQyxFQUFFO0FBQ2QsT0FBSSxlQUFKLENBQUE7QUFDQSxRQUFLLElBQUksSUFBSSxlQUFBLE9BQUEsQ0FBQSxnQkFBQSxHQUFiLENBQUEsRUFBZ0QsS0FBaEQsQ0FBQSxFQUFBLEdBQUEsRUFBNkQ7QUFDNUQsUUFBRyxLQUFBLGlCQUFBLENBQUEsQ0FBQSxFQUFBLENBQUEsS0FBSCxJQUFBLEVBQXdDOztBQUV2QyxvQkFBQSxDQUFBO0FBQ0E7QUFDQTtBQUNEO0FBQ0QsVUFBQSxZQUFBO0FBQ0E7OztvQ0FFaUIsWSxFQUFjLEMsRUFBRyxDLEVBQUU7QUFDcEMsZ0JBQUEsQ0FBQSxHQUFBLENBQUE7QUFDQSxnQkFBQSxDQUFBLEdBQUEsQ0FBQTtBQUNBLFFBQUEsSUFBQSxDQUFVLElBQUksSUFBRSxlQUFBLE9BQUEsQ0FBaEIsVUFBQSxJQUFBLFlBQUE7QUFDQTs7O3FDQUVrQixDLEVBQUcsQyxFQUFHLEksRUFBTSxJLEVBQUs7QUFDbkMsT0FBSSxlQUFlLEtBQUEsaUJBQUEsQ0FBQSxDQUFBLEVBQW5CLENBQW1CLENBQW5CO0FBQ0EsUUFBQSxvQkFBQSxDQUFBLENBQUEsRUFBQSxDQUFBO0FBQ0EsUUFBQSxpQkFBQSxDQUFBLFlBQUEsRUFBQSxJQUFBLEVBQUEsSUFBQTtBQUNBOzs7dUNBRW9CLEMsRUFBRyxDLEVBQUU7QUFDekIsUUFBQSxJQUFBLENBQVUsSUFBSSxJQUFFLGVBQUEsT0FBQSxDQUFoQixVQUFBLElBQUEsSUFBQTtBQUNBOzs7b0NBRWlCLEMsRUFBRyxDLEVBQUU7QUFDdEIsT0FBRyxDQUFDLEtBQUEsUUFBQSxDQUFBLENBQUEsRUFBSixDQUFJLENBQUosRUFBd0I7QUFDdkIsV0FBQSxJQUFBO0FBQ0E7QUFDRCxVQUFPLEtBQUEsSUFBQSxDQUFVLElBQUksSUFBRSxlQUFBLE9BQUEsQ0FBdkIsVUFBTyxDQUFQO0FBQ0E7O0FBRUQ7Ozs7MkJBQ1MsQyxFQUFHLEMsRUFBRTtBQUNiLE9BQUcsSUFBQSxDQUFBLElBQVMsS0FBSyxlQUFBLE9BQUEsQ0FBZCxVQUFBLElBQ0YsSUFERSxDQUFBLElBQ08sS0FBSyxlQUFBLE9BQUEsQ0FEZixnQkFBQSxFQUM2QztBQUM1QyxXQUFBLEtBQUE7QUFGRCxJQUFBLE1BR0s7QUFDSixXQUFBLElBQUE7QUFDQTtBQUNEOztBQUVEOzs7OytCQUNZO0FBQ1gsUUFBSyxJQUFJLElBQVQsQ0FBQSxFQUFnQixJQUFJLEtBQUEsSUFBQSxDQUFwQixNQUFBLEVBQUEsR0FBQSxFQUEyQztBQUMxQyxRQUFHLEtBQUEsSUFBQSxDQUFBLENBQUEsS0FBSCxJQUFBLEVBQXdCO0FBQ3ZCLFVBQUEsa0JBQUEsQ0FBd0IsS0FBQSxJQUFBLENBQUEsQ0FBQSxFQUF4QixDQUFBLEVBQXdDLEtBQUEsSUFBQSxDQUFBLENBQUEsRUFBeEMsQ0FBQSxFQUF3RCxLQUFBLElBQUEsQ0FBQSxDQUFBLEVBQXhELENBQUEsRUFBd0UsS0FBQSxJQUFBLENBQUEsQ0FBQSxFQUFBLENBQUEsR0FBeEUsQ0FBQTtBQUNBO0FBQ0Q7QUFDRDs7QUFFRDs7Ozt1Q0FDb0I7QUFDbkIsUUFBSyxJQUFJLElBQVQsQ0FBQSxFQUFnQixJQUFJLGVBQUEsT0FBQSxDQUFwQixnQkFBQSxFQUFBLEdBQUEsRUFBd0Q7QUFDdkQsUUFBSSxZQUFKLEVBQUE7O0FBRUEsU0FBSyxJQUFJLElBQVQsQ0FBQSxFQUFnQixJQUFJLGVBQUEsT0FBQSxDQUFwQixVQUFBLEVBQUEsR0FBQSxFQUFrRDtBQUNqRCxTQUFHLEtBQUgsQ0FBQSxFQUFVO0FBQ1QsbUJBQUEsSUFBQTtBQURELE1BQUEsTUFFSztBQUNKLFVBQUksV0FBVyxLQUFBLGlCQUFBLENBQUEsQ0FBQSxFQUFmLENBQWUsQ0FBZjtBQUNBLFVBQUcsWUFBSCxJQUFBLEVBQW9CO0FBQ25CLG9CQUFhLFNBQUEsU0FBQSxHQUFiLEdBQUE7QUFERCxPQUFBLE1BRUs7QUFDSixvQkFBQSxJQUFBO0FBQ0E7QUFDRDtBQUNEOztBQUVELFlBQUEsR0FBQSxDQUFBLFNBQUE7QUFDQTtBQUNEOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3pMRCxVQUFBLE1BQUEsQ0FBQSxLQUFBLEVBQThCO0FBQUEsTUFBWCxTQUFXLFVBQUEsTUFBQSxHQUFBLENBQUEsSUFBQSxVQUFBLENBQUEsTUFBQSxTQUFBLEdBQUEsVUFBQSxDQUFBLENBQUEsR0FBRixDQUFFOztBQUFBLGtCQUFBLElBQUEsRUFBQSxNQUFBOztBQUM3QixPQUFBLFNBQUEsR0FBQSxLQUFBO0FBQ0EsT0FBQSxPQUFBLEdBQUEsTUFBQTtBQUNBLE9BQUEsRUFBQSxHQUFBLENBQUE7QUFDQSxPQUFBLEVBQUEsR0FBQSxDQUFBO0FBQ0EsT0FBQSxVQUFBLEdBQUEsS0FBQTs7QUFFQSxPQUFBLFNBQUEsR0FBaUIsSUFBSSxPQVBRLE1BT1osRUFBakIsQ0FQNkIsQ0FPUztBQUN0QyxPQUFBLGVBQUEsR0FBdUIsSUFBSSxPQVJFLE1BUU4sRUFBdkIsQ0FSNkIsQ0FRZTtBQUM1Qzs7OztzQkFFTztBQUNQLFVBQU8sS0FBUCxFQUFBOztvQkFlSyxLLEVBQU87QUFDWixRQUFBLEVBQUEsR0FBQSxLQUFBO0FBQ0E7OztzQkFkTztBQUNQLFVBQU8sS0FBUCxFQUFBOztvQkFlSyxLLEVBQU87QUFDWixRQUFBLEVBQUEsR0FBQSxLQUFBO0FBQ0EsUUFBQSxTQUFBLENBQUEsUUFBQTtBQUNBOzs7c0JBZmU7QUFDZixVQUFPLEtBQVAsVUFBQTs7b0JBZ0JhLEssRUFBTztBQUNwQixRQUFBLFVBQUEsR0FBQSxLQUFBOztBQUVBLE9BQUcsS0FBSCxTQUFBLEVBQWtCO0FBQ2pCLFNBQUEsU0FBQTtBQUNBO0FBQ0Q7OztzQkFuQlk7QUFDWixVQUFPLEtBQVAsT0FBQTs7b0JBb0JVLEssRUFBTztBQUNqQixRQUFBLE9BQUEsR0FBQSxLQUFBOztBQUVBLE9BQUcsS0FBQSxPQUFBLEdBQUgsQ0FBQSxFQUFvQjtBQUNuQixTQUFBLE9BQUEsR0FBQSxDQUFBO0FBREQsSUFBQSxNQUVLO0FBQ0osU0FBQSxlQUFBLENBQUEsUUFBQTtBQUNBO0FBQ0Q7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O3NCQ2xEdUI7QUFDdkIsVUFBQSxDQUFBO0FBQ0E7OztzQkFFd0I7QUFDeEIsVUFBQSxDQUFBO0FBQ0E7O0FBRUQ7QUFDQTs7OztzQkFDOEI7QUFDN0IsVUFBQSxDQUFBO0FBQ0E7OztzQkFNMkI7QUFDM0IsVUFBQSxFQUFBO0FBQ0E7OztzQkFFMkI7QUFDM0IsVUFBQSxFQUFBO0FBQ0E7OztzQkFFeUM7QUFDekMsVUFBQSxDQUFBO0FBQ0E7OztzQkFHMkI7QUFDM0IsVUFBQSxDQUFBO0FBQ0E7OztzQkFFMkI7QUFDM0IsVUFBQSxDQUFBO0FBQ0E7OztzQkFFNkI7QUFDN0IsVUFBQSxDQUFBO0FBQ0E7OztzQkFFOEI7QUFDOUIsVUFBQSxFQUFBO0FBQ0E7OztzQkFFa0M7QUFDbEMsVUFBQSxHQUFBO0FBQ0E7OztzQkFFaUM7QUFDakMsVUFBQSxDQUFBO0FBQ0E7OztzQkFFaUM7QUFDakMsVUFBQSxFQUFBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs0QkN4RFE7QUFDUjtBQUNBLFFBQUEsSUFBQSxDQUFBLEtBQUEsR0FBQSxFQUFBOztBQUVBLFVBQUEsYUFBQSxHQUF1QjtBQUN0QjtBQUNBLFlBQVE7QUFDTixlQUFVLENBQUEsa0JBQUE7QUFESjs7QUFGYyxJQUF2Qjs7QUFRQSxRQUFBLElBQUEsQ0FBQSxJQUFBLENBQUEsS0FBQSxDQUFBLFlBQUEsRUFBbUMsQ0FBQSw2QkFBQSxFQUFuQyw2QkFBbUMsQ0FBbkM7QUFDQSxRQUFBLElBQUEsQ0FBQSxJQUFBLENBQUEsS0FBQSxDQUFBLFdBQUEsRUFBa0MsQ0FBQSw0QkFBQSxFQUFsQyw0QkFBa0MsQ0FBbEM7QUFDQSxRQUFBLElBQUEsQ0FBQSxJQUFBLENBQUEsS0FBQSxDQUFBLFdBQUEsRUFBa0MsQ0FBQSw0QkFBQSxFQUFsQyw0QkFBa0MsQ0FBbEM7QUFDQSxRQUFBLElBQUEsQ0FBQSxJQUFBLENBQUEsS0FBQSxDQUFBLFVBQUEsRUFBaUMsQ0FBQSwyQkFBQSxFQUFqQywyQkFBaUMsQ0FBakM7QUFDQSxRQUFBLElBQUEsQ0FBQSxJQUFBLENBQUEsS0FBQSxDQUFBLFNBQUEsRUFBZ0MsQ0FBQSw2QkFBQSxFQUFoQywwQkFBZ0MsQ0FBaEM7QUFDQSxRQUFBLElBQUEsQ0FBQSxJQUFBLENBQUEsS0FBQSxDQUFBLFNBQUEsRUFBZ0MsQ0FBQSwwQkFBQSxFQUFoQywwQkFBZ0MsQ0FBaEM7QUFDQSxRQUFBLElBQUEsQ0FBQSxJQUFBLENBQUEsS0FBQSxDQUFBLFFBQUEsRUFBK0IsQ0FBQSw2QkFBQSxFQUEvQix5QkFBK0IsQ0FBL0I7O0FBRUEsUUFBQSxJQUFBLENBQUEsSUFBQSxDQUFBLE1BQUEsQ0FBQSxTQUFBLEVBQUEsMERBQUE7QUFHQTs7OzJCQUVPOztBQUVQLFFBQUEsSUFBQSxDQUFBLFNBQUEsQ0FBQSxPQUFBLENBQUEsc0JBQUEsRUFBb0QsS0FBcEQsSUFBQTtBQUNBLFFBQUEsSUFBQSxDQUFBLEtBQUEsQ0FBQSxlQUFBLEdBQUEsU0FBQTtBQUNBLFFBQUEsSUFBQSxDQUFBLEtBQUEsQ0FBQSxLQUFBLENBQUEsUUFBQTtBQUNBOzs7O0VBbEMyQixPQUFPLEs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OzRCQ0UxQjtBQUNSLFFBQUEsSUFBQSxDQUFBLElBQUEsQ0FBQSxLQUFBLENBQUEsa0JBQUEsRUFBQSx1Q0FBQTtBQUNBLFFBQUEsSUFBQSxDQUFBLElBQUEsQ0FBQSxXQUFBLENBQUEsa0JBQUEsRUFBQSw2Q0FBQSxFQUFBLEdBQUEsRUFBQSxFQUFBO0FBQ0E7OzsyQkFFTztBQUNQLFFBQUEsSUFBQSxDQUFBLEtBQUEsQ0FBQSxlQUFBLEdBQUEsU0FBQTtBQUNBLE9BQUksWUFBWSxLQUFBLElBQUEsQ0FBQSxHQUFBLENBQUEsTUFBQSxDQUFxQixLQUFBLElBQUEsQ0FBQSxLQUFBLENBQXJCLE9BQUEsRUFBQSxHQUFBLEVBQWhCLGtCQUFnQixDQUFoQjtBQUNBLGFBQUEsTUFBQSxDQUFBLEtBQUEsQ0FBQSxHQUFBLEVBQUEsQ0FBQTs7QUFFQSxPQUFJLFNBQVMsRUFBRSxNQUFGLHVCQUFBLEVBQWlDLE1BQWpDLFNBQUEsRUFBa0QsT0FBL0QsUUFBYSxFQUFiO0FBQ0EsUUFBQSxrQkFBQSxHQUEwQixLQUFBLElBQUEsQ0FBQSxHQUFBLENBQUEsSUFBQSxDQUFBLENBQUEsRUFBQSxDQUFBLEVBQXdCLGlCQUFpQixLQUFBLElBQUEsQ0FBQSxLQUFBLENBQUEsTUFBQSxDQUF6QyxTQUFBLEVBQTFCLE1BQTBCLENBQTFCO0FBQ0EsUUFBQSxrQkFBQSxDQUFBLE1BQUEsQ0FBQSxLQUFBLENBQUEsR0FBQSxFQUFBLENBQUE7QUFDQSxRQUFBLGtCQUFBLENBQUEsQ0FBQSxHQUE0QixLQUFBLElBQUEsQ0FBQSxLQUFBLENBQTVCLE9BQUE7QUFDQSxRQUFBLGtCQUFBLENBQUEsQ0FBQSxHQUFBLEdBQUE7O0FBRUEsUUFBQSxNQUFBLEdBQWMsS0FBQSxJQUFBLENBQUEsR0FBQSxDQUFBLE1BQUEsQ0FBcUIsS0FBQSxJQUFBLENBQUEsS0FBQSxDQUFyQixPQUFBLEVBQUEsR0FBQSxFQUFBLGtCQUFBLEVBQXVFLEtBQXZFLFdBQUEsRUFBQSxJQUFBLEVBQUEsQ0FBQSxFQUFBLENBQUEsRUFBZCxDQUFjLENBQWQ7QUFDQSxRQUFBLE1BQUEsQ0FBQSxNQUFBLENBQUEsS0FBQSxDQUFBLEdBQUEsRUFBQSxHQUFBOztBQUVBLE9BQUksU0FBUyxFQUFFLE1BQUYsdUJBQUEsRUFBaUMsTUFBakMsU0FBQSxFQUFrRCxPQUEvRCxRQUFhLEVBQWI7QUFDQSxPQUFJLFFBQVEsS0FBQSxJQUFBLENBQUEsR0FBQSxDQUFBLElBQUEsQ0FBQSxDQUFBLEVBQUEsQ0FBQSxFQUFBLE1BQUEsRUFBWixNQUFZLENBQVo7QUFDQSxRQUFBLE1BQUEsQ0FBQSxRQUFBLENBQUEsS0FBQTtBQUNBLFNBQUEsTUFBQSxDQUFBLEdBQUEsQ0FBQSxHQUFBOztBQUVBLFFBQUEsSUFBQSxDQUFBLFNBQUEsQ0FBQSxPQUFBLENBQUEsc0JBQUEsRUFBQSxTQUFBO0FBQ0E7OztnQ0FFWTtBQUNaOztBQUVBLFFBQUEsSUFBQSxDQUFBLEtBQUEsQ0FBQSxLQUFBLENBQUEsVUFBQTtBQUNBOzs7O0VBakMyQixPQUFPLEs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OzRCQ0UxQjtBQUNSLFFBQUEsSUFBQSxDQUFBLElBQUEsQ0FBQSxLQUFBLENBQUEsY0FBQSxFQUFBLHVDQUFBO0FBQ0E7OzsyQkFFTztBQUNQLE9BQUksY0FBYyxLQUFBLElBQUEsQ0FBQSxHQUFBLENBQUEsTUFBQSxDQUFxQixLQUFBLElBQUEsQ0FBQSxLQUFBLENBQXJCLE9BQUEsRUFBQSxFQUFBLEVBQWxCLGNBQWtCLENBQWxCO0FBQ0EsZUFBQSxNQUFBLENBQUEsS0FBQSxDQUFBLEdBQUEsRUFBQSxDQUFBO0FBQ0EsZUFBQSxLQUFBLEdBQUEsQ0FBQTs7QUFFQSxPQUFJLGlCQUFpQixLQUFBLElBQUEsQ0FBQSxHQUFBLENBQUEsS0FBQSxDQUFBLFdBQUEsRUFBQSxFQUFBLENBQXFDLEVBQUUsT0FBdkMsQ0FBcUMsRUFBckMsRUFBQSxJQUFBLEVBQXJCLFFBQXFCLENBQXJCO0FBQ0EsT0FBSSxrQkFBa0IsS0FBQSxJQUFBLENBQUEsR0FBQSxDQUFBLEtBQUEsQ0FBQSxXQUFBLEVBQUEsRUFBQSxDQUFxQyxFQUFFLE9BQXZDLENBQXFDLEVBQXJDLEVBQUEsSUFBQSxFQUFBLFFBQUEsRUFBQSxLQUFBLEVBQXRCLElBQXNCLENBQXRCOztBQUVBLGtCQUFBLEtBQUEsQ0FBQSxlQUFBO0FBQ0EsbUJBQUEsVUFBQSxDQUFBLEdBQUEsQ0FBK0IsS0FBL0IsbUJBQUEsRUFBQSxJQUFBO0FBQ0Esa0JBQUEsS0FBQTtBQUNBOzs7MkJBRU87QUFDUCxPQUFJLEtBQUEsSUFBQSxDQUFBLEtBQUEsQ0FBQSxhQUFBLENBQUosTUFBQSxFQUF5QztBQUN4QyxTQUFBLGtCQUFBO0FBQ0E7QUFDRDs7O3dDQUVvQjtBQUNwQixRQUFBLGtCQUFBO0FBQ0E7Ozt1Q0FFbUI7QUFDbkIsUUFBQSxJQUFBLENBQUEsS0FBQSxDQUFBLEtBQUEsQ0FBQSxVQUFBO0FBQ0E7Ozs7RUEvQjJCLE9BQU8sSzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDQXBDLElBQUEsY0FBQSxRQUFBLGNBQUEsQ0FBQTs7OztBQUNBLElBQUEsa0JBQUEsUUFBQSxrQkFBQSxDQUFBOzs7O0FBQ0EsSUFBQSxnQkFBQSxRQUFBLGdCQUFBLENBQUE7Ozs7QUFFQSxJQUFBLGdCQUFBLFFBQUEsNkJBQUEsQ0FBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs0QkFLVTtBQUNSLFFBQUEsSUFBQSxDQUFBLElBQUEsQ0FBQSxhQUFBLENBQUEsYUFBQSxFQUFBLHlDQUFBLEVBQUEsMENBQUE7QUFDQTs7OzJCQUVPO0FBQ1AsUUFBQSxXQUFBLEdBQUEsRUFBQTtBQUNBLFFBQUEsY0FBQSxHQUFBLEVBQUE7QUFDQSxRQUFBLFdBQUEsR0FBQSxLQUFBO0FBQ0EsUUFBQSxrQkFBQSxHQUFBLEtBQUE7QUFDQSxRQUFBLFNBQUEsR0FBaUIsS0FBQSxJQUFBLENBQUEsR0FBQSxDQUFqQixLQUFpQixFQUFqQjtBQUNBLFFBQUEsU0FBQSxDQUFBLENBQUEsR0FBQSxHQUFBO0FBQ0EsUUFBQSxTQUFBLENBQUEsQ0FBQSxHQUFBLEdBQUE7O0FBSUEsUUFBQSxVQUFBLEdBQWtCLEtBQUEsSUFBQSxDQUFBLEdBQUEsQ0FBbEIsS0FBa0IsRUFBbEI7O0FBRUEsUUFBQSxrQkFBQSxHQUFBLElBQUE7QUFDQSxRQUFBLGNBQUEsR0FBQSxJQUFBOztBQUVBLFFBQUEsVUFBQSxHQUFrQixJQUFJLGFBQUosT0FBQSxDQUFlLEtBQWpDLElBQWtCLENBQWxCO0FBQ0EsUUFBQSxVQUFBLENBQUEsTUFBQSxDQUFBLFNBQUEsQ0FBQSxHQUFBLENBQXFDLEtBQXJDLGlCQUFBLEVBQUEsSUFBQTtBQUNBLFFBQUEsU0FBQSxDQUFBLEdBQUEsQ0FBbUIsS0FBbkIsVUFBQTtBQUNBLFFBQUEsU0FBQSxDQUFBLEdBQUEsQ0FBbUIsS0FBbkIsVUFBQTs7QUFFQSxRQUFBLE9BQUEsR0FBZSxLQUFBLElBQUEsQ0FBQSxHQUFBLENBQWYsS0FBZSxFQUFmO0FBQ0EsUUFBQSxPQUFBLENBQUEsQ0FBQSxHQUFpQixLQUFBLFNBQUEsQ0FBakIsQ0FBQTtBQUNBLFFBQUEsT0FBQSxDQUFBLENBQUEsR0FBaUIsS0FBQSxTQUFBLENBQWpCLENBQUE7O0FBRUEsT0FBSSxTQUFTLEVBQUUsTUFBRix1QkFBQSxFQUFpQyxNQUFqQyxTQUFBLEVBQWtELE9BQS9ELE1BQWEsRUFBYjtBQUNBLFFBQUEsY0FBQSxHQUFzQixLQUFBLElBQUEsQ0FBQSxHQUFBLENBQUEsSUFBQSxDQUFBLENBQUEsRUFBQSxDQUFBLEVBQUEsT0FBQSxFQUF0QixNQUFzQixDQUF0QjtBQUNBLFFBQUEsY0FBQSxDQUFBLENBQUEsR0FBQSxFQUFBO0FBQ0EsUUFBQSxjQUFBLENBQUEsQ0FBQSxHQUFBLEVBQUE7O0FBRUEsT0FBSSxTQUFTLEVBQUUsTUFBRix1QkFBQSxFQUFpQyxNQUFqQyxTQUFBLEVBQWtELE9BQS9ELE9BQWEsRUFBYjtBQUNBLFFBQUEsa0JBQUEsR0FBMEIsS0FBQSxJQUFBLENBQUEsR0FBQSxDQUFBLElBQUEsQ0FBQSxDQUFBLEVBQUEsQ0FBQSxFQUFBLFlBQUEsRUFBMUIsTUFBMEIsQ0FBMUI7QUFDQSxRQUFBLGtCQUFBLENBQUEsQ0FBQSxHQUFBLEdBQUE7QUFDQSxRQUFBLGtCQUFBLENBQUEsQ0FBQSxHQUFBLEVBQUE7QUFDQSxRQUFBLGtCQUFBLENBQUEsTUFBQSxDQUFBLEdBQUEsQ0FBQSxDQUFBLEVBQUEsQ0FBQTs7QUFFQSxRQUFBLElBQUEsQ0FBQSxTQUFBLENBQUEsT0FBQSxDQUFBLG9DQUFBLEVBQWtFLEtBQWxFLElBQUEsRUFBQSxJQUFBO0FBQ0EsUUFBQSxJQUFBLENBQUEsU0FBQSxDQUFBLE9BQUEsQ0FBQSxzQkFBQSxFQUFBLFVBQUE7QUFDQTs7OzJCQUVRO0FBQ1IsT0FBRyxLQUFBLGNBQUEsSUFBQSxJQUFBLElBQStCLENBQUMsS0FBbkMsa0JBQUEsRUFBMkQ7QUFDMUQsU0FBQSxjQUFBO0FBQ0E7O0FBRUQsUUFBQSxVQUFBLENBQUEsSUFBQSxDQUFBLEdBQUEsRUFBMEIsT0FBQSxLQUFBLENBQTFCLGVBQUE7QUFDQSxRQUFBLGNBQUEsQ0FBQSxJQUFBLEdBQTJCLFlBQVksS0FBQSxJQUFBLENBQUEsS0FBQSxDQUFBLFFBQUEsQ0FBdkMsS0FBQTtBQUNBLFFBQUEsa0JBQUEsQ0FBQSxJQUFBLEdBQStCLGlCQUFpQixLQUFBLElBQUEsQ0FBQSxLQUFBLENBQUEsUUFBQSxDQUFqQixtQkFBQSxHQUEvQixRQUFBO0FBQ0E7OztvQ0FFaUIsSyxFQUFPLE8sRUFBUTtBQUNoQyxPQUFHLENBQUMsS0FBRCxXQUFBLElBQXFCLENBQUMsS0FBekIsa0JBQUEsRUFBaUQ7QUFDaEQ7QUFDQSxRQUFJLFNBQVMsUUFBQSxDQUFBLEdBQVksS0FBQSxTQUFBLENBQXpCLENBQUE7QUFDQSxRQUFJLFVBQVUsS0FBQSxLQUFBLENBQVcsU0FBUyxlQUFBLE9BQUEsQ0FBbEMsY0FBYyxDQUFkOztBQUVBO0FBQ0EsUUFBRyxVQUFILENBQUEsRUFBZTtBQUNkLGVBQUEsQ0FBQTtBQUNBO0FBQ0QsUUFBRyxVQUFVLGVBQUEsT0FBQSxDQUFBLFVBQUEsR0FBYixDQUFBLEVBQXlDO0FBQ3hDLGVBQVUsZUFBQSxPQUFBLENBQUEsVUFBQSxHQUFWLENBQUE7QUFDQTs7QUFFRCxTQUFBLGtCQUFBLEdBQTBCLEtBQTFCLGNBQUE7QUFDQSxTQUFBLGNBQUEsR0FBQSxJQUFBOztBQUVBLFNBQUEsSUFBQSxDQUFBLFNBQUEsQ0FBQSxPQUFBLENBQUEsa0NBQUEsRUFBZ0UsS0FBaEUsSUFBQSxFQUEyRSxLQUFBLGtCQUFBLENBQTNFLElBQUEsRUFBQSxPQUFBLEVBQUEsSUFBQTtBQUNBO0FBQ0Q7OzttQ0FFZTtBQUNmO0FBQ0EsT0FBSSxTQUFTLEtBQUEsSUFBQSxDQUFBLEtBQUEsQ0FBQSxDQUFBLEdBQW9CLEtBQUEsU0FBQSxDQUFqQyxDQUFBO0FBQ0E7QUFDQSxPQUFJLFVBQVUsS0FBQSxLQUFBLENBQVcsU0FBUyxlQUFBLE9BQUEsQ0FBbEMsY0FBYyxDQUFkOztBQUVBO0FBQ0EsT0FBRyxVQUFILENBQUEsRUFBZTtBQUNkLGNBQUEsQ0FBQTtBQUNBO0FBQ0QsT0FBRyxVQUFVLGVBQUEsT0FBQSxDQUFBLFVBQUEsR0FBYixDQUFBLEVBQXlDO0FBQ3hDLGNBQVUsZUFBQSxPQUFBLENBQUEsVUFBQSxHQUFWLENBQUE7QUFDQTs7QUFFRCxPQUFJLGVBQWUsVUFBVyxlQUFBLE9BQUEsQ0FBWCxjQUFBLEdBQTBDLGVBQUEsT0FBQSxDQUFBLGNBQUEsR0FBN0QsR0FBQTs7QUFFQSxRQUFBLGNBQUEsQ0FBQSxDQUFBLEdBQUEsWUFBQTtBQUNBLFFBQUEsY0FBQSxDQUFBLENBQUEsR0FBeUIsZUFBQSxPQUFBLENBQUEsY0FBQSxHQUF6QixHQUFBO0FBQ0E7Ozt1Q0FFb0IsWSxFQUFhO0FBQ2pDLFFBQUEsY0FBQSxHQUFzQixLQUFBLHVCQUFBLENBQXRCLFlBQXNCLENBQXRCO0FBQ0E7OzswQ0FFdUIsWSxFQUFhO0FBQ3BDLE9BQUksb0JBQW9CLElBQUksaUJBQUosT0FBQSxDQUFtQixLQUFuQixJQUFBLEVBQXhCLFlBQXdCLENBQXhCO0FBQ0EsUUFBQSxVQUFBLENBQUEsR0FBQSxDQUFBLGlCQUFBO0FBQ0EsUUFBQSxXQUFBLENBQUEsSUFBQSxDQUFBLGlCQUFBO0FBQ0EsVUFBQSxpQkFBQTtBQUNBOzs7cUNBRWtCLEssRUFBTyxDLEVBQUcsQyxFQUFFO0FBQzlCLE9BQUksa0JBQWtCLElBQUksZUFBSixPQUFBLENBQWlCLEtBQWpCLElBQUEsRUFBQSxLQUFBLEVBQUEsQ0FBQSxFQUF0QixDQUFzQixDQUF0QjtBQUNBLFFBQUEsT0FBQSxDQUFBLEdBQUEsQ0FBQSxlQUFBO0FBQ0EsbUJBQUEsT0FBQTtBQUNBOzs7cUNBR2lCO0FBQ2pCLE9BQUksaUJBQUosRUFBQTtBQUNBLE9BQUksZ0JBQUosRUFBQTtBQUNBO0FBQ0EsUUFBSyxJQUFJLElBQVQsQ0FBQSxFQUFnQixJQUFJLEtBQUEsV0FBQSxDQUFwQixNQUFBLEVBQUEsR0FBQSxFQUFrRDtBQUNqRCxRQUFJLGFBQWEsS0FBQSxXQUFBLENBQWpCLENBQWlCLENBQWpCOztBQUVBLFFBQUcsV0FBSCxTQUFBLEVBQXdCO0FBQ3ZCLG9CQUFBLElBQUEsQ0FBQSxVQUFBO0FBREQsS0FBQSxNQUVNLElBQUcsV0FBSCxPQUFBLEVBQXNCO0FBQzNCLG1CQUFBLElBQUEsQ0FBQSxVQUFBO0FBQ0E7QUFDRDs7QUFFRDtBQUNBLFFBQUEsV0FBQSxHQUFtQixLQUFBLFdBQUEsQ0FBQSxNQUFBLENBQXlCLFVBQUEsRUFBQSxFQUFlO0FBQzFELFdBQU8sQ0FBQyxlQUFBLFFBQUEsQ0FBUixFQUFRLENBQVI7QUFERCxJQUFtQixDQUFuQjs7QUFJQSxPQUFHLGVBQUEsTUFBQSxHQUFILENBQUEsRUFBNkI7QUFDNUIsU0FBQSxjQUFBLENBQUEsSUFBQSxDQUFBLGNBQUE7QUFDQTs7QUFFRDtBQUNBLFFBQUssSUFBSSxJQUFULENBQUEsRUFBZ0IsSUFBSSxlQUFwQixNQUFBLEVBQUEsR0FBQSxFQUFnRDtBQUMvQyxTQUFBLG9CQUFBLENBQTBCLGVBQTFCLENBQTBCLENBQTFCO0FBQ0EsbUJBQUEsQ0FBQSxFQUFBLFVBQUE7QUFDQTs7QUFFRDtBQUNBLE9BQUcsY0FBQSxNQUFBLEdBQUgsQ0FBQSxFQUE0QjtBQUMzQixTQUFBLGNBQUEsQ0FBQSxJQUFBLENBQUEsYUFBQTtBQUNBOztBQUVELFFBQUssSUFBSSxJQUFULENBQUEsRUFBZ0IsSUFBSSxjQUFwQixNQUFBLEVBQUEsR0FBQSxFQUErQztBQUM5QyxTQUFBLG1CQUFBLENBQXlCLGNBQXpCLENBQXlCLENBQXpCOztBQUVBO0FBQ0EsUUFBRyxlQUFBLE1BQUEsSUFBSCxDQUFBLEVBQThCO0FBQzdCLG1CQUFBLENBQUEsRUFBQSxVQUFBO0FBQ0E7QUFDRDs7QUFFRCxPQUFHLEtBQUEsY0FBQSxDQUFBLE1BQUEsR0FBSCxDQUFBLEVBQWtDO0FBQ2pDLFNBQUEsV0FBQSxHQUFBLElBQUE7QUFDQTtBQUVEOzs7dUNBRW9CLFUsRUFBVztBQUMvQixjQUFBLGNBQUEsQ0FBMEIsS0FBMUIsMkJBQUEsRUFBQSxJQUFBO0FBQ0E7OztzQ0FFbUIsVSxFQUFXO0FBQzlCLGNBQUEsb0JBQUEsQ0FBZ0MsS0FBaEMsMkJBQUEsRUFBQSxJQUFBO0FBQ0E7Ozs4Q0FFMkIsVSxFQUFzQztBQUFBLE9BQTFCLG9CQUEwQixVQUFBLE1BQUEsR0FBQSxDQUFBLElBQUEsVUFBQSxDQUFBLE1BQUEsU0FBQSxHQUFBLFVBQUEsQ0FBQSxDQUFBLEdBQU4sS0FBTTs7QUFDakUsT0FBSSx3QkFBeUIsS0FBQSxjQUFBLENBQTdCLENBQTZCLENBQTdCOztBQUVBLE9BQUcsc0JBQUEsUUFBQSxDQUFILFVBQUcsQ0FBSCxFQUE4QztBQUM3QyxRQUFJLFFBQVEsc0JBQUEsT0FBQSxDQUFaLFVBQVksQ0FBWjtBQUNBLDBCQUFBLE1BQUEsQ0FBQSxLQUFBLEVBQUEsQ0FBQTtBQUNBOztBQUVELE9BQUEsaUJBQUEsRUFBcUI7QUFDcEIsZUFBQSxPQUFBO0FBQ0E7O0FBRUQsT0FBRyxzQkFBQSxNQUFBLElBQUgsQ0FBQSxFQUFxQztBQUNwQyxTQUFBLGNBQUEsQ0FBQSxNQUFBLENBQUEsQ0FBQSxFQUFBLENBQUE7QUFDQTs7QUFFQSxRQUFHLEtBQUEsY0FBQSxDQUFBLE1BQUEsSUFBSCxDQUFBLEVBQW1DO0FBQ2xDLFVBQUEsV0FBQSxHQUFBLEtBQUE7QUFDQTtBQUNBLFVBQUEsSUFBQSxDQUFBLFNBQUEsQ0FBQSxPQUFBLENBQUEscUNBQUEsRUFBbUUsS0FBbkUsSUFBQTtBQUhELEtBQUEsTUFJSztBQUNKLFVBQUssSUFBSSxJQUFULENBQUEsRUFBZ0IsSUFBSSxLQUFBLGNBQUEsQ0FBQSxDQUFBLEVBQXBCLE1BQUEsRUFBQSxHQUFBLEVBQXdEO0FBQ3ZELFVBQUksT0FBTyxLQUFBLGNBQUEsQ0FBQSxDQUFBLEVBQVgsQ0FBVyxDQUFYO0FBQ0EsV0FBQSxVQUFBO0FBQ0E7QUFDRDtBQUNEO0FBQ0Q7OzsrQkFFVztBQUNYLFFBQUEsY0FBQSxHQUFzQixLQUF0QixrQkFBQTtBQUNBLFFBQUEsV0FBQSxHQUFBLEtBQUE7QUFDQTs7OzBDQUVzQjtBQUN0QixRQUFBLGtCQUFBLEdBQUEsSUFBQTs7QUFFQSxPQUFJLFdBQVcsSUFBQSxXQUFBLENBQWdCLEVBQUMsWUFBVyxLQUFBLFdBQUEsQ0FBQSxJQUFBLENBQTNDLElBQTJDLENBQVosRUFBaEIsQ0FBZjtBQUNBLFlBQUEsR0FBQSxDQUFBLFlBQUEsRUFBMkIsRUFBQyxXQUE1QixDQUEyQixFQUEzQjtBQUNBLFFBQUEsU0FBQSxDQUFBLE9BQUEsQ0FBdUIsVUFBQSxJQUFBLEVBQWU7QUFDckMsYUFBQSxFQUFBLENBQUEsSUFBQSxFQUFBLEdBQUEsRUFBdUIsRUFBQyxPQUF4QixHQUF1QixFQUF2QixFQUFBLENBQUE7QUFERCxJQUFBOztBQUlBLFlBQUEsSUFBQSxDQUFBLG9CQUFBLEVBQUEsQ0FBQSxFQUF1QyxFQUFDLFNBQUQsSUFBQSxFQUFlLE1BQUssS0FBM0QsU0FBdUMsRUFBdkMsRUFBQSxHQUFBLENBQUEsU0FBQSxFQUFBLElBQUEsQ0FBQSxpQkFBQSxFQUFBLENBQUEsRUFFNEIsRUFBQyxPQUY3QixDQUU0QixFQUY1QixFQUFBLFNBQUEsRUFBQSxFQUFBLENBQUEsb0JBQUEsRUFBQSxDQUFBLEVBRzZCLEVBQUMsT0FIOUIsQ0FHNkIsRUFIN0IsRUFBQSxTQUFBLEVBQUEsRUFBQSxDQUFBLFlBQUEsRUFBQSxDQUFBLEVBSXFCLEVBQUMsV0FKdEIsQ0FJcUIsRUFKckIsRUFBQSxLQUFBO0FBS0E7OztnQ0FHWTtBQUNaLFFBQUEsSUFBQSxDQUFBLFNBQUEsQ0FBQSxPQUFBLENBQUEsZ0NBQUEsRUFBOEQsS0FBOUQsSUFBQTtBQUNBOzs7O0VBak8yQixPQUFPLEs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNQcEMsSUFBQSxnQkFBQSxRQUFBLDZCQUFBLENBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBSUMsVUFBQSxNQUFBLENBQUEsSUFBQSxFQUFBLFlBQUEsRUFBK0I7QUFBQSxrQkFBQSxJQUFBLEVBQUEsTUFBQTs7QUFFOUIsTUFBQSxNQUFBO0FBQ0EsTUFBRyxhQUFBLE1BQUEsSUFBSCxDQUFBLEVBQTRCO0FBQzNCLFlBQVUsS0FBQSxJQUFBLENBQUEsTUFBQSxDQUFBLENBQUEsRUFBQSxDQUFBLEVBQUEsYUFBQSxFQUFWLFNBQVUsQ0FBVjtBQURELEdBQUEsTUFFTSxJQUFHLGFBQUEsTUFBQSxJQUFILENBQUEsRUFBNEI7QUFDakMsWUFBVSxLQUFBLElBQUEsQ0FBQSxNQUFBLENBQUEsQ0FBQSxFQUFBLENBQUEsRUFBQSxhQUFBLEVBQVYsU0FBVSxDQUFWO0FBREssR0FBQSxNQUVEO0FBQ0osWUFBVSxLQUFBLElBQUEsQ0FBQSxNQUFBLENBQUEsQ0FBQSxFQUFBLENBQUEsRUFBQSxhQUFBLEVBQXNDLGFBQUEsU0FBQSxDQUFoRCxRQUFnRCxFQUF0QyxDQUFWO0FBQ0E7O0FBVDZCLE1BQUEsUUFBQSwyQkFBQSxJQUFBLEVBQUEsQ0FBQSxPQUFBLFNBQUEsSUFBQSxPQUFBLGNBQUEsQ0FBQSxNQUFBLENBQUEsRUFBQSxJQUFBLENBQUEsSUFBQSxFQUFBLElBQUEsRUFBQSxDQUFBLEVBQUEsQ0FBQSxFQVlaLE9BWlksT0FBQSxDQUFBLENBQUE7O0FBZTlCLFFBQUEsYUFBQSxHQUFzQixLQUFBLElBQUEsQ0FBQSxNQUFBLENBQUEsQ0FBQSxFQUFBLENBQUEsRUFBQSxhQUFBLEVBQXRCLFNBQXNCLENBQXRCO0FBQ0EsUUFBQSxhQUFBLEdBQXNCLEtBQUEsSUFBQSxDQUFBLE1BQUEsQ0FBQSxDQUFBLEVBQUEsQ0FBQSxFQUFBLGFBQUEsRUFBdEIsU0FBc0IsQ0FBdEI7QUFDQSxRQUFBLFdBQUEsR0FBbUIsS0FBQSxJQUFBLENBQUEsTUFBQSxDQUFBLENBQUEsRUFBQSxDQUFBLEVBQUEsYUFBQSxFQUFzQyxhQUFBLFNBQUEsQ0FBekQsUUFBeUQsRUFBdEMsQ0FBbkI7O0FBRUEsUUFBQSxNQUFBLENBQUEsR0FBQSxDQUFBLEdBQUE7QUFDQSxRQUFBLElBQUEsR0FBQSxZQUFBO0FBQ0EsUUFBQSxJQUFBLENBQUEsU0FBQSxDQUFBLEdBQUEsQ0FBd0IsTUFBeEIsc0JBQUEsRUFBQSxLQUFBO0FBQ0EsUUFBQSxJQUFBLENBQUEsZUFBQSxDQUFBLEdBQUEsQ0FBOEIsTUFBOUIsb0JBQUEsRUFBQSxLQUFBO0FBQ0EsUUFBQSxPQUFBLEdBQUEsS0FBQTtBQUNBLFFBQUEsS0FBQSxHQUFBLElBQUE7QUF4QjhCLFNBQUEsS0FBQTtBQXlCOUI7Ozs7MkJBRU87QUFDUCxRQUFBLENBQUEsR0FBUyxDQUFDLEtBQVYsQ0FBQTtBQUNBOzs7aUNBRWE7QUFDYixPQUFHLEtBQUEsSUFBQSxDQUFBLE1BQUEsSUFBSCxDQUFBLEVBQXlCO0FBQ3hCLFNBQUEsV0FBQSxDQUFBLGFBQUEsRUFBQSxTQUFBO0FBREQsSUFBQSxNQUVNLElBQUcsS0FBQSxJQUFBLENBQUEsTUFBQSxJQUFILENBQUEsRUFBeUI7QUFDOUIsU0FBQSxXQUFBLENBQUEsYUFBQSxFQUFBLFNBQUE7QUFESyxJQUFBLE1BRUQ7QUFDSixTQUFBLFdBQUEsQ0FBQSxhQUFBLEVBQWdDLEtBQUEsSUFBQSxDQUFBLFNBQUEsQ0FBaEMsUUFBZ0MsRUFBaEM7QUFDQTtBQUNEOzs7MkNBT3VCO0FBQ3ZCLFFBQUEsT0FBQSxHQUFBLElBQUE7QUFDQTs7O3lDQUVxQjtBQUNyQixRQUFBLFlBQUE7QUFDQTs7O2dEQUU0QjtBQUM1QixRQUFBLENBQUEsR0FBUyxLQUFBLElBQUEsQ0FBQSxDQUFBLEdBQWMsZUFBQSxPQUFBLENBQWQsY0FBQSxHQUE2QyxlQUFBLE9BQUEsQ0FBQSxjQUFBLEdBQXRELEdBQUE7QUFDQSxRQUFBLENBQUEsR0FBUyxLQUFBLElBQUEsQ0FBQSxDQUFBLEdBQWMsZUFBQSxPQUFBLENBQWQsY0FBQSxHQUE2QyxlQUFBLE9BQUEsQ0FBQSxjQUFBLEdBQXRELEdBQUE7QUFDQTs7O2lDQUVjLGtCLEVBQW9CLEssRUFBTTtBQUN4QyxRQUFBLEtBQUEsR0FBYSxTQUFBLEVBQUEsQ0FBWSxLQUFaLEtBQUEsRUFBQSxHQUFBLEVBQTZCLEVBQUMsR0FBRCxDQUFBLEVBQU0sR0FBTixDQUFBLEVBQVcsTUFBSyxLQUFBLE1BQUEsQ0FBQSxNQUFBLENBQWhCLEdBQWdCLENBQWhCLEVBQXlDLFlBQVksbUJBQUEsSUFBQSxDQUFyRCxLQUFxRCxDQUFyRCxFQUFzRixrQkFBaUIsQ0FBQSxJQUFBLEVBQXZHLElBQXVHLENBQXZHLEVBQXNILFFBQWhLLElBQTBDLEVBQTdCLENBQWI7QUFDQTs7O3VDQUVvQixrQixFQUFvQixLLEVBQU07QUFDOUMsT0FBSSxTQUFTLEtBQUEsSUFBQSxDQUFBLENBQUEsR0FBYyxlQUFBLE9BQUEsQ0FBZCxjQUFBLEdBQTZDLGVBQUEsT0FBQSxDQUFBLGNBQUEsR0FBMUQsR0FBQTtBQUNBLE9BQUksU0FBUyxLQUFBLElBQUEsQ0FBQSxDQUFBLEdBQWMsZUFBQSxPQUFBLENBQWQsY0FBQSxHQUE2QyxlQUFBLE9BQUEsQ0FBQSxjQUFBLEdBQTFELEdBQUE7O0FBR0EsUUFBQSxLQUFBLEdBQWEsU0FBQSxFQUFBLENBQUEsSUFBQSxFQUFBLElBQUEsRUFBd0IsRUFBRSxHQUFGLE1BQUEsRUFBYSxHQUFiLE1BQUEsRUFBd0IsTUFBTSxLQUE5QixTQUFBLEVBQThDLFlBQVksbUJBQUEsSUFBQSxDQUExRCxLQUEwRCxDQUExRCxFQUEyRixrQkFBaUIsQ0FBNUcsSUFBNEcsQ0FBNUcsRUFBb0gsUUFBekosSUFBcUMsRUFBeEIsQ0FBYjtBQUNBLFFBQUEsT0FBQSxHQUFBLEtBQUE7QUFDQTs7OzRCQUVRO0FBQ1IsUUFBQSxJQUFBLENBQUEsU0FBQSxDQUFBLE1BQUEsQ0FBMkIsS0FBM0Isc0JBQUEsRUFBQSxJQUFBO0FBQ0EsUUFBQSxPQUFBLFNBQUEsQ0FBQSxTQUFBLElBQUEsT0FBQSxjQUFBLENBQUEsT0FBQSxTQUFBLENBQUEsRUFBQSxTQUFBLEVBQUEsSUFBQSxFQUFBLElBQUEsQ0FBQSxJQUFBO0FBQ0E7OzsrQkFFVztBQUNYLFFBQUEsS0FBQSxDQUFBLElBQUE7QUFDQTs7O3NCQXRDYztBQUNkLFVBQU8sS0FBQSxJQUFBLENBQVAsU0FBQTtBQUNBOzs7O0VBN0MyQixPQUFPLE07Ozs7Ozs7Ozs7O0FDRnBDLElBQUEsZ0JBQUEsUUFBQSw2QkFBQSxDQUFBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUlDLFVBQUEsTUFBQSxDQUFBLElBQUEsRUFBaUI7QUFBQSxrQkFBQSxJQUFBLEVBQUEsTUFBQTs7QUFFaEIsTUFBSSxXQUFXLEtBQUEsR0FBQSxDQUFBLFFBQUEsQ0FBQSxDQUFBLEVBQWYsQ0FBZSxDQUFmOztBQUVBLFdBQUEsU0FBQSxDQUFBLFFBQUEsRUFBQSxDQUFBOztBQUVBLE9BQUssSUFBSSxJQUFULENBQUEsRUFBZ0IsSUFBSSxlQUFBLE9BQUEsQ0FBcEIsZ0JBQUEsRUFBQSxHQUFBLEVBQXdEO0FBQ3ZELFFBQUssSUFBSSxJQUFULENBQUEsRUFBZ0IsSUFBSSxlQUFBLE9BQUEsQ0FBcEIsVUFBQSxFQUFBLEdBQUEsRUFBa0Q7QUFDakQ7QUFDQSxRQUFHLEtBQUgsQ0FBQSxFQUFVO0FBQ1QsY0FBQSxTQUFBLENBQUEsUUFBQSxFQUFBLEdBQUE7QUFERCxLQUFBLE1BRUs7QUFDSixjQUFBLFNBQUEsQ0FBQSxRQUFBLEVBQUEsQ0FBQTtBQUNBOztBQUVELGFBQUEsU0FBQSxDQUFBLENBQUEsRUFBQSxRQUFBLEVBQUEsQ0FBQTtBQUNBLGFBQUEsUUFBQSxDQUFrQixJQUFFLGVBQUEsT0FBQSxDQUFwQixjQUFBLEVBQWlELElBQUUsZUFBQSxPQUFBLENBQW5ELGNBQUEsRUFBZ0YsZUFBQSxPQUFBLENBQWhGLGNBQUEsRUFBNkcsZUFBQSxPQUFBLENBQTdHLGNBQUE7QUFDQTtBQUNEOztBQWxCZSxNQUFBLFFBQUEsMkJBQUEsSUFBQSxFQUFBLENBQUEsT0FBQSxTQUFBLElBQUEsT0FBQSxjQUFBLENBQUEsTUFBQSxDQUFBLEVBQUEsSUFBQSxDQUFBLElBQUEsRUFBQSxJQUFBLEVBQUEsQ0FBQSxFQUFBLENBQUEsRUFvQkUsU0FwQkYsZUFvQkUsRUFwQkYsQ0FBQSxDQUFBOztBQXFCaEIsV0FBQSxPQUFBOztBQUVBLFFBQUEsWUFBQSxHQUFBLElBQUE7QUF2QmdCLFNBQUEsS0FBQTtBQXdCaEI7OztFQTFCMkIsT0FBTyxNOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNGcEMsSUFBQSxnQkFBQSxRQUFBLDZCQUFBLENBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBSUMsVUFBQSxNQUFBLENBQUEsSUFBQSxFQUFBLFdBQUEsRUFBQSxDQUFBLEVBQUEsQ0FBQSxFQUFvQztBQUFBLGtCQUFBLElBQUEsRUFBQSxNQUFBOztBQUNuQyxNQUFJLFNBQVMsRUFBRSxNQUFGLHVCQUFBLEVBQWlDLE1BQWpDLFNBQUEsRUFBa0QsT0FBL0QsUUFBYSxFQUFiOztBQUVBLE1BQUksT0FBTyxLQUFBLElBQUEsQ0FBQSxJQUFBLENBQUEsQ0FBQSxFQUFBLENBQUEsRUFBb0IsTUFBcEIsV0FBQSxFQUFYLE1BQVcsQ0FBWDtBQUNBLE9BQUEsTUFBQSxHQUFBLFNBQUE7QUFDQSxPQUFBLGVBQUEsR0FBQSxDQUFBOztBQUxtQyxNQUFBLFFBQUEsMkJBQUEsSUFBQSxFQUFBLENBQUEsT0FBQSxTQUFBLElBQUEsT0FBQSxjQUFBLENBQUEsTUFBQSxDQUFBLEVBQUEsSUFBQSxDQUFBLElBQUEsRUFBQSxJQUFBLEVBQUEsQ0FBQSxFQUFBLENBQUEsRUFPakIsS0FQaUIsZUFPakIsRUFQaUIsQ0FBQSxDQUFBOztBQVFuQyxRQUFBLE1BQUEsQ0FBQSxHQUFBLENBQUEsR0FBQTtBQUNBLFFBQUEsQ0FBQSxHQUFTLElBQUksZUFBQSxPQUFBLENBQUosY0FBQSxHQUFtQyxlQUFBLE9BQUEsQ0FBQSxjQUFBLEdBQTVDLEdBQUE7QUFDQSxRQUFBLENBQUEsR0FBUyxJQUFJLGVBQUEsT0FBQSxDQUFKLGNBQUEsR0FBbUMsZUFBQSxPQUFBLENBQUEsY0FBQSxHQUE1QyxHQUFBO0FBVm1DLFNBQUEsS0FBQTtBQVduQzs7Ozs0QkFFUTtBQUNSLE9BQUksV0FBVyxJQUFBLFdBQUEsQ0FBZ0IsRUFBQyxZQUFXLEtBQUEsT0FBQSxDQUFBLElBQUEsQ0FBNUIsSUFBNEIsQ0FBWixFQUFoQixFQUFBLEVBQUEsQ0FBQSxJQUFBLEVBQUEsQ0FBQSxFQUNGLEVBQUMsR0FBRCxPQUFBLEVBQVksTUFBSyxLQURmLE1BQ0YsRUFERSxFQUFBLEVBQUEsQ0FBQSxJQUFBLEVBQUEsR0FBQSxFQUVBLEVBQUMsT0FGRCxDQUVBLEVBRkEsRUFBZixHQUFlLENBQWY7QUFHQTs7OztFQW5CMkIsT0FBTyxNIiwiZmlsZSI6ImdlbmVyYXRlZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24oKXtmdW5jdGlvbiByKGUsbix0KXtmdW5jdGlvbiBvKGksZil7aWYoIW5baV0pe2lmKCFlW2ldKXt2YXIgYz1cImZ1bmN0aW9uXCI9PXR5cGVvZiByZXF1aXJlJiZyZXF1aXJlO2lmKCFmJiZjKXJldHVybiBjKGksITApO2lmKHUpcmV0dXJuIHUoaSwhMCk7dmFyIGE9bmV3IEVycm9yKFwiQ2Fubm90IGZpbmQgbW9kdWxlICdcIitpK1wiJ1wiKTt0aHJvdyBhLmNvZGU9XCJNT0RVTEVfTk9UX0ZPVU5EXCIsYX12YXIgcD1uW2ldPXtleHBvcnRzOnt9fTtlW2ldWzBdLmNhbGwocC5leHBvcnRzLGZ1bmN0aW9uKHIpe3ZhciBuPWVbaV1bMV1bcl07cmV0dXJuIG8obnx8cil9LHAscC5leHBvcnRzLHIsZSxuLHQpfXJldHVybiBuW2ldLmV4cG9ydHN9Zm9yKHZhciB1PVwiZnVuY3Rpb25cIj09dHlwZW9mIHJlcXVpcmUmJnJlcXVpcmUsaT0wO2k8dC5sZW5ndGg7aSsrKW8odFtpXSk7cmV0dXJuIG99cmV0dXJuIHJ9KSgpIiwiaW1wb3J0IEJvb3RTdGF0ZSBmcm9tICcuL3N0YXRlcy9Cb290U3RhdGUnXG5pbXBvcnQgTWFpbk1lbnVTdGF0ZSBmcm9tICcuL3N0YXRlcy9NYWluTWVudVN0YXRlJ1xuaW1wb3J0IEdhbWVwbGF5U3RhdGUgZnJvbSAnLi9zdGF0ZXMvZ2FtZXBsYXkvR2FtZXBsYXlTdGF0ZSdcbmltcG9ydCBTcGxhc2hTdGF0ZSBmcm9tICcuL3N0YXRlcy9TcGxhc2hTdGF0ZSdcblxuaW1wb3J0IEdhbWVDb21tYW5kZXIgZnJvbSAnLi9jb21tYW5kcy9HYW1lQ29tbWFuZGVyJ1xuXG5jbGFzcyBHYW1lIGV4dGVuZHMgUGhhc2VyLkdhbWUge1xuXHRjb25zdHJ1Y3Rvcigpe1xuXHRcdC8qdmFyIHdpblcgPSB3aW5kb3cuaW5uZXJXaWR0aCAqIHdpbmRvdy5kZXZpY2VQaXhlbFJhdGlvO1xuXHRcdHZhciB3aW5IID0gd2luZG93LmlubmVySGVpZ2h0ICogd2luZG93LmRldmljZVBpeGVsUmF0aW87Ki9cblxuXHRcdHZhciB3aW5XID0gNzUwO1xuXHRcdHZhciB3aW5IID0gMTMzNDtcblxuXHRcdHZhciBjb25maWcgPSB7XG5cdFx0XHR0eXBlOiBQaGFzZXIuQVVUTyxcblx0XHRcdHdpZHRoOiB3aW5XLFxuXHRcdFx0aGVpZ2h0OiB3aW5ILFxuXHRcdFx0cGFyZW50OiBcImFwcC1jb250YWluZXJcIlxuXHRcdH07XG5cblx0XHRzdXBlcihjb25maWcpO1xuXG5cdFx0dGhpcy5zdGF0ZS5hZGQoXCJCb290XCIsIEJvb3RTdGF0ZSk7XG5cdFx0dGhpcy5zdGF0ZS5hZGQoXCJNYWluTWVudVwiLCBNYWluTWVudVN0YXRlKTtcblx0XHR0aGlzLnN0YXRlLmFkZChcIkdhbWVwbGF5XCIsIEdhbWVwbGF5U3RhdGUpO1xuXHRcdHRoaXMuc3RhdGUuYWRkKFwiU3BsYXNoXCIsIFNwbGFzaFN0YXRlKTtcblxuXHRcdHRoaXMuY29tbWFuZGVyID0gbmV3IEdhbWVDb21tYW5kZXIoKTtcblxuXHRcdHRoaXMuc3RhdGUuc3RhcnQoXCJCb290XCIpO1xuXG5cdH1cbn1cblxudmFyIGdhbWUgPSBuZXcgR2FtZSgpOyIsIi8qIEFCU1RSQUNUIENMQVNTICovXG5leHBvcnQgZGVmYXVsdCBjbGFzcyB7XG5cblx0Y29uc3RydWN0b3IoKXtcblx0XHRcblx0fVxuXG5cdGV4ZWN1dGUoLi4uYXJncyl7XG5cdFx0cmV0dXJuIGZhbHNlOyAvLyBSZXR1cm4gJ3N1Y2Nlc3MnXG5cdH1cblxuXHRzdGF0aWMgZ2V0IE5BTUUoKSB7XG5cdFx0cmV0dXJuIFwiYWJzdHJhY3RfY2xhc3NcIjtcblx0fVxufVxuXG4iLCJpbXBvcnQgQ29tbWFuZCBmcm9tICcuL0NvbW1hbmQnXG5pbXBvcnQgQm9vdHN0cmFwQ29tbWFuZCBmcm9tICcuL2FwcC9Cb290c3RyYXBDb21tYW5kJ1xuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyB7XG5cdGNvbnN0cnVjdG9yKCl7XG5cdFx0dGhpcy5jb21tYW5kcyA9IHt9XG5cdFx0dGhpcy5yZWdpc3RlckNvbW1hbmQoQm9vdHN0cmFwQ29tbWFuZC5OQU1FLCBuZXcgQm9vdHN0cmFwQ29tbWFuZCgpKTtcblx0fVxuXG5cdHJlZ2lzdGVyQ29tbWFuZChuYW1lLCBjb21tYW5kLCBvdmVycmlkZSA9IGZhbHNlKXtcblxuXHRcdGlmKChuYW1lIGluIHRoaXMuY29tbWFuZHMgJiYgIW92ZXJyaWRlKSB8fCAhY29tbWFuZCBpbnN0YW5jZW9mIENvbW1hbmQpe1xuXHRcdFx0cmV0dXJuO1xuXHRcdH1cblxuXHRcdGNvbnNvbGUubG9nKFwiUmVnaXN0ZXJlZCBcIiArIG5hbWUpO1xuXG5cdFx0dGhpcy5jb21tYW5kc1tuYW1lXSA9IGNvbW1hbmQ7XG5cdH1cblxuXHRleGVjdXRlKG5hbWUsIC4uLmFyZ3Mpe1xuXHRcdGlmKG5hbWUgaW4gdGhpcy5jb21tYW5kcyl7XG5cdFx0XHQvL2NvbnNvbGUubG9nKFwiRXhlY3V0ZTogXCIgKyBuYW1lKTtcblx0XHRcdHJldHVybiB0aGlzLmNvbW1hbmRzW25hbWVdLmV4ZWN1dGUoLi4uYXJncyk7XG5cdFx0fWVsc2V7XG5cdFx0XHRjb25zb2xlLmxvZyhcIlVua25vd24gY29tbWFuZDogXCIgKyBuYW1lKTtcblx0XHRcdHJldHVybiBmYWxzZTtcblx0XHR9XG5cdFx0XG5cdH1cbn0iLCJpbXBvcnQgQ29tbWFuZCBmcm9tICcuLi9Db21tYW5kJ1xuXG5pbXBvcnQgUGxheWVyRGF0YSBmcm9tICcuLi8uLi9tb2RlbC9QbGF5ZXJEYXRhJ1xuaW1wb3J0IEdlbmVyYXRlR3JpZERhdGFDb21tYW5kIGZyb20gJy4uL2dhbWVwbGF5L0dlbmVyYXRlR3JpZERhdGFDb21tYW5kJ1xuaW1wb3J0IEluaXRpYWxpc2VHYW1lcGxheUNvbW1hbmQgZnJvbSAnLi4vZ2FtZXBsYXkvSW5pdGlhbGlzZUdhbWVwbGF5Q29tbWFuZCdcbmltcG9ydCBDcmVhdGVOZXh0R3JpZERpc2NDb21tYW5kIGZyb20gJy4uL2dhbWVwbGF5L0NyZWF0ZU5leHRHcmlkRGlzY0NvbW1hbmQnXG5pbXBvcnQgRHJvcEdyaWREaXNjQ29tbWFuZCBmcm9tICcuLi9nYW1lcGxheS9Ecm9wR3JpZERpc2NDb21tYW5kJ1xuaW1wb3J0IERyb3BOZXh0R3JpZERpc2NDb21tYW5kIGZyb20gJy4uL2dhbWVwbGF5L0Ryb3BOZXh0R3JpZERpc2NDb21tYW5kJ1xuaW1wb3J0IEV2YWx1YXRlR3JpZE1hdGNoZXNDb21tYW5kIGZyb20gJy4uL2dhbWVwbGF5L0V2YWx1YXRlR3JpZE1hdGNoZXNDb21tYW5kJ1xuaW1wb3J0IEhhbmRsZVR1cm5FbmRDb21tYW5kIGZyb20gJy4uL2dhbWVwbGF5L0hhbmRsZVR1cm5FbmRDb21tYW5kJ1xuaW1wb3J0IEhhbmRsZUdhbWVPdmVyQ29tbWFuZCBmcm9tICcuLi9nYW1lcGxheS9IYW5kbGVHYW1lT3ZlckNvbW1hbmQnXG5pbXBvcnQgUGxheVNvdW5kQ29tbWFuZCBmcm9tICcuL1BsYXlTb3VuZENvbW1hbmQnXG5cblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgZXh0ZW5kcyBDb21tYW5kIHtcblxuXHRjb25zdHJ1Y3Rvcigpe1xuXHRcdHN1cGVyKCk7XG5cdFx0XG5cdFx0XG5cdH1cblxuXHRleGVjdXRlKGdhbWUpe1xuXHRcdGNvbnNvbGUubG9nKFwiQm9vdHN0cmFwQ29tbWFuZFwiKTtcblx0XHRnYW1lLm1vZGVsLnBsYXllciA9IG5ldyBQbGF5ZXJEYXRhKCk7XG5cblx0XHQvLyBjcmVhdGUgZ2FtcGxheSBjb21tYW5kc1xuXHRcdGdhbWUuY29tbWFuZGVyLnJlZ2lzdGVyQ29tbWFuZChQbGF5U291bmRDb21tYW5kLk5BTUUsIG5ldyBQbGF5U291bmRDb21tYW5kKGdhbWUpKTtcblx0XHRnYW1lLmNvbW1hbmRlci5yZWdpc3RlckNvbW1hbmQoSW5pdGlhbGlzZUdhbWVwbGF5Q29tbWFuZC5OQU1FLCBuZXcgSW5pdGlhbGlzZUdhbWVwbGF5Q29tbWFuZChnYW1lKSk7XG5cdFx0Z2FtZS5jb21tYW5kZXIucmVnaXN0ZXJDb21tYW5kKEdlbmVyYXRlR3JpZERhdGFDb21tYW5kLk5BTUUsIG5ldyBHZW5lcmF0ZUdyaWREYXRhQ29tbWFuZCgpKTtcblx0XHRnYW1lLmNvbW1hbmRlci5yZWdpc3RlckNvbW1hbmQoRHJvcEdyaWREaXNjQ29tbWFuZC5OQU1FLCBuZXcgRHJvcEdyaWREaXNjQ29tbWFuZCgpKTtcblx0XHRnYW1lLmNvbW1hbmRlci5yZWdpc3RlckNvbW1hbmQoRHJvcE5leHRHcmlkRGlzY0NvbW1hbmQuTkFNRSwgbmV3IERyb3BOZXh0R3JpZERpc2NDb21tYW5kKCkpO1xuXHRcdGdhbWUuY29tbWFuZGVyLnJlZ2lzdGVyQ29tbWFuZChFdmFsdWF0ZUdyaWRNYXRjaGVzQ29tbWFuZC5OQU1FLCBuZXcgRXZhbHVhdGVHcmlkTWF0Y2hlc0NvbW1hbmQoKSk7XG5cdFx0Z2FtZS5jb21tYW5kZXIucmVnaXN0ZXJDb21tYW5kKEhhbmRsZVR1cm5FbmRDb21tYW5kLk5BTUUsIG5ldyBIYW5kbGVUdXJuRW5kQ29tbWFuZCgpKTtcblx0XHRnYW1lLmNvbW1hbmRlci5yZWdpc3RlckNvbW1hbmQoSGFuZGxlR2FtZU92ZXJDb21tYW5kLk5BTUUsIG5ldyBIYW5kbGVHYW1lT3ZlckNvbW1hbmQoKSk7XG5cdFx0XG5cdFx0cmV0dXJuIHRydWU7XG5cblx0fVxuXG5cdHN0YXRpYyBnZXQgTkFNRSgpIHtcblx0XHRyZXR1cm4gXCJBcHBDb21tYW5kLkJvb3RzdHJhcFwiO1xuXHR9XG59XG5cbiIsImltcG9ydCBDb21tYW5kIGZyb20gJy4uL0NvbW1hbmQnXG5cblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgZXh0ZW5kcyBDb21tYW5kIHtcblxuXHRjb25zdHJ1Y3RvcihnYW1lKXtcblx0XHRzdXBlcigpO1xuXG5cdFx0dGhpcy5zb3VuZHMgPSB7fVxuXG5cdFx0dGhpcy5zb3VuZHNbXCJicmVha19kaXNjXCJdID0gZ2FtZS5hZGQuYXVkaW8oJ25ld19nYW1lJyk7XG5cdFx0dGhpcy5zb3VuZHNbXCJkcm9wX2Rpc2NcIl0gPSBnYW1lLmFkZC5hdWRpbygnZHJvcF9kaXNjJyk7XG5cdFx0dGhpcy5zb3VuZHNbXCJnYW1lX292ZXJcIl0gPSBnYW1lLmFkZC5hdWRpbygnZ2FtZV9vdmVyJyk7XG5cdFx0dGhpcy5zb3VuZHNbXCJuZXdfZ2FtZVwiXSA9IGdhbWUuYWRkLmF1ZGlvKCduZXdfZ2FtZScpO1xuXHRcdHRoaXMuc291bmRzW1wibmV3X3Jvd1wiXSA9IGdhbWUuYWRkLmF1ZGlvKCduZXdfcm93Jyk7XG5cdFx0dGhpcy5zb3VuZHNbXCJ3ZWxjb21lXCJdID0gZ2FtZS5hZGQuYXVkaW8oJ3dlbGNvbWUnKTtcblx0XHR0aGlzLnNvdW5kc1tcImNhbmNlbFwiXSA9IGdhbWUuYWRkLmF1ZGlvKCdjYW5jZWwnKTtcblx0fVxuXG5cdGV4ZWN1dGUoc291bmROYW1lKXtcblx0XHRpZihzb3VuZE5hbWUgaW4gdGhpcy5zb3VuZHMpe1xuXHRcdFx0dGhpcy5zb3VuZHNbc291bmROYW1lXS5wbGF5KCk7XG5cdFx0XHRyZXR1cm4gdHJ1ZTtcblx0XHR9ZWxzZXtcblx0XHRcdGNvbnNvbGUubG9nKFwiVW5rbm93biBzb3VuZDogXCIgKyBzb3VuZE5hbWUpO1xuXHRcdFx0cmV0dXJuIGZhbHNlO1xuXHRcdH1cblx0fVxuXG5cdHN0YXRpYyBnZXQgTkFNRSgpIHtcblx0XHRyZXR1cm4gXCJBcHBDb21tYW5kLlBsYXlTb3VuZFwiO1xuXHR9XG59XG5cbiIsImltcG9ydCBDb21tYW5kIGZyb20gJy4uL0NvbW1hbmQnXG5pbXBvcnQgR2FtZVNldHRpbmdzIGZyb20gJy4uLy4uL3NldHRpbmdzL0dhbWVTZXR0aW5ncydcbmltcG9ydCBHYW1lcGxheUdyaWREaXNjRGF0YSBmcm9tICcuLi8uLi9tb2RlbC9nYW1lcGxheS9HYW1lcGxheUdyaWREaXNjRGF0YSdcblxuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBleHRlbmRzIENvbW1hbmQge1xuXG5cdGNvbnN0cnVjdG9yKGdhbWVwbGF5U3RhdGUpe1xuXHRcdHN1cGVyKCk7XG5cdFx0dGhpcy5nYW1lcGxheVN0YXRlID0gZ2FtZXBsYXlTdGF0ZTtcblx0fVxuXG5cdGV4ZWN1dGUoZ2FtZSl7XG5cdFx0dmFyIGdyaWQgPSBnYW1lLm1vZGVsLmdyaWREYXRhO1xuXHRcdGdyaWQuc2hpZnRVcEFsbCgpO1xuXG5cdFx0Zm9yICh2YXIgeCA9IDA7IHggPCBHYW1lU2V0dGluZ3MuR1JJRF9XSURUSDsgeCsrKSB7XG5cdFx0XHR2YXIgZGlzY0RhdGEgPSB0aGlzLmNyZWF0ZURpc2MoZ3JpZCwgeCwgR2FtZVNldHRpbmdzLlRSVUVfR1JJRF9IRUlHSFQtMSk7XG5cdFx0XHRnYW1lLmNvbW1hbmRlci5leGVjdXRlKFwiR2FtZXBsYXlDb21tYW5kLkNyZWF0ZURpc2NTcHJpdGVcIiwgZ2FtZSwgZGlzY0RhdGEsIHRydWUpO1xuXHRcdH1cblxuXHRcdGdhbWUuY29tbWFuZGVyLmV4ZWN1dGUoXCJHYW1lcGxheUNvbW1hbmQuQW5pbWF0ZURpc2NzXCIpO1xuXG5cdFx0cmV0dXJuIHRydWU7XG5cdH1cblxuXHRjcmVhdGVEaXNjKGdyaWQsIHgsIHkpe1xuXHRcdHZhciBtaW4gPSBHYW1lU2V0dGluZ3MuTUlOX0RJU0NfVkFMVUU7XG5cdFx0dmFyIG1heCA9IEdhbWVTZXR0aW5ncy5NQVhfRElTQ19WQUxVRTtcblxuXHRcdHZhciBkaXNjVmFsdWUgPSBNYXRoLmZsb29yKE1hdGgucmFuZG9tKCkgKiAobWF4IC0gbWluICsgMSkpICsgbWluO1xuXG5cdFx0dmFyIGRpc2NEYXRhID0gbmV3IEdhbWVwbGF5R3JpZERpc2NEYXRhKGRpc2NWYWx1ZSwgR2FtZVNldHRpbmdzLkRFRkFVTFRfQVJNT1VSX1ZBTFVFKVxuXHRcdGdyaWQuYWRkRGlzY0F0UG9zaXRpb24oZGlzY0RhdGEsIHgsIHkpO1xuXG5cdFx0cmV0dXJuIGRpc2NEYXRhO1xuXG5cdH1cblxuXHRzdGF0aWMgZ2V0IE5BTUUoKSB7XG5cdFx0cmV0dXJuIFwiR2FtZXBsYXlDb21tYW5kLkFkZEJvdHRvbVJvd0Rpc2NzXCI7XG5cdH1cbn1cblxuIiwiaW1wb3J0IENvbW1hbmQgZnJvbSAnLi4vQ29tbWFuZCdcblxuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBleHRlbmRzIENvbW1hbmQge1xuXG5cdGNvbnN0cnVjdG9yKGdhbWVwbGF5U3RhdGUpe1xuXHRcdHN1cGVyKCk7XG5cdFx0dGhpcy5nYW1lcGxheVNwcml0ZSA9IGdhbWVwbGF5U3RhdGU7XG5cdH1cblxuXHRleGVjdXRlKGdhbWUpe1xuXHRcdHRoaXMuZ2FtZXBsYXlTcHJpdGUuaGFuZGxlQW5pbWF0aW9ucygpO1xuXG5cdFx0cmV0dXJuIHRydWU7XG5cdH1cblxuXHRzdGF0aWMgZ2V0IE5BTUUoKSB7XG5cdFx0cmV0dXJuIFwiR2FtZXBsYXlDb21tYW5kLkFuaW1hdGVEaXNjc1wiO1xuXHR9XG59XG5cbiIsImltcG9ydCBDb21tYW5kIGZyb20gJy4uL0NvbW1hbmQnXG5pbXBvcnQgR2FtZVNldHRpbmdzIGZyb20gJy4uLy4uL3NldHRpbmdzL0dhbWVTZXR0aW5ncydcblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgZXh0ZW5kcyBDb21tYW5kIHtcblxuXHRjb25zdHJ1Y3RvcihnYW1lcGxheVN0YXRlKXtcblx0XHRzdXBlcigpO1xuXHRcdHRoaXMuZ2FtZXBsYXlTdGF0ZSA9IGdhbWVwbGF5U3RhdGU7XG5cdH1cblxuXHRleGVjdXRlKGdhbWUpe1xuXHRcdHZhciBncmlkID0gZ2FtZS5tb2RlbC5ncmlkRGF0YTtcblx0XHQvL0dhbWUgb3ZlciBpZiBncmlkIGZ1bGwgb3IgZGlzY3MgaW4gdG9wIHJvd1xuXHRcdHZhciBpc0dhbWVPdmVyID0gdHJ1ZTtcblxuXHRcdGZvciAodmFyIGkgPSAwOyBpIDwgR2FtZVNldHRpbmdzLkdSSURfV0lEVEggKiBHYW1lU2V0dGluZ3MuVFJVRV9HUklEX0hFSUdIVDsgaSsrKSB7XG5cdFx0XHR2YXIgZGlzY0RhdGEgPSBncmlkLmRhdGFbaV07XG5cdFx0XHR2YXIgeCA9IGkgJSBHYW1lU2V0dGluZ3MuR1JJRF9XSURUSDtcblx0XHRcdHZhciB5ID0gTWF0aC5mbG9vcihpIC8gR2FtZVNldHRpbmdzLkdSSURfV0lEVEgpO1xuXG5cdFx0XHRpZih5ID09IDApe1xuXHRcdFx0XHRpZihkaXNjRGF0YSAhPSBudWxsKXtcblx0XHRcdFx0XHRpc0dhbWVPdmVyID0gdHJ1ZTtcblx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0fVxuXHRcdFx0fWVsc2V7XG5cdFx0XHRcdGlmKGRpc2NEYXRhID09IG51bGwpe1xuXHRcdFx0XHRcdGlzR2FtZU92ZXIgPSBmYWxzZTtcblx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0fVxuXHRcdFx0fVxuXHRcdH1cblxuXHRcdGlmKGlzR2FtZU92ZXIpe1xuXHRcdFx0dGhpcy5nYW1lcGxheVN0YXRlLmJlZ2luR2FtZU92ZXJTZXF1ZW5jZSgpO1xuXHRcdFx0Z2FtZS5jb21tYW5kZXIuZXhlY3V0ZShcIkFwcENvbW1hbmQuUGxheVNvdW5kXCIsIFwiZ2FtZV9vdmVyXCIpO1xuXG5cdFx0XHRyZXR1cm4gdHJ1ZTtcblx0XHR9ZWxzZXtcblx0XHRcdGdhbWUubW9kZWwuZ2FtZXBsYXkuaW5jcmVhc2VDb21ibygpO1xuXHRcdFx0Z2FtZS5jb21tYW5kZXIuZXhlY3V0ZShcIkdhbWVwbGF5Q29tbWFuZC5BbmltYXRlRGlzY3NcIik7XG5cblx0XHRcdHJldHVybiBmYWxzZTtcblx0XHR9XG5cdH1cblxuXHRzdGF0aWMgZ2V0IE5BTUUoKSB7XG5cdFx0cmV0dXJuIFwiR2FtZXBsYXlDb21tYW5kLkNoZWNrRm9yR2FtZU92ZXJcIjtcblx0fVxufVxuXG4iLCJpbXBvcnQgQ29tbWFuZCBmcm9tICcuLi9Db21tYW5kJ1xuXG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIGV4dGVuZHMgQ29tbWFuZCB7XG5cblx0Y29uc3RydWN0b3IoZ2FtZXBsYXlTdGF0ZSl7XG5cdFx0c3VwZXIoKTtcblx0XHR0aGlzLmdhbWVwbGF5U3RhdGUgPSBnYW1lcGxheVN0YXRlO1xuXHR9XG5cblx0ZXhlY3V0ZShnYW1lLCBncmlkRGlzY0RhdGEsIGZvcmNlV29ybGRQb3NpdGlvbiA9IGZhbHNlKXtcblx0XHR2YXIgc3ByaXRlID0gdGhpcy5nYW1lcGxheVN0YXRlLmNyZWF0ZU5ld0dyaWREaXNjU3ByaXRlKGdyaWREaXNjRGF0YSk7XG5cblx0XHRpZihmb3JjZVdvcmxkUG9zaXRpb24pe1xuXHRcdFx0c3ByaXRlLmdyaWRQb3NpdGlvblRvV29ybGRQb3NpdGlvbigpO1xuXHRcdH1cblxuXHRcdHJldHVybiB0cnVlO1xuXHR9XG5cblx0c3RhdGljIGdldCBOQU1FKCkge1xuXHRcdHJldHVybiBcIkdhbWVwbGF5Q29tbWFuZC5DcmVhdGVEaXNjU3ByaXRlXCI7XG5cdH1cbn1cblxuIiwiaW1wb3J0IENvbW1hbmQgZnJvbSAnLi4vQ29tbWFuZCdcblxuaW1wb3J0IEdhbWVwbGF5R3JpZERpc2NEYXRhIGZyb20gJy4uLy4uL21vZGVsL2dhbWVwbGF5L0dhbWVwbGF5R3JpZERpc2NEYXRhJ1xuaW1wb3J0IEdhbWVTZXR0aW5ncyBmcm9tICcuLi8uLi9zZXR0aW5ncy9HYW1lU2V0dGluZ3MnXG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIGV4dGVuZHMgQ29tbWFuZCB7XG5cblx0Y29uc3RydWN0b3IoZ2FtZXBsYXlTdGF0ZSl7XG5cdFx0c3VwZXIoKTtcblx0XHR0aGlzLmdhbWVwbGF5U3RhdGUgPSBnYW1lcGxheVN0YXRlO1xuXHR9XG5cblx0ZXhlY3V0ZShnYW1lKXtcblx0XHR2YXIgbWluID0gR2FtZVNldHRpbmdzLk1JTl9ESVNDX1ZBTFVFO1xuXHRcdHZhciBtYXggPSBHYW1lU2V0dGluZ3MuTUFYX0RJU0NfVkFMVUU7XG5cblx0XHR2YXIgZGlzY1ZhbHVlID0gTWF0aC5mbG9vcihNYXRoLnJhbmRvbSgpICogKG1heCAtIG1pbiArIDEpKSArIG1pbjtcblx0XHR2YXIgYXJtb3VyVmFsdWUgPSAoTWF0aC5yYW5kb20oKSA8ICBHYW1lU2V0dGluZ3MuQ0hBTkNFX09GX0FSTU9VUl9ESVNDKT8gR2FtZVNldHRpbmdzLkRFRkFVTFRfQVJNT1VSX1ZBTFVFIDogMDtcblxuXG5cdFx0dmFyIGRpc2NEYXRhID0gbmV3IEdhbWVwbGF5R3JpZERpc2NEYXRhKGRpc2NWYWx1ZSwgYXJtb3VyVmFsdWUpO1xuXHRcdHRoaXMuZ2FtZXBsYXlTdGF0ZS5jcmVhdGVOZXh0RGlzY1Nwcml0ZShkaXNjRGF0YSk7XG5cblx0XHRyZXR1cm4gdHJ1ZTtcblx0fVxuXG5cdHN0YXRpYyBnZXQgTkFNRSgpIHtcblx0XHRyZXR1cm4gXCJHYW1lcGxheUNvbW1hbmQuQ3JlYXRlTmV4dEdyaWREaXNjXCI7XG5cdH1cbn1cblxuIiwiaW1wb3J0IENvbW1hbmQgZnJvbSAnLi4vQ29tbWFuZCdcblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgZXh0ZW5kcyBDb21tYW5kIHtcblxuXHRjb25zdHJ1Y3RvcihnYW1lcGxheVN0YXRlKXtcblx0XHRzdXBlcigpO1xuXHRcdHRoaXMuZ2FtZXBsYXlTdGF0ZSA9IGdhbWVwbGF5U3RhdGU7XG5cdH1cblxuXHRleGVjdXRlKHBvaW50cywgeCwgeSl7XG5cdFx0dGhpcy5nYW1lcGxheVN0YXRlLmNyZWF0ZVBvaW50c1Nwcml0ZShwb2ludHMsIHgsIHkpO1xuXG5cdFx0cmV0dXJuIHRydWU7XG5cdH1cblxuXHRzdGF0aWMgZ2V0IE5BTUUoKSB7XG5cdFx0cmV0dXJuIFwiR2FtZXBsYXlDb21tYW5kLkNyZWF0ZVBvaW50c1Nwcml0ZVwiO1xuXHR9XG59XG5cbiIsImltcG9ydCBDb21tYW5kIGZyb20gJy4uL0NvbW1hbmQnXG5pbXBvcnQgR2FtZXBsYXlHcmlkRGlzY0RhdGEgZnJvbSAnLi4vLi4vbW9kZWwvZ2FtZXBsYXkvR2FtZXBsYXlHcmlkRGlzY0RhdGEnXG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIGV4dGVuZHMgQ29tbWFuZCB7XG5cblx0Y29uc3RydWN0b3IoKXtcblx0XHRzdXBlcigpO1xuXHRcdFxuXHR9XG5cblx0ZXhlY3V0ZShnYW1lLCBkaXNjRGF0YSwgY3JlYXRlU3ByaXRlID0gZmFsc2Upe1xuXHRcdGNvbnNvbGUubG9nKFwiRHJvcHBpbmcgYSBcIiArIGRpc2NEYXRhLmRpc2NWYWx1ZSArIFwiIGluIGNvbHVtbiBcIiArIGRpc2NEYXRhLngpO1xuXG5cdFx0aWYgKGdhbWUubW9kZWwuZ3JpZERhdGEuZHJvcEluQ29sdW1uKGRpc2NEYXRhLCBkaXNjRGF0YS54KSl7XG5cdFx0XHRpZihjcmVhdGVTcHJpdGUpe1xuXHRcdFx0XHRnYW1lLmNvbW1hbmRlci5leGVjdXRlKFwiR2FtZXBsYXlDb21tYW5kLkNyZWF0ZURpc2NTcHJpdGVcIiwgZ2FtZSwgZGlzY0RhdGEpO1xuXHRcdFx0fVxuXHRcdFx0XG5cdFx0XHRyZXR1cm4gdHJ1ZTtcblx0XHQgfWVsc2V7XG5cdFx0XHRyZXR1cm4gZmFsc2U7XG5cdFx0IH1cblxuXHRcdFxuXHR9XG5cblx0c3RhdGljIGdldCBOQU1FKCkge1xuXHRcdHJldHVybiBcIkdhbWVwbGF5Q29tbWFuZC5Ecm9wR3JpZERpc2NcIjtcblx0fVxufVxuXG4iLCJpbXBvcnQgQ29tbWFuZCBmcm9tICcuLi9Db21tYW5kJ1xuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBleHRlbmRzIENvbW1hbmQge1xuXG5cdGNvbnN0cnVjdG9yKCl7XG5cdFx0c3VwZXIoKTtcblx0XHRcblx0fVxuXG5cdGV4ZWN1dGUoZ2FtZSwgZGlzY0RhdGEsIGNvbHVtbiwgZ2FtZXN0YXRlKXtcblx0XHRkaXNjRGF0YS54ID0gY29sdW1uO1xuXG5cdFx0dmFyIGRyb3BTdWNjZXNzZnVsID0gZ2FtZS5jb21tYW5kZXIuZXhlY3V0ZShcIkdhbWVwbGF5Q29tbWFuZC5Ecm9wR3JpZERpc2NcIiwgZ2FtZSwgZGlzY0RhdGEsIGZhbHNlKTtcblx0XHRcblx0XHRpZihkcm9wU3VjY2Vzc2Z1bCl7XG5cdFx0XHRnYW1lLmNvbW1hbmRlci5leGVjdXRlKFwiQXBwQ29tbWFuZC5QbGF5U291bmRcIiwgXCJkcm9wX2Rpc2NcIik7XG5cdFx0XHRnYW1lc3RhdGUuaGFuZGxlQW5pbWF0aW9ucygpO1xuXHRcdFx0cmV0dXJuIHRydWU7XG5cdFx0fWVsc2V7XG5cdFx0XHRnYW1lLmNvbW1hbmRlci5leGVjdXRlKFwiQXBwQ29tbWFuZC5QbGF5U291bmRcIiwgXCJjYW5jZWxcIik7XG5cblx0XHRcdGdhbWVzdGF0ZS5jYW5jZWxEcm9wKCk7XG5cdFx0XHRyZXR1cm4gZmFsc2U7XG5cdFx0fVxuXHR9XG5cblx0c3RhdGljIGdldCBOQU1FKCkge1xuXHRcdHJldHVybiBcIkdhbWVwbGF5Q29tbWFuZC5Ecm9wTmV4dEdyaWREaXNjXCI7XG5cdH1cbn1cblxuIiwiaW1wb3J0IENvbW1hbmQgZnJvbSAnLi4vQ29tbWFuZCdcbmltcG9ydCBHYW1lU2V0dGluZ3MgZnJvbSAnLi4vLi4vc2V0dGluZ3MvR2FtZVNldHRpbmdzJ1xuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBleHRlbmRzIENvbW1hbmQge1xuXG5cdGNvbnN0cnVjdG9yKCl7XG5cdFx0c3VwZXIoKTtcblx0XHRcblx0fVxuXG5cdGV4ZWN1dGUoZ2FtZSwgY2FsbGJhY2spe1xuXHRcdC8vIGdldCByb3cgbWF0Y2hlc1xuXHRcdHZhciBncmlkID0gZ2FtZS5tb2RlbC5ncmlkRGF0YTtcblx0XHR2YXIgcm93TWF0Y2hlcyA9IGdyaWQuY2hlY2tSb3dzRm9yTWF0Y2hlcygpO1xuXHRcdHZhciBjb2x1bW5NYXRjaGVzID0gZ3JpZC5jaGVja0NvbHVtbnNGb3JNYXRjaGVzKCk7XG5cdFx0dmFyIGFsbE1hdGNoZXMgPSByb3dNYXRjaGVzLmNvbmNhdChjb2x1bW5NYXRjaGVzKTtcblxuXHRcdHZhciBjb2x1bW5zVG9VcGRhdGUgPSBbXTtcblx0XHRmb3IgKHZhciBpID0gMDsgaSA8IGFsbE1hdGNoZXMubGVuZ3RoOyBpKyspIHtcblxuXHRcdFx0Z2FtZS5tb2RlbC5nYW1lcGxheS5pbmNyZWFzZVNjb3JlKCk7XG5cblx0XHRcdHZhciBtYXRjaGVkRGlzYyA9IGFsbE1hdGNoZXNbaV07XG5cdFx0XHRpZighY29sdW1uc1RvVXBkYXRlLmluY2x1ZGVzKG1hdGNoZWREaXNjLngpKXtcblx0XHRcdFx0Y29sdW1uc1RvVXBkYXRlLnB1c2gobWF0Y2hlZERpc2MueCk7XG5cdFx0XHR9XG5cblx0XHRcdGdyaWQucmVtb3ZlRGlzY0F0UG9zaXRpb24obWF0Y2hlZERpc2MueCwgbWF0Y2hlZERpc2MueSk7XG5cdFx0XHR0aGlzLmJyZWFrQWRqYWNlbnRBcm1vdXIoZ3JpZCwgbWF0Y2hlZERpc2MueCwgbWF0Y2hlZERpc2MueSk7XG5cdFx0XHRtYXRjaGVkRGlzYy5kZXN0cm95ZWQgPSB0cnVlO1xuXHRcdFx0Z2FtZS5jb21tYW5kZXIuZXhlY3V0ZShcIkdhbWVwbGF5Q29tbWFuZC5DcmVhdGVQb2ludHNTcHJpdGVcIiwgZ2FtZS5tb2RlbC5nYW1lcGxheS5nZXRDb21ib1ZhbHVlKCksIG1hdGNoZWREaXNjLngsIG1hdGNoZWREaXNjLnkpO1xuXHRcdH1cblxuXHRcdGZvciAodmFyIGogPSAwOyBqIDwgY29sdW1uc1RvVXBkYXRlLmxlbmd0aDsgaisrKSB7XG5cdFx0XHR2YXIgeCA9IGNvbHVtbnNUb1VwZGF0ZVtqXTtcblx0XHRcdGdyaWQudXBkYXRlQ29sdW1uKHgpO1xuXHRcdH1cblxuXHRcdGdhbWUuY29tbWFuZGVyLmV4ZWN1dGUoXCJHYW1lcGxheUNvbW1hbmQuQ2hlY2tGb3JHYW1lT3ZlclwiLCBnYW1lKTtcblxuXHRcdGlmKGFsbE1hdGNoZXMubGVuZ3RoID09IDApe1xuXHRcdFx0Ly9HcmlkIHN0YWJpbGlzZWQgLSBhZGQgbmV4dCBkaXNjXG5cdFx0XHRpZihnYW1lLm1vZGVsLmdhbWVwbGF5LmlzUm91bmRDb21wbGV0ZSgpKXtcblx0XHRcdFx0Z2FtZS5jb21tYW5kZXIuZXhlY3V0ZShcIkdhbWVwbGF5Q29tbWFuZC5IYW5kbGVUdXJuRW5kXCIsIGdhbWUpO1xuXHRcdFx0fWVsc2V7XG5cdFx0XHRcdGdhbWUubW9kZWwuZ2FtZXBsYXkubmV4dFR1cm4oKTtcblx0XHRcdFx0Z2FtZS5jb21tYW5kZXIuZXhlY3V0ZShcIkdhbWVwbGF5Q29tbWFuZC5DcmVhdGVOZXh0R3JpZERpc2NcIiwgZ2FtZSk7XG5cdFx0XHRcdGdhbWUubW9kZWwuZ2FtZXBsYXkucmVzZXRDb21ibygpO1xuXHRcdFx0fVxuXHRcdH1lbHNle1xuXHRcdFx0Z2FtZS5jb21tYW5kZXIuZXhlY3V0ZShcIkFwcENvbW1hbmQuUGxheVNvdW5kXCIsIFwiYnJlYWtfZGlzY1wiKTtcblx0XHR9XG5cdFx0XG5cdFx0cmV0dXJuIGFsbE1hdGNoZXMubGVuZ3RoID4gMDtcblx0fVxuXG5cdGJyZWFrQWRqYWNlbnRBcm1vdXIoZ3JpZCwgeCwgeSl7XG5cdFx0Ly8gZ2V0IGxlZnRcblx0XHRpZih4ID4gMCl7XG5cdFx0XHR2YXIgbGVmdERhdGEgPSBncmlkLmdldERpc2NBdFBvc2l0aW9uKHgtMSwgeSk7XG5cblx0XHRcdGlmKGxlZnREYXRhICE9IG51bGwpe1xuXHRcdFx0XHRsZWZ0RGF0YS5hcm1vdXItLTtcblx0XHRcdH1cblx0XHR9XG5cdFx0Ly8gZ2V0IHJpZ2h0XG5cdFx0aWYoeCA8IEdhbWVTZXR0aW5ncy5HUklEX1dJRFRILTEpe1xuXHRcdFx0dmFyIHJpZ2h0RGF0YSA9IGdyaWQuZ2V0RGlzY0F0UG9zaXRpb24oeCsxLCB5KTtcblxuXHRcdFx0aWYocmlnaHREYXRhICE9IG51bGwpe1xuXHRcdFx0XHRyaWdodERhdGEuYXJtb3VyLS07XG5cdFx0XHR9XG5cdFx0fVxuXHRcdC8vZ2V0IHRvcFxuXHRcdGlmKHkgPiAwKXtcblx0XHRcdHZhciB0b3BEYXRhID0gZ3JpZC5nZXREaXNjQXRQb3NpdGlvbih4LCB5LTEpO1xuXG5cdFx0XHRpZih0b3BEYXRhICE9IG51bGwpe1xuXHRcdFx0XHR0b3BEYXRhLmFybW91ci0tO1xuXHRcdFx0fVxuXHRcdH1cblx0XHQvL2dldCBib3R0b21cblx0XHRpZih5IDwgR2FtZVNldHRpbmdzLlRSVUVfR1JJRF9IRUlHSFQtMSl7XG5cdFx0XHR2YXIgYm90dG9tRGF0YSA9IGdyaWQuZ2V0RGlzY0F0UG9zaXRpb24oeCwgeSsxKTtcblxuXHRcdFx0aWYoYm90dG9tRGF0YSAhPSBudWxsKXtcblx0XHRcdFx0Ym90dG9tRGF0YS5hcm1vdXItLTtcblx0XHRcdH1cblx0XHR9XG5cdH1cblxuXHRzdGF0aWMgZ2V0IE5BTUUoKSB7XG5cdFx0cmV0dXJuIFwiR2FtZXBsYXlDb21tYW5kLkV2YWx1YXRlR3JpZE1hdGNoZXNcIjtcblx0fVxufVxuXG4iLCJpbXBvcnQgQ29tbWFuZCBmcm9tICcuLi9Db21tYW5kJ1xuaW1wb3J0IEdhbWVwbGF5R3JpZERhdGEgZnJvbSAnLi4vLi4vbW9kZWwvZ2FtZXBsYXkvR2FtZXBsYXlHcmlkRGF0YSdcbmltcG9ydCBHYW1lcGxheUdyaWREaXNjRGF0YSBmcm9tICcuLi8uLi9tb2RlbC9nYW1lcGxheS9HYW1lcGxheUdyaWREaXNjRGF0YSdcbmltcG9ydCBHYW1lU2V0dGluZ3MgZnJvbSAnLi4vLi4vc2V0dGluZ3MvR2FtZVNldHRpbmdzJ1xuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBleHRlbmRzIENvbW1hbmQge1xuXG5cdGNvbnN0cnVjdG9yKCl7XG5cdFx0c3VwZXIoKTtcblx0XHRcblx0fVxuXG5cdGV4ZWN1dGUoZ2FtZSl7XG5cdFx0dmFyIG5ld0dyaWQgPSBuZXcgR2FtZXBsYXlHcmlkRGF0YSgpO1xuXHRcdGdhbWUubW9kZWwuZ3JpZERhdGEgPSBuZXdHcmlkO1xuXG5cdFx0Zm9yICh2YXIgaSA9IDA7IGkgPCBHYW1lU2V0dGluZ3MuU1RBUlRJTkdfRElTQ1M7IGkrKykge1xuXHRcdFx0dmFyIHJhbmRvbURpc2MgPSB0aGlzLmNyZWF0ZVJhbmRvbURpc2MobmV3R3JpZCk7XG5cdFx0XHRnYW1lLmNvbW1hbmRlci5leGVjdXRlKFwiR2FtZXBsYXlDb21tYW5kLkNyZWF0ZURpc2NTcHJpdGVcIiwgZ2FtZSwgcmFuZG9tRGlzYywgdHJ1ZSk7XG5cdFx0fVxuXG5cdFx0cmV0dXJuIHRydWU7XG5cdH1cblxuXHRjcmVhdGVSYW5kb21EaXNjKGdyaWQpe1xuXHRcdHZhciBtaW4gPSBHYW1lU2V0dGluZ3MuTUlOX0RJU0NfVkFMVUU7XG5cdFx0dmFyIG1heCA9IEdhbWVTZXR0aW5ncy5NQVhfRElTQ19WQUxVRTtcblxuXHRcdHZhciBkaXNjVmFsdWUgPSBNYXRoLmZsb29yKE1hdGgucmFuZG9tKCkgKiAobWF4IC0gbWluICsgMSkpICsgbWluO1xuXG5cdFx0Ly8gUHJldmVudCBncmlkIGZyb20gb3ZlcmZsb3dpbmcgb24gZmlyc3QgdHVyblxuXHRcdHZhciBsb3dlc3RSb3cgPSBHYW1lU2V0dGluZ3MuVFJVRV9HUklEX0hFSUdIVCAtIEdhbWVTZXR0aW5ncy5TVEFSVElOR19DT0xVTU5fSEVJR0hUX0xJTUlUO1xuXG5cdFx0dmFyIGNvbHVtblRvRHJvcEluID0gdGhpcy5nZXRSYW5kb21Db2x1bW4oKTtcblx0XHR2YXIgbG93ZXN0U2xvdEluQ29sdW1uID0gZ3JpZC5maXJzdEZyZWVSb3coY29sdW1uVG9Ecm9wSW4pO1xuXHRcdFxuXHRcdHdoaWxlKGxvd2VzdFNsb3RJbkNvbHVtbiA8IGxvd2VzdFJvdyl7XG5cdFx0XHRjb2x1bW5Ub0Ryb3BJbiA9IHRoaXMuZ2V0UmFuZG9tQ29sdW1uKCk7XG5cdFx0XHRsb3dlc3RTbG90SW5Db2x1bW4gPSBncmlkLmZpcnN0RnJlZVJvdyhjb2x1bW5Ub0Ryb3BJbilcblx0XHR9XG5cblx0XHR2YXIgYXJtb3VyVmFsdWUgPSAoTWF0aC5yYW5kb20oKSA8ICBHYW1lU2V0dGluZ3MuQ0hBTkNFX09GX0FSTU9VUl9ESVNDKT8gR2FtZVNldHRpbmdzLkRFRkFVTFRfQVJNT1VSX1ZBTFVFIDogMDtcblxuXHRcdHZhciBkaXNjRGF0YSA9IG5ldyBHYW1lcGxheUdyaWREaXNjRGF0YShkaXNjVmFsdWUsIGFybW91clZhbHVlKVxuXHRcdGdyaWQuZHJvcEluQ29sdW1uKGRpc2NEYXRhLCBjb2x1bW5Ub0Ryb3BJbik7XG5cblx0XHRyZXR1cm4gZGlzY0RhdGE7XG5cblx0fVxuXG5cdGdldFJhbmRvbUNvbHVtbigpe1xuXHRcdHJldHVybiBNYXRoLmZsb29yKE1hdGgucmFuZG9tKCkgKiBHYW1lU2V0dGluZ3MuR1JJRF9XSURUSCk7XG5cdH1cblxuXHRzdGF0aWMgZ2V0IE5BTUUoKSB7XG5cdFx0cmV0dXJuIFwiR2FtZXBsYXlDb21tYW5kLkdlbmVyYXRlR3JpZERhdGFcIjtcblx0fVxufVxuXG4iLCJpbXBvcnQgQ29tbWFuZCBmcm9tICcuLi9Db21tYW5kJ1xuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBleHRlbmRzIENvbW1hbmQge1xuXG5cdGNvbnN0cnVjdG9yKCl7XG5cdFx0c3VwZXIoKTtcblx0fVxuXG5cdGV4ZWN1dGUoZ2FtZSl7XG5cdFx0dmFyIHNjb3JlID0gZ2FtZS5tb2RlbC5nYW1lcGxheS5zY29yZTtcblx0XHRnYW1lLm1vZGVsLnBsYXllci5jb21wYXJlU2NvcmUoc2NvcmUpO1xuXHRcdGdhbWUuc3RhdGUuc3RhcnQoXCJNYWluTWVudVwiKTtcblxuXHRcdHJldHVybiB0cnVlO1xuXHR9XG5cblx0c3RhdGljIGdldCBOQU1FKCkge1xuXHRcdHJldHVybiBcIkdhbWVwbGF5Q29tbWFuZC5IYW5kbGVHYW1lT3ZlclwiO1xuXHR9XG59XG5cbiIsImltcG9ydCBDb21tYW5kIGZyb20gJy4uL0NvbW1hbmQnXG5cblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgZXh0ZW5kcyBDb21tYW5kIHtcblxuXHRjb25zdHJ1Y3Rvcigpe1xuXHRcdHN1cGVyKCk7XG5cdH1cblxuXHRleGVjdXRlKGdhbWUpe1xuXHRcdFxuXHRcdGdhbWUubW9kZWwuZ2FtZXBsYXkucmVzZXRSb3VuZCgpO1xuXHRcdGdhbWUuY29tbWFuZGVyLmV4ZWN1dGUoXCJBcHBDb21tYW5kLlBsYXlTb3VuZFwiLCBcIm5ld19yb3dcIik7XG5cdFx0Z2FtZS5jb21tYW5kZXIuZXhlY3V0ZShcIkdhbWVwbGF5Q29tbWFuZC5BZGRCb3R0b21Sb3dEaXNjc1wiLCBnYW1lKTtcblxuXHRcdHJldHVybiB0cnVlO1xuXHR9XG5cblx0c3RhdGljIGdldCBOQU1FKCkge1xuXHRcdHJldHVybiBcIkdhbWVwbGF5Q29tbWFuZC5IYW5kbGVUdXJuRW5kXCI7XG5cdH1cbn1cblxuIiwiaW1wb3J0IENvbW1hbmQgZnJvbSAnLi4vQ29tbWFuZCdcbmltcG9ydCBDcmVhdGVEaXNjU3ByaXRlQ29tbWFuZCBmcm9tICcuL0NyZWF0ZURpc2NTcHJpdGVDb21tYW5kJ1xuaW1wb3J0IENyZWF0ZVBvaW50c1Nwcml0ZUNvbW1hbmQgZnJvbSAnLi9DcmVhdGVQb2ludHNTcHJpdGVDb21tYW5kJ1xuaW1wb3J0IEFuaW1hdGVEaXNjc0NvbW1hbmQgZnJvbSAnLi9BbmltYXRlRGlzY3NDb21tYW5kJ1xuaW1wb3J0IENyZWF0ZU5leHRHcmlkRGlzY0NvbW1hbmQgZnJvbSAnLi9DcmVhdGVOZXh0R3JpZERpc2NDb21tYW5kJ1xuaW1wb3J0IEFkZEJvdHRvbVJvd0Rpc2NzQ29tbWFuZCBmcm9tICcuL0FkZEJvdHRvbVJvd0Rpc2NzQ29tbWFuZCdcbmltcG9ydCBHYW1lcGxheURhdGEgZnJvbSAnLi4vLi4vbW9kZWwvZ2FtZXBsYXkvR2FtZXBsYXlEYXRhJ1xuaW1wb3J0IENoZWNrRm9yR2FtZU92ZXJDb21tYW5kIGZyb20gJy4vQ2hlY2tGb3JHYW1lT3ZlckNvbW1hbmQnXG5cblxuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBleHRlbmRzIENvbW1hbmQge1xuXG5cdGNvbnN0cnVjdG9yKCl7XG5cdFx0c3VwZXIoKTtcblxuXHR9XG5cblx0ZXhlY3V0ZShnYW1lLCBnYW1lcGxheVN0YXRlKXtcblx0XHRnYW1lLmNvbW1hbmRlci5yZWdpc3RlckNvbW1hbmQoQ3JlYXRlRGlzY1Nwcml0ZUNvbW1hbmQuTkFNRSwgbmV3IENyZWF0ZURpc2NTcHJpdGVDb21tYW5kKGdhbWVwbGF5U3RhdGUpLCB0cnVlKTtcblx0XHRnYW1lLmNvbW1hbmRlci5yZWdpc3RlckNvbW1hbmQoQW5pbWF0ZURpc2NzQ29tbWFuZC5OQU1FLCBuZXcgQW5pbWF0ZURpc2NzQ29tbWFuZChnYW1lcGxheVN0YXRlKSwgdHJ1ZSk7XG5cdFx0Z2FtZS5jb21tYW5kZXIucmVnaXN0ZXJDb21tYW5kKENyZWF0ZU5leHRHcmlkRGlzY0NvbW1hbmQuTkFNRSwgbmV3IENyZWF0ZU5leHRHcmlkRGlzY0NvbW1hbmQoZ2FtZXBsYXlTdGF0ZSksIHRydWUpO1xuXHRcdGdhbWUuY29tbWFuZGVyLnJlZ2lzdGVyQ29tbWFuZChBZGRCb3R0b21Sb3dEaXNjc0NvbW1hbmQuTkFNRSwgbmV3IEFkZEJvdHRvbVJvd0Rpc2NzQ29tbWFuZChnYW1lcGxheVN0YXRlKSwgdHJ1ZSk7XG5cdFx0Z2FtZS5jb21tYW5kZXIucmVnaXN0ZXJDb21tYW5kKENoZWNrRm9yR2FtZU92ZXJDb21tYW5kLk5BTUUsIG5ldyBDaGVja0ZvckdhbWVPdmVyQ29tbWFuZChnYW1lcGxheVN0YXRlKSwgdHJ1ZSk7XG5cdFx0Z2FtZS5jb21tYW5kZXIucmVnaXN0ZXJDb21tYW5kKENyZWF0ZVBvaW50c1Nwcml0ZUNvbW1hbmQuTkFNRSwgbmV3IENyZWF0ZVBvaW50c1Nwcml0ZUNvbW1hbmQoZ2FtZXBsYXlTdGF0ZSksIHRydWUpO1xuXHRcdFxuXHRcdGdhbWUubW9kZWwuZ2FtZXBsYXkgPSBuZXcgR2FtZXBsYXlEYXRhKCk7XG5cdFx0Z2FtZS5jb21tYW5kZXIuZXhlY3V0ZShcIkdhbWVwbGF5Q29tbWFuZC5HZW5lcmF0ZUdyaWREYXRhXCIsIGdhbWUpO1xuXHRcdGdhbWUuY29tbWFuZGVyLmV4ZWN1dGUoXCJHYW1lcGxheUNvbW1hbmQuRXZhbHVhdGVHcmlkTWF0Y2hlc1wiLCBnYW1lKTtcblxuXHRcdHJldHVybiB0cnVlO1xuXHR9XG5cblx0c3RhdGljIGdldCBOQU1FKCkge1xuXHRcdHJldHVybiBcIkdhbWVwbGF5Q29tbWFuZC5Jbml0aWFsaXNlR2FtZXBsYXlcIjtcblx0fVxufVxuXG4iLCJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIHtcblx0Y29uc3RydWN0b3IoKXtcblx0XHR0aGlzLmhpZ2hzY29yZSA9IDA7XG5cdH1cblxuXHRjb21wYXJlU2NvcmUobmV3U2NvcmUpe1xuXHRcdGlmKG5ld1Njb3JlID4gdGhpcy5oaWdoc2NvcmUpe1xuXHRcdFx0dGhpcy5oaWdoc2NvcmUgPSBuZXdTY29yZTtcblx0XHR9XG5cdH1cbn0iLCJpbXBvcnQgR2FtZVNldHRpbmdzIGZyb20gJy4uLy4uL3NldHRpbmdzL0dhbWVTZXR0aW5ncydcblxuZXhwb3J0IGRlZmF1bHQgY2xhc3Mge1xuXHRjb25zdHJ1Y3Rvcigpe1xuXHRcdHRoaXMuc2NvcmUgPSAwO1xuXHRcdHRoaXMuY29tYm8gPSAwO1xuXHRcdHRoaXMudHVybnNVbnRpbE5leHRSb3VuZCA9IEdhbWVTZXR0aW5ncy5UVVJOU19CRVRXRUVOX1JPVU5EUztcblx0fVxuXG5cdGluY3JlYXNlU2NvcmUoKXtcblx0XHR0aGlzLnNjb3JlICs9IEdhbWVTZXR0aW5ncy5CQVNFX0RJU0NfUE9JTlRTICsgKEdhbWVTZXR0aW5ncy5QT0lOVFNfTVVMVElQTElFUiAqIHRoaXMuY29tYm8pO1xuXHR9XG5cblx0aW5jcmVhc2VDb21ibygpe1xuXHRcdHRoaXMuY29tYm8rKztcblx0fVxuXG5cdHJlc2V0Q29tYm8oKXtcblx0XHR0aGlzLmNvbWJvID0gMDtcblx0fVxuXG5cdGdldENvbWJvVmFsdWUoKXtcblx0XHRyZXR1cm4gIEdhbWVTZXR0aW5ncy5CQVNFX0RJU0NfUE9JTlRTICsgKEdhbWVTZXR0aW5ncy5QT0lOVFNfTVVMVElQTElFUiAqIHRoaXMuY29tYm8pO1xuXHR9XG5cblx0bmV4dFR1cm4oKXtcblx0XHR0aGlzLnR1cm5zVW50aWxOZXh0Um91bmQtLTtcblx0fVxuXG5cdGlzUm91bmRDb21wbGV0ZSgpe1xuXHRcdHJldHVybiB0aGlzLnR1cm5zVW50aWxOZXh0Um91bmQgPT0gMDtcblx0fVxuXG5cdHJlc2V0Um91bmQoKXtcblx0XHR0aGlzLnR1cm5zVW50aWxOZXh0Um91bmQgPSBHYW1lU2V0dGluZ3MuVFVSTlNfQkVUV0VFTl9ST1VORFM7XG5cdH1cbn0iLCJpbXBvcnQgR2FtZVNldHRpbmdzIGZyb20gJy4uLy4uL3NldHRpbmdzL0dhbWVTZXR0aW5ncydcblxuZXhwb3J0IGRlZmF1bHQgY2xhc3Mge1xuXHRjb25zdHJ1Y3Rvcigpe1xuXHRcdHRoaXMuZGF0YSA9IFtdXG5cdH1cblxuXHR1cGRhdGVDb2x1bW4oeCl7XG5cdFx0Ly8gRXZhbHVhdGUgYm90dG9tIHRvIHRvcFxuXHRcdC8vIElHTk9SRSBCT1RUT00gUk9XLCBjYW4ndCBtb3ZlIGRvd24gYW55IGZ1cnRoZXJcblx0XHRmb3IgKHZhciB5ID0gR2FtZVNldHRpbmdzLlRSVUVfR1JJRF9IRUlHSFQgLSAyOyB5ID4gMDsgeS0tKSB7XG5cdFx0XHR2YXIgY3VycmVudERpc2MgPSB0aGlzLmdldERpc2NBdFBvc2l0aW9uKHgsIHkpO1xuXHRcdFx0dmFyIGRpc2NCZWxvdyA9IHRoaXMuZ2V0RGlzY0F0UG9zaXRpb24oeCwgeSsxKTtcblx0XHRcdFxuXHRcdFx0aWYoY3VycmVudERpc2MgIT0gbnVsbCAmJiBkaXNjQmVsb3cgPT0gbnVsbCl7IC8vIERyb3AgZGlzYyBkb3duXG5cdFx0XHRcdHZhciBuZXdZID0geTtcblx0XHRcdFx0d2hpbGUobmV3WSA8IEdhbWVTZXR0aW5ncy5HUklEX0hFSUdIVCAmJiBkaXNjQmVsb3cgPT0gbnVsbCl7IC8vIEZpbmQgbG93ZXN0IGZyZWUgc2xvdFxuXHRcdFx0XHRcdG5ld1kgKz0gMTtcblx0XHRcdFx0XHR2YXIgZGlzY0JlbG93ID0gdGhpcy5nZXREaXNjQXRQb3NpdGlvbih4LCBuZXdZKzEpO1xuXG5cdFx0XHRcdH1cblxuXHRcdFx0XHR0aGlzLm1vdmVEaXNjQXRQb3NpdGlvbih4LCB5LCB4LCBuZXdZKTtcblx0XHRcdH1cblx0XHR9XG5cdH1cblxuXHQvLyBGaW5kIG1hdGNoaW5nIGRpc2NzIHZlcnRpY2FsbHlcblx0Y2hlY2tDb2x1bW5zRm9yTWF0Y2hlcygpe1xuXHRcdHZhciBtYXRjaGluZ0NvbHVtbkRpc2NzID0gW107XG5cdFx0dmFyIGdyb3VwcyA9IFtdO1xuXG5cdFx0Zm9yICh2YXIgeCA9IDA7IHggPCBHYW1lU2V0dGluZ3MuR1JJRF9XSURUSDsgeCsrKSB7XG5cdFx0XHR2YXIgY3VycmVudEdyb3VwID0gW107XG5cdFx0XHR2YXIgeSA9IDE7XG5cblx0XHRcdHdoaWxlKHkgPCBHYW1lU2V0dGluZ3MuVFJVRV9HUklEX0hFSUdIVCl7XG5cdFx0XHRcdHZhciBjdXJyZW50R3JpZERpc2MgPSB0aGlzLmdldERpc2NBdFBvc2l0aW9uKHgsIHkpO1xuXHRcdFx0XHRpZihjdXJyZW50R3JpZERpc2MgIT0gbnVsbCl7XG5cdFx0XHRcdFx0Y3VycmVudEdyb3VwLnB1c2goY3VycmVudEdyaWREaXNjKTtcblx0XHRcdFx0fVxuXG5cdFx0XHRcdHkrKztcblx0XHRcdH1cblxuXHRcdFx0aWYoY3VycmVudEdyb3VwLmxlbmd0aCA+IDApe1xuXHRcdFx0XHRncm91cHMucHVzaChjdXJyZW50R3JvdXApO1xuXHRcdFx0fVxuXHRcdH1cblxuXHRcdHJldHVybiB0aGlzLmNoZWNrTWF0Y2hlc0luR3JvdXBzKGdyb3Vwcyk7XG5cdH1cblxuXHQvLyBGaW5kIG1hdGNoaW5nIGRpc2NzIGhvcml6b250YWxseVxuXHRjaGVja1Jvd3NGb3JNYXRjaGVzKCl7XG5cdFx0dmFyIGdyb3VwcyA9IFtdO1xuXHRcdC8vQ3JlYXRlIGdyb3VwcyBvZiBjb25uZWN0ZWQgZGlzY3MgSUdOT1JFIFRPUCBST1dcblx0XHRmb3IgKHZhciB5ID0gMTsgeSA8IEdhbWVTZXR0aW5ncy5UUlVFX0dSSURfSEVJR0hUOyB5KyspIHtcblx0XHRcdFxuXHRcdFx0dmFyIGN1cnJlbnRHcm91cCA9IFtdO1xuXHRcdFx0dmFyIHggPSAwO1xuXG5cdFx0XHR3aGlsZSh4IDwgR2FtZVNldHRpbmdzLkdSSURfV0lEVEgpe1xuXHRcdFx0XHR2YXIgY3VycmVudEdyaWREaXNjID0gdGhpcy5nZXREaXNjQXRQb3NpdGlvbih4LCB5KTtcblxuXHRcdFx0XHRpZihjdXJyZW50R3JpZERpc2MgPT0gbnVsbCl7IC8vIFRoZXJlIGlzIGEgYnJlYWsgaW4gY29ubmVjdGVkIGRpc2NzXG5cdFx0XHRcdFx0aWYoY3VycmVudEdyb3VwLmxlbmd0aCA+IDApey8vIFByZXZpb3VzIGdyb3VwIGhhZCBkaXNjcyAtIGFkZCB0byBsaXN0XG5cdFx0XHRcdFx0XHRncm91cHMucHVzaChjdXJyZW50R3JvdXApO1xuXHRcdFx0XHRcdFx0Y3VycmVudEdyb3VwID0gW107IC8vIENyZWF0ZSBuZXcgZ3JvdXBcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH1lbHNle1xuXHRcdFx0XHRcdGN1cnJlbnRHcm91cC5wdXNoKGN1cnJlbnRHcmlkRGlzYyk7XG5cdFx0XHRcdH1cblx0XHRcdFx0eCsrO1xuXHRcdFx0fVxuXG5cdFx0XHRpZihjdXJyZW50R3JvdXAubGVuZ3RoID4gMCl7XG5cdFx0XHRcdGdyb3Vwcy5wdXNoKGN1cnJlbnRHcm91cCk7XG5cdFx0XHR9XG5cdFx0fVxuXG5cdFx0cmV0dXJuIHRoaXMuY2hlY2tNYXRjaGVzSW5Hcm91cHMoZ3JvdXBzKTtcblx0fVxuXG5cdGNoZWNrTWF0Y2hlc0luR3JvdXBzKGdyb3Vwcyl7XG5cdFx0dmFyIG1hdGNoaW5nUm93RGlzY3MgPSBbXTtcblx0XHRmb3IgKHZhciBpID0gMDsgaSA8IGdyb3Vwcy5sZW5ndGg7IGkrKykge1xuXHRcdFx0dmFyIGNvbm5lY3RlZEdyb3VwID0gZ3JvdXBzW2ldO1xuXHRcdFx0dmFyIG1hdGNoVmFsdWUgPSBjb25uZWN0ZWRHcm91cC5sZW5ndGg7XG5cblx0XHRcdGZvciAodmFyIGogPSAwOyBqIDwgY29ubmVjdGVkR3JvdXAubGVuZ3RoOyBqKyspIHtcblx0XHRcdFx0dmFyIGRpc2MgPSBjb25uZWN0ZWRHcm91cFtqXTtcblx0XHRcdFx0aWYobWF0Y2hWYWx1ZSA9PSBkaXNjLmRpc2NWYWx1ZSAmJiBkaXNjLmFybW91ciA8PSAwKXtcblx0XHRcdFx0XHRtYXRjaGluZ1Jvd0Rpc2NzLnB1c2goZGlzYyk7XG5cdFx0XHRcdH1cblx0XHRcdH1cblx0XHR9XG5cblx0XHRyZXR1cm4gbWF0Y2hpbmdSb3dEaXNjcztcblx0fVxuXG5cdGRyb3BJbkNvbHVtbihncmlkRGlzY0RhdGEsIHgpe1xuXHRcdHZhciBmcmVlUm93ID0gdGhpcy5maXJzdEZyZWVSb3coeCk7XG5cdFx0aWYoZnJlZVJvdyA9PSAwKXtcblx0XHRcdC8vUm93IGlzIGZ1bGxcblx0XHRcdHJldHVybiBmYWxzZTtcblx0XHR9ZWxzZXtcblx0XHRcdHRoaXMuYWRkRGlzY0F0UG9zaXRpb24oZ3JpZERpc2NEYXRhLCB4LCBmcmVlUm93KVxuXHRcdFx0cmV0dXJuIHRydWU7XG5cdFx0fVxuXHR9XG5cblx0Zmlyc3RGcmVlUm93KHgpe1xuXHRcdHZhciBmaXJzdEZyZWVSb3cgPSAwO1xuXHRcdGZvciAodmFyIHkgPSBHYW1lU2V0dGluZ3MuVFJVRV9HUklEX0hFSUdIVCAtIDE7IHkgPj0gMDsgeS0tKSB7XG5cdFx0XHRpZih0aGlzLmdldERpc2NBdFBvc2l0aW9uKHgsIHkpID09IG51bGwpe1xuXHRcdFx0XHRcblx0XHRcdFx0Zmlyc3RGcmVlUm93ID0geTtcblx0XHRcdFx0YnJlYWs7XG5cdFx0XHR9XG5cdFx0fVxuXHRcdHJldHVybiBmaXJzdEZyZWVSb3c7XG5cdH1cblxuXHRhZGREaXNjQXRQb3NpdGlvbihncmlkRGlzY0RhdGEsIHgsIHkpe1xuXHRcdGdyaWREaXNjRGF0YS54ID0geDtcblx0XHRncmlkRGlzY0RhdGEueSA9IHk7XG5cdFx0dGhpcy5kYXRhW3ggKyB5KkdhbWVTZXR0aW5ncy5HUklEX1dJRFRIXSA9IGdyaWREaXNjRGF0YTtcblx0fVxuXG5cdG1vdmVEaXNjQXRQb3NpdGlvbih4LCB5LCBuZXdYLCBuZXdZKXtcblx0XHR2YXIgZ3JpZERpc2NEYXRhID0gdGhpcy5nZXREaXNjQXRQb3NpdGlvbih4LCB5KTtcblx0XHR0aGlzLnJlbW92ZURpc2NBdFBvc2l0aW9uKHgsIHkpO1xuXHRcdHRoaXMuYWRkRGlzY0F0UG9zaXRpb24oZ3JpZERpc2NEYXRhLCBuZXdYLCBuZXdZKTtcblx0fVxuXG5cdHJlbW92ZURpc2NBdFBvc2l0aW9uKHgsIHkpe1xuXHRcdHRoaXMuZGF0YVt4ICsgeSpHYW1lU2V0dGluZ3MuR1JJRF9XSURUSF0gPSBudWxsO1xuXHR9XG5cblx0Z2V0RGlzY0F0UG9zaXRpb24oeCwgeSl7XG5cdFx0aWYoIXRoaXMuaW5Cb3VuZHMoeCwgeSkpe1xuXHRcdFx0cmV0dXJuIG51bGw7XG5cdFx0fVxuXHRcdHJldHVybiB0aGlzLmRhdGFbeCArIHkqR2FtZVNldHRpbmdzLkdSSURfV0lEVEhdO1xuXHR9XG5cblx0Ly8gQ2hlY2sgY29vcmRpbmF0ZXMgYXJlIHdpdGhpbiB0aGUgZ3JpZFxuXHRpbkJvdW5kcyh4LCB5KXtcblx0XHRpZih4IDwgMCB8fCB4ID49IEdhbWVTZXR0aW5ncy5HUklEX1dJRFRIIHx8XG5cdFx0XHR5IDwgMSB8fCB5ID49IEdhbWVTZXR0aW5ncy5UUlVFX0dSSURfSEVJR0hUKXtcblx0XHRcdHJldHVybiBmYWxzZTtcblx0XHR9ZWxzZXtcblx0XHRcdHJldHVybiB0cnVlO1xuXHRcdH1cblx0fVxuXG5cdC8vIE1vdmUgYWxsIGdyaWQgaXRlbXMgdXAgb25lIChlbmQgb2Ygcm91bmQpXG5cdHNoaWZ0VXBBbGwoKXtcblx0XHRmb3IgKHZhciBpID0gMDsgaSA8IHRoaXMuZGF0YS5sZW5ndGg7IGkrKykge1xuXHRcdFx0aWYodGhpcy5kYXRhW2ldICE9IG51bGwpe1xuXHRcdFx0XHR0aGlzLm1vdmVEaXNjQXRQb3NpdGlvbih0aGlzLmRhdGFbaV0ueCwgdGhpcy5kYXRhW2ldLnksIHRoaXMuZGF0YVtpXS54LCB0aGlzLmRhdGFbaV0ueS0xKTtcblx0XHRcdH1cblx0XHR9XG5cdH1cblxuXHQvL1ByaW50IGdyaWQgYXJyYXkgdG8gY29uc29sZSAoZGVidWdnaW5nKVxuXHRwcmludEdyaWRUb0NvbnNvbGUoKXtcblx0XHRmb3IgKHZhciB5ID0gMDsgeSA8IEdhbWVTZXR0aW5ncy5UUlVFX0dSSURfSEVJR0hUOyB5KyspIHtcblx0XHRcdHZhciByb3dTdHJpbmcgPSBcIlwiXG5cblx0XHRcdGZvciAodmFyIHggPSAwOyB4IDwgR2FtZVNldHRpbmdzLkdSSURfV0lEVEg7IHgrKykge1xuXHRcdFx0XHRpZih5ID09IDApe1xuXHRcdFx0XHRcdHJvd1N0cmluZyArPSBcIi0gXCI7XG5cdFx0XHRcdH1lbHNle1xuXHRcdFx0XHRcdHZhciBkaXNjRGF0YSA9IHRoaXMuZ2V0RGlzY0F0UG9zaXRpb24oeCwgeSlcblx0XHRcdFx0XHRpZihkaXNjRGF0YSAhPSBudWxsKXtcblx0XHRcdFx0XHRcdHJvd1N0cmluZyArPSBkaXNjRGF0YS5kaXNjVmFsdWUgKyBcIiBcIjtcblx0XHRcdFx0XHR9ZWxzZXtcblx0XHRcdFx0XHRcdHJvd1N0cmluZyArPSBcInggXCI7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9XG5cdFx0XHR9XG5cblx0XHRcdGNvbnNvbGUubG9nKHJvd1N0cmluZyk7XG5cdFx0fVxuXHR9XG59IiwiZXhwb3J0IGRlZmF1bHQgY2xhc3Mge1xuXHRjb25zdHJ1Y3Rvcih2YWx1ZSwgYXJtb3VyID0gMCl7XG5cdFx0dGhpcy5kaXNjVmFsdWUgPSB2YWx1ZTtcblx0XHR0aGlzLl9hcm1vdXIgPSBhcm1vdXI7XG5cdFx0dGhpcy5feCA9IDA7XG5cdFx0dGhpcy5feSA9IDA7XG5cdFx0dGhpcy5fZGVzdHJveWVkID0gZmFsc2U7XG5cblx0XHR0aGlzLm9uQ2hhbmdlZCA9IG5ldyBQaGFzZXIuU2lnbmFsKCk7IC8vIFRyaWdnZXJlZCB3aGVuIHBvc2l0aW9uIGNoYW5nZXNcblx0XHR0aGlzLm9uQXJtb3VyQ2hhbmdlZCA9IG5ldyBQaGFzZXIuU2lnbmFsKCk7IC8vIFRyaWdnZXJlZCB3aGVuIGFybW91ciBjaGFuZ2VzXG5cdH1cblxuXHRnZXQgeCgpIHtcblx0XHRyZXR1cm4gdGhpcy5feDtcblx0fVxuXG5cdGdldCB5KCkge1xuXHRcdHJldHVybiB0aGlzLl95O1xuXHR9XG5cblx0Z2V0IGRlc3Ryb3llZCgpIHtcblx0XHRyZXR1cm4gdGhpcy5fZGVzdHJveWVkO1xuXHR9XG5cblx0Z2V0IGFybW91cigpIHtcblx0XHRyZXR1cm4gdGhpcy5fYXJtb3VyO1xuXHR9XG5cblx0c2V0IHgodmFsdWUpIHtcblx0XHR0aGlzLl94ID0gdmFsdWU7XG5cdH1cblxuXHRzZXQgeSh2YWx1ZSkge1xuXHRcdHRoaXMuX3kgPSB2YWx1ZTtcblx0XHR0aGlzLm9uQ2hhbmdlZC5kaXNwYXRjaCgpXG5cdH1cblxuXHRzZXQgZGVzdHJveWVkKHZhbHVlKSB7XG5cdFx0dGhpcy5fZGVzdHJveWVkID0gdmFsdWU7XG5cblx0XHRpZih0aGlzLl9kZXN0cm95ZSl7XG5cdFx0XHR0aGlzLnJlbW92ZUFsbCgpXG5cdFx0fVxuXHR9XG5cblx0c2V0IGFybW91cih2YWx1ZSkge1xuXHRcdHRoaXMuX2FybW91ciA9IHZhbHVlO1xuXG5cdFx0aWYodGhpcy5fYXJtb3VyIDwgMCl7XG5cdFx0XHR0aGlzLl9hcm1vdXIgPSAwO1xuXHRcdH1lbHNle1xuXHRcdFx0dGhpcy5vbkFybW91ckNoYW5nZWQuZGlzcGF0Y2goKVxuXHRcdH1cblx0fVxufSIsImV4cG9ydCBkZWZhdWx0IGNsYXNze1xuXG5cblx0c3RhdGljIGdldCBHUklEX1dJRFRIKCkge1xuXHRcdHJldHVybiA3O1xuXHR9XG5cblx0c3RhdGljIGdldCBHUklEX0hFSUdIVCgpIHtcblx0XHRyZXR1cm4gNztcblx0fVxuXG5cdC8vIFRoZSB0cnVlIGdyaWQgaGVpZ2h0IGlzIG9uZSByb3cgdGFsbGVyIHRoYW4gdGhlIGRpc3BsYXkgaGVpZ2h0LiBJZiB0aGVyZSBpcyBcblx0Ly8gYSBkaXNjIGluIHRoZSB0b3Agcm93LCB0aGUgZ3JpZCBoYXMgb3ZlcmZsb3dlZCBhbmQgdGhlIGdhbWUgaXMgb3ZlclxuXHRzdGF0aWMgZ2V0IFRSVUVfR1JJRF9IRUlHSFQoKSB7XG5cdFx0cmV0dXJuIDg7XG5cdH1cblxuXHRzdGF0aWMgZ2V0IEdSSURfVElMRV9TSVpFKCkge1xuXHRcdHJldHVybiA4MDtcblx0fVxuXG5cdHN0YXRpYyBnZXQgR1JJRF9USUxFX1NJWkUoKSB7XG5cdFx0cmV0dXJuIDgwO1xuXHR9XG5cblx0c3RhdGljIGdldCBTVEFSVElOR19ESVNDUygpIHtcblx0XHRyZXR1cm4gMTI7XG5cdH1cblxuXHRzdGF0aWMgZ2V0IFNUQVJUSU5HX0NPTFVNTl9IRUlHSFRfTElNSVQoKSB7XG5cdFx0cmV0dXJuIDQ7XG5cdH1cblxuXG5cdHN0YXRpYyBnZXQgTUlOX0RJU0NfVkFMVUUoKSB7XG5cdFx0cmV0dXJuIDE7XG5cdH1cblxuXHRzdGF0aWMgZ2V0IE1BWF9ESVNDX1ZBTFVFKCkge1xuXHRcdHJldHVybiA3O1xuXHR9XG5cblx0c3RhdGljIGdldCBCQVNFX0RJU0NfUE9JTlRTKCkge1xuXHRcdHJldHVybiA1O1xuXHR9XG5cblx0c3RhdGljIGdldCBQT0lOVFNfTVVMVElQTElFUigpIHtcblx0XHRyZXR1cm4gMjM7XG5cdH1cblxuXHRzdGF0aWMgZ2V0IENIQU5DRV9PRl9BUk1PVVJfRElTQygpIHtcblx0XHRyZXR1cm4gMC4xO1xuXHR9XG5cblx0c3RhdGljIGdldCBERUZBVUxUX0FSTU9VUl9WQUxVRSgpIHtcblx0XHRyZXR1cm4gMjtcblx0fVxuXG5cdHN0YXRpYyBnZXQgVFVSTlNfQkVUV0VFTl9ST1VORFMoKSB7XG5cdFx0cmV0dXJuIDEyO1xuXHR9XG59IiwiZXhwb3J0IGRlZmF1bHQgY2xhc3MgZXh0ZW5kcyBQaGFzZXIuU3RhdGV7XG5cblx0XG5cblx0cHJlbG9hZCgpe1xuXHRcdC8vVE9ETzogcmVzdG9yZSBtb2RlbCBmcm9tIHNhdmUgZGF0YVxuXHRcdHRoaXMuZ2FtZS5tb2RlbCA9IHt9XG5cblx0XHR3aW5kb3cuV2ViRm9udENvbmZpZyA9IHtcblx0XHRcdC8vICBUaGUgR29vZ2xlIEZvbnRzIHdlIHdhbnQgdG8gbG9hZCAoc3BlY2lmeSBhcyBtYW55IGFzIHlvdSBsaWtlIGluIHRoZSBhcnJheSlcblx0XHRcdGdvb2dsZToge1xuXHRcdFx0ICBmYW1pbGllczogWydQZXJtYW5lbnQgTWFya2VyJ11cblx0XHRcdH1cblxuXHRcdH07XG5cblx0XHR0aGlzLmdhbWUubG9hZC5hdWRpbygnYnJlYWtfZGlzYycsIFsnYXNzZXRzL2F1ZGlvL2JyZWFrX2Rpc2MubTRhJywgJ2Fzc2V0cy9hdWRpby9icmVha19kaXNjLm9nZyddKTtcblx0XHR0aGlzLmdhbWUubG9hZC5hdWRpbygnZHJvcF9kaXNjJywgWydhc3NldHMvYXVkaW8vZHJvcF9kaXNjLm00YScsICdhc3NldHMvYXVkaW8vZHJvcF9kaXNjLm9nZyddKTtcblx0XHR0aGlzLmdhbWUubG9hZC5hdWRpbygnZ2FtZV9vdmVyJywgWydhc3NldHMvYXVkaW8vZ2FtZV9vdmVyLm00YScsICdhc3NldHMvYXVkaW8vZ2FtZV9vdmVyLm9nZyddKTtcblx0XHR0aGlzLmdhbWUubG9hZC5hdWRpbygnbmV3X2dhbWUnLCBbJ2Fzc2V0cy9hdWRpby9uZXdfZ2FtZS5tNGEnLCAnYXNzZXRzL2F1ZGlvL25ld19nYW1lLm9nZyddKTtcblx0XHR0aGlzLmdhbWUubG9hZC5hdWRpbygnbmV3X3JvdycsIFsnYXNzZXRzL2F1ZGlvL2JyZWFrX2Rpc2MubTRhJywgJ2Fzc2V0cy9hdWRpby9uZXdfcm93Lm9nZyddKTtcblx0XHR0aGlzLmdhbWUubG9hZC5hdWRpbygnd2VsY29tZScsIFsnYXNzZXRzL2F1ZGlvL3dlbGNvbWUubTRhJywgJ2Fzc2V0cy9hdWRpby93ZWxjb21lLm9nZyddKTtcblx0XHR0aGlzLmdhbWUubG9hZC5hdWRpbygnY2FuY2VsJywgWydhc3NldHMvYXVkaW8vYnJlYWtfZGlzYy5tNGEnLCAnYXNzZXRzL2F1ZGlvL2NhbmNlbC5vZ2cnXSk7XG5cblx0XHR0aGlzLmdhbWUubG9hZC5zY3JpcHQoJ3dlYmZvbnQnLCAnLy9hamF4Lmdvb2dsZWFwaXMuY29tL2FqYXgvbGlicy93ZWJmb250LzEuNC43L3dlYmZvbnQuanMnKTtcblxuXHRcdFxuXHR9XG5cblx0Y3JlYXRlKCl7XG5cdFx0XG5cdFx0dGhpcy5nYW1lLmNvbW1hbmRlci5leGVjdXRlKFwiQXBwQ29tbWFuZC5Cb290c3RyYXBcIiwgdGhpcy5nYW1lKTtcblx0XHR0aGlzLmdhbWUuc3RhZ2UuYmFja2dyb3VuZENvbG9yID0gXCIjMDFjZGZlXCI7XG5cdFx0dGhpcy5nYW1lLnN0YXRlLnN0YXJ0KFwiU3BsYXNoXCIpO1xuXHR9XG5cbn1cbiIsImV4cG9ydCBkZWZhdWx0IGNsYXNzIGV4dGVuZHMgUGhhc2VyLlN0YXRle1xuXG5cdHByZWxvYWQoKXtcblx0XHR0aGlzLmdhbWUubG9hZC5pbWFnZSgnZHJvcDctbG9nby1pbWFnZScsICdhc3NldHMvaW1hZ2VzL21haW5NZW51L2Ryb3A3LWxvZ28ucG5nJyk7XG5cdFx0dGhpcy5nYW1lLmxvYWQuc3ByaXRlc2hlZXQoJ21haW4tbWVudS1idXR0b24nLCAnYXNzZXRzL2ltYWdlcy9tYWluTWVudS9tYWluLW1lbnUtYnV0dG9uLnBuZycsIDIzNiwgODApO1xuXHR9XG5cblx0Y3JlYXRlKCl7XG5cdFx0dGhpcy5nYW1lLnN0YWdlLmJhY2tncm91bmRDb2xvciA9IFwiI2ZmZmZmZlwiO1xuXHRcdHZhciBsb2dvSW1hZ2UgPSB0aGlzLmdhbWUuYWRkLnNwcml0ZSh0aGlzLmdhbWUud29ybGQuY2VudGVyWCwgMTAwLCAnZHJvcDctbG9nby1pbWFnZScpO1xuXHRcdGxvZ29JbWFnZS5hbmNob3Iuc2V0VG8oMC41LCAwKTtcblxuXHRcdHZhciBzdHlsZTEgPSB7IGZvbnQ6IFwiNDBweCBQZXJtYW5lbnQgTWFya2VyXCIsIGZpbGw6IFwiI2IwNWRmZlwiLCBhbGlnbjogXCJjZW50ZXJcIiB9O1xuXHRcdHRoaXMuaGlnaHNjb3JlVGV4dGZpZWxkID0gdGhpcy5nYW1lLmFkZC50ZXh0KDAsIDAsXCJIaWdoIHNjb3JlOiBcIiArIHRoaXMuZ2FtZS5tb2RlbC5wbGF5ZXIuaGlnaHNjb3JlLCBzdHlsZTEpO1xuXHRcdHRoaXMuaGlnaHNjb3JlVGV4dGZpZWxkLmFuY2hvci5zZXRUbygwLjUsIDApO1xuXHRcdHRoaXMuaGlnaHNjb3JlVGV4dGZpZWxkLnggPSB0aGlzLmdhbWUud29ybGQuY2VudGVyWDtcblx0XHR0aGlzLmhpZ2hzY29yZVRleHRmaWVsZC55ID0gNDI1O1xuXG5cdFx0dGhpcy5idXR0b24gPSB0aGlzLmdhbWUuYWRkLmJ1dHRvbih0aGlzLmdhbWUud29ybGQuY2VudGVyWCwgNTUwLCAnbWFpbi1tZW51LWJ1dHRvbicsIHRoaXMuaGFuZGxlQ2xpY2ssIHRoaXMsIDEsIDAsIDApO1xuXHRcdHRoaXMuYnV0dG9uLmFuY2hvci5zZXRUbygwLjUsIDAuNSk7XG5cblx0XHR2YXIgc3R5bGUyID0geyBmb250OiBcIjUwcHggUGVybWFuZW50IE1hcmtlclwiLCBmaWxsOiBcIiNGRkZGRkZcIiwgYWxpZ246IFwiY2VudGVyXCIgfTtcblx0XHR2YXIgbGFiZWwgPSB0aGlzLmdhbWUuYWRkLnRleHQoMCwgMCwgXCJwbGF5XCIsIHN0eWxlMik7XG5cdFx0dGhpcy5idXR0b24uYWRkQ2hpbGQobGFiZWwpO1xuXHRcdGxhYmVsLmFuY2hvci5zZXQoMC41KTtcblxuXHRcdHRoaXMuZ2FtZS5jb21tYW5kZXIuZXhlY3V0ZShcIkFwcENvbW1hbmQuUGxheVNvdW5kXCIsIFwid2VsY29tZVwiKTtcblx0fVxuXG5cdGhhbmRsZUNsaWNrKCl7XG5cdFx0Lyp0aGlzLmdhbWUuc2NhbGUuZnVsbFNjcmVlblNjYWxlTW9kZSA9IFBoYXNlci5TY2FsZU1hbmFnZXIuU0hPV19BTEw7XG5cdFx0dGhpcy5nYW1lLnNjYWxlLnN0YXJ0RnVsbFNjcmVlbihmYWxzZSk7Ki9cblx0XHR0aGlzLmdhbWUuc3RhdGUuc3RhcnQoXCJHYW1lcGxheVwiKTtcblx0fVxuXG59XG4iLCJleHBvcnQgZGVmYXVsdCBjbGFzcyBleHRlbmRzIFBoYXNlci5TdGF0ZXtcblxuXHRwcmVsb2FkKCl7XG5cdFx0dGhpcy5nYW1lLmxvYWQuaW1hZ2UoJ3NwbGFzaC1pbWFnZScsICdhc3NldHMvaW1hZ2VzL3NwbGFzaC9zcGxhc2gtaW1hZ2UucG5nJyk7XG5cdH1cblxuXHRjcmVhdGUoKXtcblx0XHR2YXIgc3BsYXNoSW1hZ2UgPSB0aGlzLmdhbWUuYWRkLnNwcml0ZSh0aGlzLmdhbWUud29ybGQuY2VudGVyWCwgMjAsICdzcGxhc2gtaW1hZ2UnKTtcblx0XHRzcGxhc2hJbWFnZS5hbmNob3Iuc2V0VG8oMC41LCAwKTtcblx0XHRzcGxhc2hJbWFnZS5hbHBoYSA9IDA7XG5cblx0XHR2YXIgdHdlZW5BbmltYXRlSW4gPSB0aGlzLmdhbWUuYWRkLnR3ZWVuKHNwbGFzaEltYWdlKS50byggeyBhbHBoYTogMSB9LCAyMDAwLCBcIkxpbmVhclwiKTtcblx0XHR2YXIgdHdlZW5BbmltYXRlT3V0ID0gdGhpcy5nYW1lLmFkZC50d2VlbihzcGxhc2hJbWFnZSkudG8oIHsgYWxwaGE6IDAgfSwgMjAwMCwgXCJMaW5lYXJcIiwgZmFsc2UsIDIwMDApO1xuXG5cdFx0dHdlZW5BbmltYXRlSW4uY2hhaW4odHdlZW5BbmltYXRlT3V0KTtcblx0XHR0d2VlbkFuaW1hdGVPdXQub25Db21wbGV0ZS5hZGQodGhpcy5oYW5kbGVUd2VlbkNvbXBsZXRlLCB0aGlzKTtcblx0XHR0d2VlbkFuaW1hdGVJbi5zdGFydCgpO1xuXHR9XG5cblx0dXBkYXRlKCl7XG5cdFx0aWYgKHRoaXMuZ2FtZS5pbnB1dC5hY3RpdmVQb2ludGVyLmlzRG93bil7XG5cdFx0XHR0aGlzLmdvVG9NYWluTWVudVNjcmVlbigpO1xuXHRcdH1cblx0fVxuXG5cdGhhbmRsZVR3ZWVuQ29tcGxldGUoKXtcblx0XHR0aGlzLmdvVG9NYWluTWVudVNjcmVlbigpO1xuXHR9XG5cblx0Z29Ub01haW5NZW51U2NyZWVuKCl7XG5cdFx0dGhpcy5nYW1lLnN0YXRlLnN0YXJ0KFwiTWFpbk1lbnVcIik7XG5cdH1cbn0iLCJpbXBvcnQgR3JpZFNwcml0ZSBmcm9tICcuL0dyaWRTcHJpdGUnXG5pbXBvcnQgR3JpZERpc2NTcHJpdGUgZnJvbSAnLi9HcmlkRGlzY1Nwcml0ZSdcbmltcG9ydCBQb2ludHNTcHJpdGUgZnJvbSAnLi9Qb2ludHNTcHJpdGUnXG5cbmltcG9ydCBHYW1lU2V0dGluZ3MgZnJvbSAnLi4vLi4vc2V0dGluZ3MvR2FtZVNldHRpbmdzJ1xuXG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIGV4dGVuZHMgUGhhc2VyLlN0YXRle1xuXG5cdHByZWxvYWQoKXtcblx0XHR0aGlzLmdhbWUubG9hZC5hdGxhc0pTT05IYXNoKCdkaXNjc3ByaXRlcycsICdhc3NldHMvaW1hZ2VzL2dhbWVwbGF5L2Rpc2Nfc3ByaXRlcy5wbmcnLCAnYXNzZXRzL2ltYWdlcy9nYW1lcGxheS9kaXNjX3Nwcml0ZXMuanNvbicpO1xuXHR9XG5cblx0Y3JlYXRlKCl7XG5cdFx0dGhpcy5kaXNjU3ByaXRlcyA9IFtdO1xuXHRcdHRoaXMuYW5pbWF0aW9uUXVldWUgPSBbXTtcblx0XHR0aGlzLmlzQW5pbWF0aW5nID0gZmFsc2U7XG5cdFx0dGhpcy5pbkdhbWVPdmVyU2VxdWVuY2UgPSBmYWxzZTtcblx0XHR0aGlzLmNvbnRhaW5lciA9IHRoaXMuZ2FtZS5hZGQuZ3JvdXAoKTtcblx0XHR0aGlzLmNvbnRhaW5lci54ID0gMTAwO1xuXHRcdHRoaXMuY29udGFpbmVyLnkgPSAxMDA7XG5cblx0XHRcblxuXHRcdHRoaXMuZGlzY3NHcm91cCA9IHRoaXMuZ2FtZS5hZGQuZ3JvdXAoKTtcblxuXHRcdHRoaXMucHJldmlvdXNEaXNjU3ByaXRlID0gbnVsbDtcblx0XHR0aGlzLm5leHREaXNjU3ByaXRlID0gbnVsbDtcblxuXHRcdHRoaXMuZ3JpZFNwcml0ZSA9IG5ldyBHcmlkU3ByaXRlKHRoaXMuZ2FtZSk7XG5cdFx0dGhpcy5ncmlkU3ByaXRlLmV2ZW50cy5vbklucHV0VXAuYWRkKHRoaXMuaGFuZGxlR3JpZENsaWNrZWQsIHRoaXMpO1xuXHRcdHRoaXMuY29udGFpbmVyLmFkZCh0aGlzLmdyaWRTcHJpdGUpO1xuXHRcdHRoaXMuY29udGFpbmVyLmFkZCh0aGlzLmRpc2NzR3JvdXApO1xuXG5cdFx0dGhpcy5lZmZlY3RzID0gdGhpcy5nYW1lLmFkZC5ncm91cCgpO1xuXHRcdHRoaXMuZWZmZWN0cy54ID0gdGhpcy5jb250YWluZXIueDtcblx0XHR0aGlzLmVmZmVjdHMueSA9IHRoaXMuY29udGFpbmVyLnk7XG5cblx0XHR2YXIgc3R5bGUxID0geyBmb250OiBcIjQwcHggUGVybWFuZW50IE1hcmtlclwiLCBmaWxsOiBcIiNiMDVkZmZcIiwgYWxpZ246IFwibGVmdFwiIH07XG5cdFx0dGhpcy5zY29yZVRleHRmaWVsZCA9IHRoaXMuZ2FtZS5hZGQudGV4dCgwLCAwLFwiU2NvcmVcIiwgc3R5bGUxKTtcblx0XHR0aGlzLnNjb3JlVGV4dGZpZWxkLnggPSAyMDtcblx0XHR0aGlzLnNjb3JlVGV4dGZpZWxkLnkgPSAxMDtcblxuXHRcdHZhciBzdHlsZTIgPSB7IGZvbnQ6IFwiNDBweCBQZXJtYW5lbnQgTWFya2VyXCIsIGZpbGw6IFwiI2IwNWRmZlwiLCBhbGlnbjogXCJyaWdodFwiIH07XG5cdFx0dGhpcy5jb3VudGRvd25UZXh0ZmllbGQgPSB0aGlzLmdhbWUuYWRkLnRleHQoMCwgMCxcIk5leHQgcm91bmRcIiwgc3R5bGUyKTtcblx0XHR0aGlzLmNvdW50ZG93blRleHRmaWVsZC54ID0gNzMwO1xuXHRcdHRoaXMuY291bnRkb3duVGV4dGZpZWxkLnkgPSAxMDtcblx0XHR0aGlzLmNvdW50ZG93blRleHRmaWVsZC5hbmNob3Iuc2V0KDEsIDApO1xuXG5cdFx0dGhpcy5nYW1lLmNvbW1hbmRlci5leGVjdXRlKFwiR2FtZXBsYXlDb21tYW5kLkluaXRpYWxpc2VHYW1lcGxheVwiLCB0aGlzLmdhbWUsIHRoaXMpO1xuXHRcdHRoaXMuZ2FtZS5jb21tYW5kZXIuZXhlY3V0ZShcIkFwcENvbW1hbmQuUGxheVNvdW5kXCIsIFwibmV3X2dhbWVcIik7XG5cdH1cblxuXHR1cGRhdGUoKSB7XG5cdFx0aWYodGhpcy5uZXh0RGlzY1Nwcml0ZSAhPSBudWxsICYmICF0aGlzLmluR2FtZU92ZXJTZXF1ZW5jZSl7XG5cdFx0XHR0aGlzLmhhbmRsZUdyaWRNb3ZlKCk7XG5cdFx0fVxuXG5cdFx0dGhpcy5kaXNjc0dyb3VwLnNvcnQoJ3knLCBQaGFzZXIuR3JvdXAuU09SVF9ERVNDRU5ESU5HKTtcblx0XHR0aGlzLnNjb3JlVGV4dGZpZWxkLnRleHQgPSBcIlNjb3JlOiBcIiArIHRoaXMuZ2FtZS5tb2RlbC5nYW1lcGxheS5zY29yZTtcblx0XHR0aGlzLmNvdW50ZG93blRleHRmaWVsZC50ZXh0ID0gXCJOZXh0IHJvdW5kOiBcIiArIHRoaXMuZ2FtZS5tb2RlbC5nYW1lcGxheS50dXJuc1VudGlsTmV4dFJvdW5kICsgXCIgdHVybnNcIjtcblx0fVxuXG5cdGhhbmRsZUdyaWRDbGlja2VkKG93bmVyLCBwb2ludGVyKXtcblx0XHRpZighdGhpcy5pc0FuaW1hdGluZyAmJiAhdGhpcy5pbkdhbWVPdmVyU2VxdWVuY2Upe1xuXHRcdFx0Ly9EZXRlcm1pbmUgcm93IGNsaWNrZWRcblx0XHRcdHZhciBsb2NhbFggPSBwb2ludGVyLnggLSB0aGlzLmNvbnRhaW5lci54O1xuXHRcdFx0dmFyIGNvbHVtblggPSBNYXRoLmZsb29yKGxvY2FsWCAvIEdhbWVTZXR0aW5ncy5HUklEX1RJTEVfU0laRSk7XG5cblx0XHRcdC8vQm91bmQgdG8gZ3JpZFxuXHRcdFx0aWYoY29sdW1uWCA8IDApe1xuXHRcdFx0XHRjb2x1bW5YID0gMDtcblx0XHRcdH1cblx0XHRcdGlmKGNvbHVtblggPiBHYW1lU2V0dGluZ3MuR1JJRF9XSURUSCAtIDEpe1xuXHRcdFx0XHRjb2x1bW5YID0gR2FtZVNldHRpbmdzLkdSSURfV0lEVEggLSAxO1xuXHRcdFx0fVxuXG5cdFx0XHR0aGlzLnByZXZpb3VzRGlzY1Nwcml0ZSA9IHRoaXMubmV4dERpc2NTcHJpdGU7XG5cdFx0XHR0aGlzLm5leHREaXNjU3ByaXRlID0gbnVsbDtcblxuXHRcdFx0dGhpcy5nYW1lLmNvbW1hbmRlci5leGVjdXRlKFwiR2FtZXBsYXlDb21tYW5kLkRyb3BOZXh0R3JpZERpc2NcIiwgdGhpcy5nYW1lLCB0aGlzLnByZXZpb3VzRGlzY1Nwcml0ZS5kYXRhLCBjb2x1bW5YLCB0aGlzKTtcblx0XHR9XG5cdH1cblxuXHRoYW5kbGVHcmlkTW92ZSgpe1xuXHRcdC8vRGV0ZXJtaW5lIHJvdyBjbGlja2VkXG5cdFx0dmFyIGxvY2FsWCA9IHRoaXMuZ2FtZS5pbnB1dC54IC0gdGhpcy5jb250YWluZXIueDtcblx0XHQvL3ZhciBsb2NhbFkgPSBwb2ludGVyLnkgLSB0aGlzLmNvbnRhaW5lci55O1xuXHRcdHZhciBjb2x1bW5YID0gTWF0aC5mbG9vcihsb2NhbFggLyBHYW1lU2V0dGluZ3MuR1JJRF9USUxFX1NJWkUpO1xuXG5cdFx0Ly9Cb3VuZCB0byBncmlkXG5cdFx0aWYoY29sdW1uWCA8IDApe1xuXHRcdFx0Y29sdW1uWCA9IDA7XG5cdFx0fVxuXHRcdGlmKGNvbHVtblggPiBHYW1lU2V0dGluZ3MuR1JJRF9XSURUSCAtIDEpe1xuXHRcdFx0Y29sdW1uWCA9IEdhbWVTZXR0aW5ncy5HUklEX1dJRFRIIC0gMTtcblx0XHR9XG5cblx0XHR2YXIgY29sdW1uV29ybGRYID0gY29sdW1uWCAqICBHYW1lU2V0dGluZ3MuR1JJRF9USUxFX1NJWkUgKyAoR2FtZVNldHRpbmdzLkdSSURfVElMRV9TSVpFICogMC41KTtcblxuXHRcdHRoaXMubmV4dERpc2NTcHJpdGUueCA9IGNvbHVtbldvcmxkWDtcblx0XHR0aGlzLm5leHREaXNjU3ByaXRlLnkgPSAoR2FtZVNldHRpbmdzLkdSSURfVElMRV9TSVpFICogMC41KTtcblx0fVxuXG5cdGNyZWF0ZU5leHREaXNjU3ByaXRlKGdyaWREaXNjRGF0YSl7XG5cdFx0dGhpcy5uZXh0RGlzY1Nwcml0ZSA9IHRoaXMuY3JlYXRlTmV3R3JpZERpc2NTcHJpdGUoZ3JpZERpc2NEYXRhKTtcblx0fVxuXG5cdGNyZWF0ZU5ld0dyaWREaXNjU3ByaXRlKGdyaWREaXNjRGF0YSl7XG5cdFx0dmFyIG5ld0dyaWREaXNjU3ByaXRlID0gbmV3IEdyaWREaXNjU3ByaXRlKHRoaXMuZ2FtZSwgZ3JpZERpc2NEYXRhKTtcblx0XHR0aGlzLmRpc2NzR3JvdXAuYWRkKG5ld0dyaWREaXNjU3ByaXRlKTtcblx0XHR0aGlzLmRpc2NTcHJpdGVzLnB1c2gobmV3R3JpZERpc2NTcHJpdGUpO1xuXHRcdHJldHVybiBuZXdHcmlkRGlzY1Nwcml0ZVxuXHR9XG5cblx0Y3JlYXRlUG9pbnRzU3ByaXRlKHZhbHVlLCB4LCB5KXtcblx0XHR2YXIgbmV3UG9pbnRzU3ByaXRlID0gbmV3IFBvaW50c1Nwcml0ZSh0aGlzLmdhbWUsIHZhbHVlLCB4LCB5KTtcblx0XHR0aGlzLmVmZmVjdHMuYWRkKG5ld1BvaW50c1Nwcml0ZSk7XG5cdFx0bmV3UG9pbnRzU3ByaXRlLmFuaW1hdGUoKTtcblx0fVxuXG5cblx0aGFuZGxlQW5pbWF0aW9ucygpe1xuXHRcdHZhciBkaXNjc1RvRGVzdHJveSA9IFtdO1xuXHRcdHZhciBkaXNjc1RvVXBkYXRlID0gW107XG5cdFx0Ly9kZXRlcm1pbmUgZGlzY3MgdG8gYW5pbWF0ZVxuXHRcdGZvciAodmFyIGkgPSAwOyBpIDwgdGhpcy5kaXNjU3ByaXRlcy5sZW5ndGg7IGkrKykge1xuXHRcdFx0dmFyIGRpc2NTcHJpdGUgPSB0aGlzLmRpc2NTcHJpdGVzW2ldO1xuXG5cdFx0XHRpZihkaXNjU3ByaXRlLmRlc3Ryb3llZCl7XG5cdFx0XHRcdGRpc2NzVG9EZXN0cm95LnB1c2goZGlzY1Nwcml0ZSk7XG5cdFx0XHR9ZWxzZSBpZihkaXNjU3ByaXRlLnVwZGF0ZWQpe1xuXHRcdFx0XHRkaXNjc1RvVXBkYXRlLnB1c2goZGlzY1Nwcml0ZSk7XG5cdFx0XHR9XG5cdFx0fVxuXG5cdFx0Ly9yZW1vdmUgZGVzdHJveWVkIHNwcml0ZXMgZnJvbSBtYWluIGFycmF5XG5cdFx0dGhpcy5kaXNjU3ByaXRlcyA9IHRoaXMuZGlzY1Nwcml0ZXMuZmlsdGVyKCBmdW5jdGlvbiggZWwgKSB7XG5cdFx0XHRyZXR1cm4gIWRpc2NzVG9EZXN0cm95LmluY2x1ZGVzKCBlbCApO1xuXHRcdH0gKTtcblxuXHRcdGlmKGRpc2NzVG9EZXN0cm95Lmxlbmd0aCA+IDApe1xuXHRcdFx0dGhpcy5hbmltYXRpb25RdWV1ZS5wdXNoKGRpc2NzVG9EZXN0cm95KTtcblx0XHR9XG5cdFx0XG5cdFx0Ly8gRGVzdHJveSBkaXNjc1xuXHRcdGZvciAodmFyIGogPSAwOyBqIDwgZGlzY3NUb0Rlc3Ryb3kubGVuZ3RoOyBqKyspIHtcblx0XHRcdHRoaXMuZGVzdHJveURpc2NBbmltYXRpb24oZGlzY3NUb0Rlc3Ryb3lbal0pO1xuXHRcdFx0ZGlzY3NUb0Rlc3Ryb3lbal0uc3RhcnRUd2VlbigpO1xuXHRcdH1cblxuXHRcdC8vIEFuaW1hdGUgdXBkYXRlZCBkaXNjc1xuXHRcdGlmKGRpc2NzVG9VcGRhdGUubGVuZ3RoID4gMCl7XG5cdFx0XHR0aGlzLmFuaW1hdGlvblF1ZXVlLnB1c2goZGlzY3NUb1VwZGF0ZSk7XG5cdFx0fVxuXG5cdFx0Zm9yICh2YXIgayA9IDA7IGsgPCBkaXNjc1RvVXBkYXRlLmxlbmd0aDsgaysrKSB7XG5cdFx0XHR0aGlzLnVwZGF0ZURpc2NBbmltYXRpb24oZGlzY3NUb1VwZGF0ZVtrXSk7XG5cblx0XHRcdC8vTm90aGluZyBiZWluZyBkZXN0cm95ZWQgLSBhbmltYXRlIHVwZGF0ZXNcblx0XHRcdGlmKGRpc2NzVG9EZXN0cm95Lmxlbmd0aCA9PSAwKXtcblx0XHRcdFx0ZGlzY3NUb1VwZGF0ZVtrXS5zdGFydFR3ZWVuKCk7XG5cdFx0XHR9XG5cdFx0fVxuXG5cdFx0aWYodGhpcy5hbmltYXRpb25RdWV1ZS5sZW5ndGggPiAwKXtcblx0XHRcdHRoaXMuaXNBbmltYXRpbmcgPSB0cnVlO1xuXHRcdH1cblx0XHRcblx0fVxuXG5cdGRlc3Ryb3lEaXNjQW5pbWF0aW9uKGRpc2NTcHJpdGUpe1xuXHRcdGRpc2NTcHJpdGUudHdlZW5Ub0Rlc3Ryb3kodGhpcy5oYW5kbGVEaXNjQW5pbWF0aW9uQ29tcGxldGUsIHRoaXMpO1xuXHR9XG5cblx0dXBkYXRlRGlzY0FuaW1hdGlvbihkaXNjU3ByaXRlKXtcblx0XHRkaXNjU3ByaXRlLnR3ZWVuVG9Xb3JsZFBvc2l0aW9uKHRoaXMuaGFuZGxlRGlzY0FuaW1hdGlvbkNvbXBsZXRlLCB0aGlzKTtcblx0fVxuXG5cdGhhbmRsZURpc2NBbmltYXRpb25Db21wbGV0ZShkaXNjU3ByaXRlLCBkZXN0cm95T25Db21wbGV0ZSA9IGZhbHNlKXtcblx0XHR2YXIgY3VycmVudEFuaW1hdGlvbkdyb3VwID0gIHRoaXMuYW5pbWF0aW9uUXVldWVbMF07XG5cblx0XHRpZihjdXJyZW50QW5pbWF0aW9uR3JvdXAuaW5jbHVkZXMoZGlzY1Nwcml0ZSkpe1xuXHRcdFx0dmFyIGluZGV4ID0gY3VycmVudEFuaW1hdGlvbkdyb3VwLmluZGV4T2YoZGlzY1Nwcml0ZSk7XG5cdFx0XHRjdXJyZW50QW5pbWF0aW9uR3JvdXAuc3BsaWNlKGluZGV4LCAxKTtcblx0XHR9XG5cblx0XHRpZihkZXN0cm95T25Db21wbGV0ZSl7XG5cdFx0XHRkaXNjU3ByaXRlLmRlc3Ryb3koKTtcblx0XHR9XG5cblx0XHRpZihjdXJyZW50QW5pbWF0aW9uR3JvdXAubGVuZ3RoID09IDApe1xuXHRcdFx0dGhpcy5hbmltYXRpb25RdWV1ZS5zcGxpY2UoMCwgMSk7XG5cdFx0XHQvL0FuaW1hdGlvbnMgY29tcGxldGVcblxuXHRcdFx0aWYodGhpcy5hbmltYXRpb25RdWV1ZS5sZW5ndGggPT0gMCl7XG5cdFx0XHRcdHRoaXMuaXNBbmltYXRpbmcgPSBmYWxzZTtcblx0XHRcdFx0Ly8gQ2hlY2sgZm9yIGNoYWluIGNvbWJvXG5cdFx0XHRcdHRoaXMuZ2FtZS5jb21tYW5kZXIuZXhlY3V0ZShcIkdhbWVwbGF5Q29tbWFuZC5FdmFsdWF0ZUdyaWRNYXRjaGVzXCIsIHRoaXMuZ2FtZSk7XG5cdFx0XHR9ZWxzZXtcblx0XHRcdFx0Zm9yICh2YXIgaSA9IDA7IGkgPCB0aGlzLmFuaW1hdGlvblF1ZXVlWzBdLmxlbmd0aDsgaSsrKSB7XG5cdFx0XHRcdFx0dmFyIGRpc2MgPSB0aGlzLmFuaW1hdGlvblF1ZXVlWzBdW2ldO1xuXHRcdFx0XHRcdGRpc2Muc3RhcnRUd2VlbigpO1xuXHRcdFx0XHR9XG5cdFx0XHR9XG5cdFx0fVxuXHR9XG5cblx0Y2FuY2VsRHJvcCgpe1x0XHRcblx0XHR0aGlzLm5leHREaXNjU3ByaXRlID0gdGhpcy5wcmV2aW91c0Rpc2NTcHJpdGU7XG5cdFx0dGhpcy5pc0FuaW1hdGluZyA9IGZhbHNlO1xuXHR9XG5cblx0YmVnaW5HYW1lT3ZlclNlcXVlbmNlKCl7XG5cdFx0dGhpcy5pbkdhbWVPdmVyU2VxdWVuY2UgPSB0cnVlO1xuXG5cdFx0dmFyIHRpbWVsaW5lID0gbmV3IFRpbWVsaW5lTWF4KHtvbkNvbXBsZXRlOnRoaXMuZW5kR2FtZXBsYXkuYmluZCh0aGlzKX0pO1xuXHRcdHRpbWVsaW5lLnNldChcIiNnYW1lLW92ZXJcIiwge2F1dG9BbHBoYTogMX0pXG5cdFx0dGhpcy5jb250YWluZXIuZm9yRWFjaChmdW5jdGlvbihpdGVtKSB7XG5cdFx0XHR0aW1lbGluZS50byhpdGVtLCAwLjQsIHthbHBoYSA6IDAuM30sIDApO1xuXHRcdH0pO1xuXG5cdFx0dGltZWxpbmUuZnJvbShcIi5nYW1lLW92ZXItb3V0bGluZVwiLCAxLCB7ZHJhd1NWRzpcIjAlXCIsIGVhc2U6U2luZS5lYXNlSW5PdXR9KVxuXHRcdC5hZGQoXCJGaWxsLWluXCIpXG5cdFx0LmZyb20oXCIjZ2FtZS1vdmVyLWZpbGxcIiwgMSwge2FscGhhOjB9LCBcIkZpbGwtaW5cIilcblx0XHQudG8oXCIuZ2FtZS1vdmVyLW91dGxpbmVcIiwgMSwge2FscGhhOjB9LCBcIkZpbGwtaW5cIilcblx0XHQudG8oXCIjZ2FtZS1vdmVyXCIsIDEsIHthdXRvQWxwaGE6IDB9LCBcIis9MlwiKTtcblx0fVxuXG5cblx0ZW5kR2FtZXBsYXkoKXtcblx0XHR0aGlzLmdhbWUuY29tbWFuZGVyLmV4ZWN1dGUoXCJHYW1lcGxheUNvbW1hbmQuSGFuZGxlR2FtZU92ZXJcIiwgdGhpcy5nYW1lKTtcblx0fVxuXG59XG4iLCJpbXBvcnQgR2FtZVNldHRpbmdzIGZyb20gJy4uLy4uL3NldHRpbmdzL0dhbWVTZXR0aW5ncydcblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgZXh0ZW5kcyBQaGFzZXIuU3ByaXRlIHtcblxuXHRjb25zdHJ1Y3RvcihnYW1lLCBncmlkRGlzY0RhdGEpe1xuXG5cdFx0dmFyIHNwcml0ZTtcblx0XHRpZihncmlkRGlzY0RhdGEuYXJtb3VyID49IDIpe1xuXHRcdFx0c3ByaXRlID0gIGdhbWUubWFrZS5zcHJpdGUoMCwgMCwgJ2Rpc2NzcHJpdGVzJywgXCJhcm1vdXIyXCIpO1xuXHRcdH1lbHNlIGlmKGdyaWREaXNjRGF0YS5hcm1vdXIgPT0gMSl7XG5cdFx0XHRzcHJpdGUgPSAgZ2FtZS5tYWtlLnNwcml0ZSgwLCAwLCAnZGlzY3Nwcml0ZXMnLCBcImFybW91cjFcIik7XG5cdFx0fWVsc2V7XG5cdFx0XHRzcHJpdGUgPSAgZ2FtZS5tYWtlLnNwcml0ZSgwLCAwLCAnZGlzY3Nwcml0ZXMnLCBncmlkRGlzY0RhdGEuZGlzY1ZhbHVlLnRvU3RyaW5nKCkpO1xuXHRcdH1cblxuXHRcdFxuXHRcdHN1cGVyKGdhbWUsIDAsIDAsIHNwcml0ZS50ZXh0dXJlKTtcblxuXG5cdFx0dGhpcy5hcm1vdXIyU3ByaXRlID0gIGdhbWUubWFrZS5zcHJpdGUoMCwgMCwgJ2Rpc2NzcHJpdGVzJywgXCJhcm1vdXIyXCIpO1xuXHRcdHRoaXMuYXJtb3VyMVNwcml0ZSA9ICBnYW1lLm1ha2Uuc3ByaXRlKDAsIDAsICdkaXNjc3ByaXRlcycsIFwiYXJtb3VyMVwiKTtcblx0XHR0aGlzLnZhbHVlU3ByaXRlID0gZ2FtZS5tYWtlLnNwcml0ZSgwLCAwLCAnZGlzY3Nwcml0ZXMnLCBncmlkRGlzY0RhdGEuZGlzY1ZhbHVlLnRvU3RyaW5nKCkpO1xuXG5cdFx0dGhpcy5hbmNob3Iuc2V0KDAuNSk7XG5cdFx0dGhpcy5kYXRhID0gZ3JpZERpc2NEYXRhO1xuXHRcdHRoaXMuZGF0YS5vbkNoYW5nZWQuYWRkKHRoaXMuX2hhbmRsZVBvc2l0aW9uVXBkYXRlZCwgdGhpcylcblx0XHR0aGlzLmRhdGEub25Bcm1vdXJDaGFuZ2VkLmFkZCh0aGlzLl9oYW5kbGVBcm1vdXJVcGRhdGVkLCB0aGlzKVxuXHRcdHRoaXMudXBkYXRlZCA9IGZhbHNlO1xuXHRcdHRoaXMudHdlZW4gPSBudWxsO1xuXHR9XG5cblx0dXBkYXRlKCl7XG5cdFx0dGhpcy56ID0gLXRoaXMueTtcblx0fVxuXG5cdGNoYW5nZVNwcml0ZSgpe1xuXHRcdGlmKHRoaXMuZGF0YS5hcm1vdXIgPj0gMil7XG5cdFx0XHR0aGlzLmxvYWRUZXh0dXJlKCdkaXNjc3ByaXRlcycsIFwiYXJtb3VyMlwiKTtcblx0XHR9ZWxzZSBpZih0aGlzLmRhdGEuYXJtb3VyID09IDEpe1xuXHRcdFx0dGhpcy5sb2FkVGV4dHVyZSgnZGlzY3Nwcml0ZXMnLCBcImFybW91cjFcIik7XG5cdFx0fWVsc2V7XG5cdFx0XHR0aGlzLmxvYWRUZXh0dXJlKCdkaXNjc3ByaXRlcycsIHRoaXMuZGF0YS5kaXNjVmFsdWUudG9TdHJpbmcoKSk7XG5cdFx0fVxuXHR9XG5cblx0Z2V0IGRlc3Ryb3llZCgpe1xuXHRcdHJldHVybiB0aGlzLmRhdGEuZGVzdHJveWVkO1xuXHR9XG5cblxuXHRfaGFuZGxlUG9zaXRpb25VcGRhdGVkKCl7XG5cdFx0dGhpcy51cGRhdGVkID0gdHJ1ZTtcblx0fVxuXG5cdF9oYW5kbGVBcm1vdXJVcGRhdGVkKCl7XG5cdFx0dGhpcy5jaGFuZ2VTcHJpdGUoKTtcblx0fVxuXG5cdGdyaWRQb3NpdGlvblRvV29ybGRQb3NpdGlvbigpe1xuXHRcdHRoaXMueCA9IHRoaXMuZGF0YS54ICogR2FtZVNldHRpbmdzLkdSSURfVElMRV9TSVpFICsgKEdhbWVTZXR0aW5ncy5HUklEX1RJTEVfU0laRSowLjUpO1xuXHRcdHRoaXMueSA9IHRoaXMuZGF0YS55ICogR2FtZVNldHRpbmdzLkdSSURfVElMRV9TSVpFICsgKEdhbWVTZXR0aW5ncy5HUklEX1RJTEVfU0laRSowLjUpO1xuXHR9XG5cblx0dHdlZW5Ub0Rlc3Ryb3kob25Db21wbGV0ZUNhbGxiYWNrLCBzY29wZSl7XG5cdFx0dGhpcy50d2VlbiA9IFR3ZWVuTWF4LnRvKHRoaXMuc2NhbGUsIDAuNSwge3g6MCwgeTowLCBlYXNlOkJhY2suZWFzZUluLmNvbmZpZygxLjcpLCBvbkNvbXBsZXRlOiBvbkNvbXBsZXRlQ2FsbGJhY2suYmluZChzY29wZSksICBvbkNvbXBsZXRlUGFyYW1zOlt0aGlzLCB0cnVlXSwgIHBhdXNlZDp0cnVlfSk7XG5cdH1cblxuXHR0d2VlblRvV29ybGRQb3NpdGlvbihvbkNvbXBsZXRlQ2FsbGJhY2ssIHNjb3BlKXtcblx0XHR2YXIgd29ybGRYID0gdGhpcy5kYXRhLnggKiBHYW1lU2V0dGluZ3MuR1JJRF9USUxFX1NJWkUgKyAoR2FtZVNldHRpbmdzLkdSSURfVElMRV9TSVpFKjAuNSk7XG5cdFx0dmFyIHdvcmxkWSA9IHRoaXMuZGF0YS55ICogR2FtZVNldHRpbmdzLkdSSURfVElMRV9TSVpFICsgKEdhbWVTZXR0aW5ncy5HUklEX1RJTEVfU0laRSowLjUpO1xuXG5cblx0XHR0aGlzLnR3ZWVuID0gVHdlZW5NYXgudG8odGhpcywgMC4yNSwgeyB4OiB3b3JsZFgsIHk6IHdvcmxkWSwgZWFzZTogU2luZS5lYXNlSW5PdXQsIG9uQ29tcGxldGU6IG9uQ29tcGxldGVDYWxsYmFjay5iaW5kKHNjb3BlKSwgIG9uQ29tcGxldGVQYXJhbXM6W3RoaXNdLCBwYXVzZWQ6dHJ1ZX0pO1xuXHRcdHRoaXMudXBkYXRlZCA9IGZhbHNlO1xuXHR9XG5cblx0ZGVzdHJveSgpe1xuXHRcdHRoaXMuZGF0YS5vbkNoYW5nZWQucmVtb3ZlKHRoaXMuX2hhbmRsZVBvc2l0aW9uVXBkYXRlZCwgdGhpcylcblx0XHRzdXBlci5kZXN0cm95KClcblx0fVxuXG5cdHN0YXJ0VHdlZW4oKXtcblx0XHR0aGlzLnR3ZWVuLnBsYXkoKTtcblx0fVxuXG5cbn1cblxuIiwiaW1wb3J0IEdhbWVTZXR0aW5ncyBmcm9tICcuLi8uLi9zZXR0aW5ncy9HYW1lU2V0dGluZ3MnXG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIGV4dGVuZHMgUGhhc2VyLlNwcml0ZSB7XG5cblx0Y29uc3RydWN0b3IoZ2FtZSl7XG5cdFx0XG5cdFx0dmFyIGdyYXBoaWNzID0gZ2FtZS5hZGQuZ3JhcGhpY3MoMCwgMCk7XG5cblx0XHRncmFwaGljcy5iZWdpbkZpbGwoMHhGRjAwMDAsIDEpO1xuXG5cdFx0Zm9yICh2YXIgeSA9IDA7IHkgPCBHYW1lU2V0dGluZ3MuVFJVRV9HUklEX0hFSUdIVDsgeSsrKSB7XG5cdFx0XHRmb3IgKHZhciB4ID0gMDsgeCA8IEdhbWVTZXR0aW5ncy5HUklEX1dJRFRIOyB4KyspIHtcblx0XHRcdFx0Ly9hZGQgZXh0cmEgcm93IGZvciB0aGUgb3ZlcmZsb3dcblx0XHRcdFx0aWYoeSA9PSAwKXtcblx0XHRcdFx0XHRncmFwaGljcy5iZWdpbkZpbGwoMHhGRjcxY2UsIDAuMSk7XG5cdFx0XHRcdH1lbHNle1xuXHRcdFx0XHRcdGdyYXBoaWNzLmJlZ2luRmlsbCgweEZGNzFjZSwgMSk7XG5cdFx0XHRcdH1cblx0XHRcdFx0XG5cdFx0XHRcdGdyYXBoaWNzLmxpbmVTdHlsZSgxLCAweDA1ZmZhMSwgMSk7XG5cdFx0XHRcdGdyYXBoaWNzLmRyYXdSZWN0KHgqR2FtZVNldHRpbmdzLkdSSURfVElMRV9TSVpFLCB5KkdhbWVTZXR0aW5ncy5HUklEX1RJTEVfU0laRSwgR2FtZVNldHRpbmdzLkdSSURfVElMRV9TSVpFLCBHYW1lU2V0dGluZ3MuR1JJRF9USUxFX1NJWkUpO1xuXHRcdFx0fVxuXHRcdH1cblxuXHRcdHN1cGVyKGdhbWUsIDAsIDAsIGdyYXBoaWNzLmdlbmVyYXRlVGV4dHVyZSgpKTtcblx0XHRncmFwaGljcy5kZXN0cm95KCk7XG5cblx0XHR0aGlzLmlucHV0RW5hYmxlZCA9IHRydWU7XG5cdH1cblxuXHRcbn1cblxuIiwiaW1wb3J0IEdhbWVTZXR0aW5ncyBmcm9tICcuLi8uLi9zZXR0aW5ncy9HYW1lU2V0dGluZ3MnXG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIGV4dGVuZHMgUGhhc2VyLlNwcml0ZSB7XG5cblx0Y29uc3RydWN0b3IoZ2FtZSwgcG9pbnRzVmFsdWUsIHgsIHkpe1xuXHRcdHZhciBzdHlsZTEgPSB7IGZvbnQ6IFwiNDBweCBQZXJtYW5lbnQgTWFya2VyXCIsIGZpbGw6IFwiI2IwNWRmZlwiLCBhbGlnbjogXCJjZW50ZXJcIiB9O1xuXG5cdFx0dmFyIHRleHQgPSBnYW1lLm1ha2UudGV4dCgwLCAwLFwiK1wiK3BvaW50c1ZhbHVlLCBzdHlsZTEpO1xuXHRcdHRleHQuc3Ryb2tlID0gJyNmZmZmZmYnO1xuXHRcdHRleHQuc3Ryb2tlVGhpY2tuZXNzID0gNjtcblx0XHRcblx0XHRzdXBlcihnYW1lLCAwLCAwLCB0ZXh0LmdlbmVyYXRlVGV4dHVyZSgpKTtcblx0XHR0aGlzLmFuY2hvci5zZXQoMC41KTtcblx0XHR0aGlzLnggPSB4ICogR2FtZVNldHRpbmdzLkdSSURfVElMRV9TSVpFICsgKEdhbWVTZXR0aW5ncy5HUklEX1RJTEVfU0laRSowLjUpO1xuXHRcdHRoaXMueSA9IHkgKiBHYW1lU2V0dGluZ3MuR1JJRF9USUxFX1NJWkUgKyAoR2FtZVNldHRpbmdzLkdSSURfVElMRV9TSVpFKjAuNSk7XG5cdH1cblxuXHRhbmltYXRlKCl7XG5cdFx0dmFyIHRpbWVsaW5lID0gbmV3IFRpbWVsaW5lTWF4KHtvbkNvbXBsZXRlOnRoaXMuZGVzdHJveS5iaW5kKHRoaXMpfSlcblx0XHQudG8odGhpcywgMSwge3k6XCItPTE1MFwiLCBlYXNlOlF1YWQuZWFzZUlufSlcblx0XHQudG8odGhpcywgMC41LCB7YWxwaGE6MH0sIDAuNSlcblx0fVxuXG59Il19
